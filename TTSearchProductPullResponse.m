//
//  TTSearchProductPullResponse.m
//  Houshi
//
//  Created by James Timberlake on 10/6/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTSearchProductPullResponse.h"
#import "TTProductPullDTO.h"

@implementation TTSearchProductPullResponse
+(Class) ProductPull_class                                                                                                   {
    return [TTProductPullDTO class];
}
@end
