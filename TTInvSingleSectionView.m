//
//  TTInvSingleHeaderView.m
//  Houshi
//
//  Created by James Timberlake on 6/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTInvSingleSectionView.h"

static BOOL skuAscending;
static BOOL descAscending;
static BOOL itemAscending;
static int singleHeaderActiveLabelIndex = 0;
static BOOL didCheckAll;

@interface TTInvSingleSectionView () <HighlightedButtonDelegate>
{
    BOOL inMainThread;
}

@property (nonatomic,strong) UILabel* activeLabel;
@end

@implementation TTInvSingleSectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

-(id)init{
    self = [super init];
    if(self){
            NSArray *nibObjects;
            nibObjects = [[NSBundle mainBundle] loadNibNamed:@"TTInveSingleSectionView" owner:self options:nil];
            UIView* nibView = nibObjects[0];
            [self setFrame:nibView.frame];
            [self addSubview:nibView];
            UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
            UITapGestureRecognizer* tapRec2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap2:)];
            UITapGestureRecognizer* tapRec3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap3:)];
            [_skuTitle addGestureRecognizer:tapRec];
            [_descTitle addGestureRecognizer:tapRec2];
            [_itemType addGestureRecognizer:tapRec3];
        _checkMark.delegate = self;
        _checkMark.isHeader = YES;
        self.checkMark.highlightedImage = [UIImage imageNamed:@"checkbox_checked.jpeg"];
        self.checkMark.nonHighlightedImage = [UIImage imageNamed:@"checkbox_unchecked.jpeg"];
        [self.checkMark setSelected: didCheckAll];


    }
    return self;
}

-(void)setActiveColor{
    if (!inMainThread) {
        inMainThread = YES;
        [self performSelectorOnMainThread:@selector(setActiveColor) withObject:nil waitUntilDone:YES];
    }
    if (singleHeaderActiveLabelIndex == 0) {
        [_skuTitle setBackgroundColor:[UIColor blueColor]];
    }else if(singleHeaderActiveLabelIndex == 1){
        [_descTitle setBackgroundColor:[UIColor blueColor]];
    }else if(singleHeaderActiveLabelIndex == 2){
        [_itemType setBackgroundColor:[UIColor blueColor]];
    }
    inMainThread = NO;
}

-(void)awakeFromNib{

}

//sku
-(void)onTap:(UITapGestureRecognizer*)sender
{
    if (sender.view.tag == 0) {
        sender.view.tag = 1;
        [self performSelectorOnMainThread:@selector(onTap:) withObject:sender waitUntilDone:NO];
        return;
    }
    //[self performSelectorOnMainThread:@selector(setTitleInActive:) withObject:_activeLabel waitUntilDone:YES];
    //[self performSelectorOnMainThread:@selector(setTitleActive:) withObject:_skuTitle waitUntilDone:YES];
    //[self setTitleInActive:_activeLabel];
    //[self setTitleActive:_skuTitle];
    singleHeaderActiveLabelIndex = 0;
    skuAscending = (skuAscending != YES);
    if (self.delegate) {
        [self.delegate onItemWithColumnType:@"product.warehouseBin" inAscendingOrder:skuAscending];
    }
    sender.view.tag = 0;
}

//description
-(void)onTap2:(UITapGestureRecognizer*)sender
{

    //[self setTitleInActive:_activeLabel];
    //[self setTitleActive:_descTitle];
    singleHeaderActiveLabelIndex = 1;
    descAscending = (descAscending != YES);
    if (self.delegate) {
        [self.delegate onItemWithColumnType:@"product.desc" inAscendingOrder:descAscending];
    }

}


//item
-(void)onTap3:(UITapGestureRecognizer*)sender
{
    //[self setTitleInActive:_activeLabel];
   // [self setTitleActive:_itemType];
   // singleHeaderActiveLabelIndex = 2;
    itemAscending = (itemAscending != YES);
    if (self.delegate) {
       // [self.delegate onItemWithColumnType:@"Quantity" inAscendingOrder:itemAscending];
    }
}

-(void)onButtonHighlightToggle:(BOOL)isHighlighted{
    didCheckAll = isHighlighted;
    if (_delegate) {
        [_delegate onSingleSectionAllCheckMarkChecked:isHighlighted];
    }
}

-(void)setTitleActive:(UILabel*)selectedLabel{
    if (selectedLabel)
        [selectedLabel setBackgroundColor:[UIColor blueColor]];
   // _activeLabel = selectedLabel;
}

-(void)setTitleInActive:(UILabel*)selectedLabel{
    if(selectedLabel)
        [selectedLabel setBackgroundColor:[UIColor clearColor]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
