//
//  TTCustomerProductDTO.h
//  Houshi
//
//  Created by James Timberlake on 9/16/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTCustomerProductDTO : TTBaseResponse
@property (nonatomic, strong) NSNumber* id;
@property (nonatomic, strong) NSNumber* customerId;
@property (nonatomic, strong) NSNumber* productId;
@property (nonatomic, strong) NSString* recommendedQty;
@property (nonatomic, strong) NSString* casePack;
@property (nonatomic, strong) NSString* modelStock;
@property (nonatomic, strong) NSString* presentationOrder;
@property (nonatomic, strong) NSString* frontStock;
@property (nonatomic, strong) NSString* backStock;
@property (nonatomic, strong) NSNumber* customerOrderOnly;
@property (nonatomic, strong) NSString* availableDate;
@property (nonatomic, strong) NSString* unavailableDate;
@property (nonatomic, strong) NSNumber* active;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@end
