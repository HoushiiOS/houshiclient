//
//  TTCreateOrderRequest.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTCreateOrderRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* customerId;
@property (nonatomic, strong) NSNumber* quickOrder;
@property (nonatomic, strong) NSNumber* customerScheduleId;
@property (nonatomic, strong) NSNumber* isCreditOrder;
@end
