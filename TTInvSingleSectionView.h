
//
//  TTInvSingleHeaderView.h
//  Houshi
//
//  Created by James Timberlake on 6/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTHiglightedStateButton.h"

@protocol SingleHeaderDelegate <NSObject>

-(void)onItemWithColumnType:(NSString*)type inAscendingOrder:(BOOL)isAscending;
-(void)onSingleSectionAllCheckMarkChecked:(BOOL)isHighlighted;

@end

@interface TTInvSingleSectionView : UIView
@property (weak, nonatomic) IBOutlet UILabel *descTitle;
@property (strong, nonatomic) IBOutlet UILabel *skuTitle;
@property (nonatomic,weak) IBOutlet UILabel* itemType;
@property (nonatomic,weak) IBOutlet UILabel* pastItemType;
@property (nonatomic,weak) IBOutlet TTHiglightedStateButton* checkMark;
@property (nonatomic, weak) id<SingleHeaderDelegate> delegate;
-(void)setActiveColor;
@end
