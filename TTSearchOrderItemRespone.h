//
//  TTSearchOrderItemRespone.h
//  Houshi
//
//  Created by James Timberlake on 9/9/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTSearchOrderItemRespone : TTBaseResponse
@property (nonatomic, strong) NSArray* OrderItem;
@end
