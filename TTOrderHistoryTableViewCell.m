//
//  TTOrderHistoryTableViewCell.m
//  Houshi
//
//  Created by James Timberlake on 9/16/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTOrderHistoryTableViewCell.h"

@implementation TTOrderHistoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
