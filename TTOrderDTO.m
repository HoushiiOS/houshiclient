//
//  TTOrderDTO.m
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTOrderDTO.h"
#import "TTOrderItemDTO.h"
#import "TTOrderNoteDTO.h"
#import "TTProductPullDTO.h"
#import "TTProductDeliveryItem.h"

@implementation TTOrderDTO
+(Class)orderItem_class{
    return [TTOrderItemDTO class];
}

//use this later
+(Class)note_class{
    return [TTOrderNoteDTO class];
}

//+(Class)productPull_class{
//    return [TTProductPullDTO class];
//}
//
//+(Class)productDeliveryItem_class{
//    return [TTProductDeliveryItem class];
//}

@end
