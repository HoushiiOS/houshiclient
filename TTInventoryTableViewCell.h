//
//  TTInventoryTableViewCell.h
//  Houshi
//
//  Created by James Timberlake on 6/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+UITableCellUtilities.h"
#import "TTBaseTableViewCell.h"


@interface TTInventoryTableViewCell : TTBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *skuField;
@property (weak, nonatomic) IBOutlet UILabel *headerField;
@property (weak, nonatomic) IBOutlet UITextField *frontField;
@property (weak, nonatomic) IBOutlet UITextField *backField;
@property (weak, nonatomic) IBOutlet UITextField *orderField;
@property (strong, nonatomic) id dataObject;
@property (nonatomic) int rowIndex;
-(int)getRowIndex;
-(void)setRowIndex:(int)rowIndex;
-(void)setDataObject:(id)dataObject;
@end
