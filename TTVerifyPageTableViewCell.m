//
//  TTVerifyPageTableViewCell.m
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTVerifyPageTableViewCell.h"
#import "TTOrderItemDTO.h"
#import "TTAppConatants.h"

@implementation TTVerifyPageTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setAllValuesBasedOnType:(int)type usingObject:(id)object
{
    [_skuLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_title setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_firstValue setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_secondValue setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_thirdValue setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    
    //inventory verifypage
    if (type == 0) {
        _skuLabel.text = ((TTOrderItemDTO*)object).product.sku;
        _title.text = ((TTOrderItemDTO*)object).product.desc;
        _firstValue.text = ((TTOrderItemDTO*)object).qtyFront;
        _secondValue.text = ((TTOrderItemDTO*)object).qtyBack;
        _thirdValue.text = ((TTOrderItemDTO*)object).qtyOrderSuggested;
        
        
    }
    
    //order
    if (type == 1) {
        _skuLabel.text = ((TTOrderItemDTO*)object).product.sku;
        _title.text = ((TTOrderItemDTO*)object).product.desc;
        [_firstValue setHidden:YES];
        [_secondValue setHidden:YES];
        _thirdValue.text = ((TTOrderItemDTO*)object).qtyOrdered;
    }
    
    //ppp
    if (type == 2) {
        _skuLabel.text = ((TTOrderItemDTO*)object).product.sku;
        _title.text = ((TTOrderItemDTO*)object).product.desc;
        [_firstValue setHidden:YES];
        _secondValue.text = ((TTOrderItemDTO*)object).qtyOrdered;
        _thirdValue.text = ((TTOrderItemDTO*)object).productPull.qty;
        float percentage = ((TTOrderItemDTO*)object).productPull.qty.floatValue/((TTOrderItemDTO*)object).qtyOrdered.floatValue;
        [_thirdValue setBackgroundColor:(percentage >= 0.90f && percentage <= 1.10f) ? [UIColor whiteColor]  : [UIColor yellowColor] ];
        [self setBackgroundColor:(percentage >= 0.90f && percentage <= 1.10f) ? [UIColor whiteColor]  : [UIColor yellowColor] ];
    }
    
    //delivery
    if (type == 3) {
        _skuLabel.text = ((TTOrderItemDTO*)object).product.sku;
        _title.text = ((TTOrderItemDTO*)object).product.desc;
        [_firstValue setHidden:YES];
        [_secondValue setHidden:YES];
        _thirdValue.text = ((TTOrderItemDTO*)object).productDelivery.qtyDelivered;
        if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY) {
            [_thirdValue setBackgroundColor:([((TTOrderItemDTO*)object).productPull.qty isEqualToString:((TTOrderItemDTO*)object).productDelivery.qtyDelivered]) ? [UIColor whiteColor]  : [UIColor yellowColor] ];
            [self setBackgroundColor:([((TTOrderItemDTO*)object).productPull.qty isEqualToString:((TTOrderItemDTO*)object).productDelivery.qtyDelivered]) ? [UIColor whiteColor]  : [UIColor yellowColor] ];
        }
    }
}

-(void)setDataObject:(id)dataObject atVerifyType:(int)verifyType
{
    @try {
        [self setAllValuesBasedOnType:verifyType usingObject:dataObject];
    }
    @catch (NSException *exception) {
        NSLog(@"The data you gave in the table view cell is not what was expected");
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
