//
//  TTSearchCustomerProductResponse.h
//  Houshi
//
//  Created by James Timberlake on 9/16/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTSearchCustomerProductResponse : TTBaseResponse
@property (nonatomic, strong) NSArray* customerProduct;
@end
