//
//  TTSearchPageTableViewCell.h
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTSearchPageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerField;
@property (strong, nonatomic) id dataObject;
-(void)setDataObject:(id)dataObject;
@end
