//
//  TTUserListResponse.m
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTUserListResponse.h"
#import "TTUserDTO.h"

@implementation TTUserListResponse
+ (Class)user_class {
    return [TTUserDTO class];
}
@end
