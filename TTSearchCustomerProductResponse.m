//
//  TTSearchCustomerProductResponse.m
//  Houshi
//
//  Created by James Timberlake on 9/16/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTSearchCustomerProductResponse.h"
#import "TTCustomerProductDTO.h"

@implementation TTSearchCustomerProductResponse
+(Class) customerProduct_class                                                                                                   {
    return [TTCustomerProductDTO class];
}
@end
