//
//  TTUserListResponse.h
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTUserListResponse : TTBaseResponse
@property (nonatomic, strong) NSArray* user;
@end
