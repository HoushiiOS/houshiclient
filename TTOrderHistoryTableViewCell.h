//
//  TTOrderHistoryTableViewCell.h
//  Houshi
//
//  Created by James Timberlake on 9/16/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTOrderHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *histInvAmt;
@property (weak, nonatomic) IBOutlet UILabel *histOrdAmt;
@property (weak, nonatomic) IBOutlet UILabel *histDelAmt;

@end
