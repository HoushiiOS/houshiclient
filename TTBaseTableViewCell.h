//
//  TTBaseTableViewCell.h
//  Houshi
//
//  Created by James Timberlake on 10/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTBaseTableViewCell : UITableViewCell
-(int)getRowIndex;
@end
