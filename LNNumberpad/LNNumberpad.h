/******************************************************************************
 * v. 0.9.5  09 MAY 2013
 * Filename  LNNumberpad.h
 * Project:  LNNumberpad
 * Purpose:  Class to display a custom LNNumberpad on an iPad and properly handle 
 *           the text input.
 * Author:   Louis Nafziger
 *
 * Copyright 2012 - 2013 Louis Nafziger
 ******************************************************************************
 *
 * This file is part of LNNumberpad.
 *
 * COPYRIGHT 2012 - 2013 Louis Nafziger
 *
 * LNNumberpad is free software: you can redistribute it and/or modify
 * it under the terms of the The MIT License (MIT).
 *
 * LNNumberpad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * The MIT License for more details.
 *
 * You should have received a copy of the The MIT License (MIT)
 * along with LNNumberpad.  If not, see <http://opensource.org/licenses/MIT>.
 *
 *****************************************************************************/

#import <UIKit/UIKit.h>

@protocol LNNumberPadDelegate <NSObject>
-(void)onSpecialKeyPressed:(NSString*)keyName fromTextField:(UITextField*)textField;
-(void)onTextFieldValueChanged:(UITextField*)textField;
@end

@interface LNNumberpad : UIView

// The one and only LNNumberpad instance you should ever need:
+ (LNNumberpad *)defaultLNNumberpad;
+ (LNNumberpad *)hexLNNumberpad;
-(void)setCurrentTextField:(UITextField*)textField;
-(void)setHexNumberPad;
-(void)setNumberPadByInt:(int)amt;
@property BOOL isHexNumberPad;
@property (nonatomic, strong) NSMutableArray* numberArry;
@property (weak, atomic) id<LNNumberPadDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIButton* number0;
@property (nonatomic, strong) IBOutlet UIButton* number1;
@property (nonatomic, strong) IBOutlet UIButton* number2;
@property (nonatomic, strong) IBOutlet UIButton* number3;
@property (nonatomic, strong) IBOutlet UIButton* number4;
@property (nonatomic, strong) IBOutlet UIButton* number5;
@property (nonatomic, strong) IBOutlet UIButton* number6;
@property (nonatomic, strong) IBOutlet UIButton* number7;
@property (nonatomic, strong) IBOutlet UIButton* number8;
@property (nonatomic, strong) IBOutlet UIButton* number9;
@property (nonatomic, strong) IBOutlet UIButton* number10;
@property (nonatomic, strong) IBOutlet UIButton* number11;
@property (nonatomic, strong) IBOutlet UIButton* number12;
@property (nonatomic, strong) IBOutlet UIButton* number13;
@property (nonatomic, strong) IBOutlet UIButton* number14;
@property (nonatomic, strong) IBOutlet UIButton* number15;
@property (nonatomic, strong) IBOutlet UIButton* number16;
@property (nonatomic, strong) IBOutlet UIButton* number17;
@property (nonatomic, strong) IBOutlet UIButton* number18;
@property (nonatomic, strong) IBOutlet UIButton* number19;
@property (nonatomic, strong) IBOutlet UIButton* number20;
@property (nonatomic, strong) IBOutlet UIButton* number21;
@property (nonatomic, strong) IBOutlet UIButton* number22;
@property (nonatomic, strong) IBOutlet UIButton* number23;
@end
