//
//  TTHoushiApi.h
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTBaseResponse.h"
#import "TTUserDTO.h"
#import "TTContactDTO.h"

#define ORDER_STATE_INVENTORY_CREATED 1 
#define ORDER_STATE_INVENTORY_COMPLETED 2
#define ORDER_STATE_ORDER_COMPLETED 3
#define ORDER_STATE_PPP_COMPLETED 4
#define ORDER_STATE_DELIVERY_COMPLETED 5
#define ORDER_STATE_INCOMPLETE 6

@protocol TTHoughiApiDelegate
-(void)onOperationSuccess:(TTBaseResponse*)response;
-(void)onOperationError:(TTBaseResponse*)error;
-(void)alertConfirmClicked;
@end


@interface TTHoushiApi : NSObject <UIAlertViewDelegate>
@property (nonatomic, strong) TTUserDTO* currentUser;
@property (nonatomic, strong) id<TTHoughiApiDelegate> delegate;

+(TTHoushiApi*)getInstance;
+(void)setDelegate:(id)delegate;
+(void)resetCustomerSchedule;
+(void)loginWithUsername:(NSString*)userName andPassword:(NSString*)password;
+(void)logout;
+(void)showUserList;
+(void)showOrderList;
+(void)showInventoryList;
+(void)showCustomerList;
+(void)closeOrder;
+(void)getPPPItems;
+(void)getOrderItems;
+(void)getCustomerProductByProductId:(int)productId;
+(void)getCustomerContactsWithCustomerId:(int)custId;
+(void)saveContactToCustomerWithId:(int)custId AndContact:(id)contact AndPrimary:(BOOL)isPrimary;
+(void)deleteContactWithId:(int)contactId andCustomerId:(int)customerId;
+(void)saveNotes:(NSArray*)notes;
+(void)saveOrder:(int)orderState;
+(void)savePullStatusWithNumberOfBoxes:(int)number;
+(void)saveDeliveryStatustatusWithDelNumber:(NSString*)deliveryNumber;
+(void)createOrderWithCustomerId:(int)customerId isQuickOrder:(BOOL)isQOrder withCustomerScheduleId:(int)custSchedId;
+(void)createInvoice;
+(void)createCredit;
+(void)isSavedOrderValidWithOrderId:(NSNumber*)orderId andOrderState:(int)state;
//JT - I completely changed this so it wouldn't ave to rely on a delegate
-(void)showConfirmAlert:(NSString *)message withNoMessage:(NSString*)noText andYesText:(NSString*)yesText performingTheResutInBlock:( void ( ^ )(  ) )success;
-(void)showConfirmAlert:(NSString *)message withNoMessage:(NSString *)noText andYesText:(NSString *)yesText performingTheResutInBlock:(void (^)())success andFailure:(void (^)())failure;
-(void)showAlertWithOnlyOkButton:(NSString *)message performingTheResutInBlock:(void (^)())success andFailure:(void (^)())failure;
@end
