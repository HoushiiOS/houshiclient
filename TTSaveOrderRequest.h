//
//  TTSaveOrderRequest.h
//  Houshi
//
//  Created by James Timberlake on 8/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTSaveOrderRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* customerId;
@property (nonatomic, strong) NSNumber* orderId;
@property (nonatomic, strong) NSNumber* orderState;
@property (nonatomic, strong) NSMutableArray* orderItem;
@property (nonatomic, strong) NSNumber* numBoxes;
@property (nonatomic, strong) NSString* orderName;
@property (nonatomic, strong) NSString* eventName;
@property (nonatomic, strong) NSString* occupancyRate;
@property (nonatomic, strong) NSString* purchaseOrderNumber;
@end
