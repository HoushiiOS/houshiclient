//
//  TTInvSingleHeaderView.m
//  Houshi
//
//  Created by James Timberlake on 6/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTInvAllSectionView.h"

static BOOL skuAscending;
static BOOL descAscending;
static BOOL itemAscending;
static BOOL item2Ascending;
static BOOL item3Ascending;
static int headerActiveLabelIndex = 0;

@interface TTInvAllSectionView()
{
    BOOL inMainThread;
}
@property (nonatomic,strong) UILabel* activeLabel;
@end
@implementation TTInvAllSectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)init{
    self = [super init];
    if(self){
        headerActiveLabelIndex = 0;
        NSArray *nibObjects;
        nibObjects = [[NSBundle mainBundle] loadNibNamed:@"TTInvAllSectionView" owner:self options:nil];
        
        self = [nibObjects objectAtIndex:0];
        //  UIView* nibView = nibObjects[0];
           // [self setFrame:nibView.frame]; //Vishal - Issue solved
        
        UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        UITapGestureRecognizer* tapRec2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap2:)];
        UITapGestureRecognizer* tapRec3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap3:)];
        UITapGestureRecognizer* tapRec4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap4:)];
        UITapGestureRecognizer* tapRec5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap5:)];
      
        [_skuTitle addGestureRecognizer:tapRec];
        [_descTitle addGestureRecognizer:tapRec2];
        [_itemType1 addGestureRecognizer:tapRec3];
        [_itemType2 addGestureRecognizer:tapRec4];
        [_itemType3 addGestureRecognizer:tapRec5];
        
        [self setUserInteractionEnabled:YES];
     //   [self addSubview:nibView];

        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setActiveColor{
    if (!inMainThread) {
        inMainThread = YES;
        [self performSelectorOnMainThread:@selector(setActiveColor) withObject:nil waitUntilDone:YES];
    }
    if (headerActiveLabelIndex == 0) {
        [_skuTitle setBackgroundColor:[UIColor blueColor]];
    }else if(headerActiveLabelIndex == 1){
        [_descTitle setBackgroundColor:[UIColor blueColor]];
    }else if(headerActiveLabelIndex == 2){
        [_itemType1 setBackgroundColor:[UIColor blueColor]];
    }else if(headerActiveLabelIndex == 3){
        [_itemType2 setBackgroundColor:[UIColor blueColor]];
    }else if(headerActiveLabelIndex == 4){
        [_itemType3 setBackgroundColor:[UIColor blueColor]];
    }
    inMainThread = NO;
}


//sku
-(void)onTap:(UITapGestureRecognizer*)sender
{
    headerActiveLabelIndex =0;
    skuAscending = (skuAscending != YES);
    if (self.delegate) {
        [self.delegate onItemWithColumnType:@"product.warehouseBin" inAscendingOrder:skuAscending];
    }
    
}

//description
-(void)onTap2:(UITapGestureRecognizer*)sender
{
    headerActiveLabelIndex =1;
    descAscending = (descAscending != YES);
    if (self.delegate) {
        [self.delegate onItemWithColumnType:@"product.desc" inAscendingOrder:descAscending];
    }
}


//item
-(void)onTap3:(UITapGestureRecognizer*)sender
{
    headerActiveLabelIndex =2;
    itemAscending = (itemAscending != YES);
    if (self.delegate) {
        [self.delegate onItemWithColumnType:@"Quantity" inAscendingOrder:itemAscending];
    }
}

//description
-(void)onTap4:(UITapGestureRecognizer*)sender
{
    headerActiveLabelIndex =3;
    item2Ascending = (item2Ascending != YES);
    if (self.delegate) {
        [self.delegate onItemWithColumnType:@"Quantity2" inAscendingOrder:item2Ascending];
    }
}


//item
-(void)onTap5:(UITapGestureRecognizer*)sender
{
    headerActiveLabelIndex =4;
    item3Ascending = (item3Ascending != YES);
    if (self.delegate) {
        [self.delegate onItemWithColumnType:@"Quantity3" inAscendingOrder:item3Ascending];
    }
}

-(void)setTitleActive:(UILabel*)selectedLabel{
    if (selectedLabel)
        [selectedLabel setBackgroundColor:[UIColor blueColor]];
    _activeLabel = selectedLabel;
}

-(void)setTitleInActive:(UILabel*)selectedLabel{
    if(selectedLabel)
        [selectedLabel setBackgroundColor:[UIColor clearColor]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
