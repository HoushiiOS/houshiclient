//
//  TTSavePulledItems.h
//  Houshi
//
//  Created by James Timberlake on 10/6/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTSavePulledItemsRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* orderId;
@property (nonatomic, strong) NSMutableArray* productPulled;
@property (nonatomic, strong) NSNumber* numBoxes;
//false by default
@property (nonatomic, strong) NSNumber* adminEdit;
@end
