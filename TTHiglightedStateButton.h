//
//  TTHiglightedStateButton.h
//  Houshi
//
//  Created by James Timberlake on 9/4/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HighlightedButtonDelegate
-(void)onButtonHighlightToggle:(BOOL)isHighlighted;
@end
@interface TTHiglightedStateButton : UIButton
@property (nonatomic, weak) id<HighlightedButtonDelegate> delegate;
-(BOOL)getSelected;
-(void)toggleHighlight:(id)sender;
@property (nonatomic) BOOL isHighlighted;
@property (nonatomic) BOOL isHeader;
@property (nonatomic, strong) UIImage* nonHighlightedImage;
@property (nonatomic, strong) UIImage* highlightedImage;
@end
