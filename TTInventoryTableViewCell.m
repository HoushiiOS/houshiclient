//
//  TTInventoryTableViewCell.m
//  Houshi
//
//  Created by James Timberlake on 6/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTInventoryTableViewCell.h"
#import "TTOrderItemDTO.h"
#import "TTAppConatants.h"

@implementation TTInventoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setDataObject:(id)dataObject
{
    [_skuField setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_headerField setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_frontField setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_backField setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_orderField setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    
    if ([dataObject isKindOfClass:[TTOrderItemDTO class]]) {
       
        _dataObject = dataObject;
        _skuField.text = ((TTOrderItemDTO*)dataObject).product.sku;
        _headerField.text = ((TTOrderItemDTO*)dataObject).product.desc;
        _frontField.text = ((TTOrderItemDTO*)dataObject).qtyFront;
        _backField.text = ((TTOrderItemDTO*)dataObject).qtyBack;
        _orderField.text = ((TTOrderItemDTO*)dataObject).qtyOrderSuggested;
    }
    
    _backField.tag = 1;
    _orderField.tag = 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(int)getRowIndex{
    return _rowIndex;
}

-(void)setRowIndex:(int)rowIndex{
    _rowIndex = rowIndex;
}

@end
