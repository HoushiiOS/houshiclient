//
//  TTInventoryListResponse.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTInventoryListResponse : TTBaseResponse
@property (nonatomic, strong) NSArray* customerSchedule;
@end
