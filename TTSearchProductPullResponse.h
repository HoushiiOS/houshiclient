//
//  TTSearchProductPullResponse.h
//  Houshi
//
//  Created by James Timberlake on 10/6/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTSearchProductPullResponse : TTBaseResponse
@property (nonatomic, strong) NSArray* ProductPull;
@end
