//
//  TTSaveDeliveryRequest.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTSaveDeliveryRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* customerId;
@property (nonatomic, strong) NSNumber* orderId;
@property (nonatomic, strong) NSString* confirmationName;
@property (nonatomic, strong) NSString* confirmationEmail;
@property (nonatomic, strong) NSString* confirmationSignature;
@property (nonatomic, strong) NSString* deliveryNumber;
@property (nonatomic, strong) NSMutableArray* deliveredItems;
@property (nonatomic, strong) NSNumber* adminEdit;//default is false
@end
