//
//  TTVerifyPageTableViewCell.h
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTVerifyPageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UITextField *thirdValue;
@property (weak, nonatomic) IBOutlet UILabel *skuLabel;
@property  int rowIndex;
@property (weak, nonatomic) IBOutlet UITextField *secondValue;
@property (weak, nonatomic) IBOutlet UITextField *firstValue;
-(void)setDataObject:(id)dataObject atVerifyType:(int)verifyType;
@end
