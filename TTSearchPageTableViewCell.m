//
//  TTSearchPageTableViewCell.m
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTSearchPageTableViewCell.h"
#import "TTCustomerScheduleDTO.h"
#import "TTAppConatants.h"
#import "TTOrderDTO.h"

@implementation TTSearchPageTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setDataObject:(id)dataObject
{
    [_headerField setFont:[UIFont systemFontOfSize:[TTAppConatants orderItemFontSize]]];
    
    if([dataObject isKindOfClass:[TTCustomerScheduleDTO class]])
    {
        [self setDataObject:((TTCustomerScheduleDTO*)dataObject).customer];
    }
    
    if([dataObject isKindOfClass:[TTOrderDTO class]])
    {
        _dataObject = ((TTOrderDTO*)dataObject).customer;
        NSString* orderProcessing = (((TTOrderDTO*)dataObject).workingUserId && ![((TTOrderDTO*)dataObject).workingUserId isEqualToString:@""]) ? @"**IN USE**" : @"";
        _headerField.text = (((TTOrderDTO*)dataObject).customerName || [((TTOrderDTO*)dataObject).customerName isEqualToString:@""]) ? [orderProcessing stringByAppendingFormat:@"%@",((TTOrderDTO*)dataObject).customerName ] : [orderProcessing stringByAppendingFormat:@" %@%@",((TTOrderDTO*)dataObject).customerName , ((TTOrderDTO*)dataObject).createdAt];
    }
    
    if ([dataObject isKindOfClass:[TTCustomerDTO class]]) {
        _dataObject = dataObject;
        _headerField.text = ((TTCustomerDTO*)dataObject).name;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
