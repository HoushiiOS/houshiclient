//
//  TTOrderReport.h
//  Houshi
//
//  Created by James Timberlake on 10/9/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTProductPullDTO.h"
#import "TTProductDeliveryItem.h"

@interface TTOrderReport : TTBaseResponse
@property (nonatomic, strong) TTProductPullDTO* productPull;
@property (nonatomic, strong) TTProductDeliveryItem* productDelivery;
@end
