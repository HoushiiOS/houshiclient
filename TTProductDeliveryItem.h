//
//  TTProductDeliveryItem.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTProductDTO.h"

@interface TTProductDeliveryItem : TTBaseResponse
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* productDeliveryId;
@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSString* orderItemId;
@property (nonatomic, strong) NSString* qtyDelivered;
@property (nonatomic, strong) NSString* changeReason;
@property (nonatomic, strong) TTProductDTO* product;
@property (nonatomic, strong) NSString* productPricingTier;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@end
