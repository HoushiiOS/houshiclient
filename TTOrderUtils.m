//
//  TTOrderUtils.m
//  Houshi
//
//  Created by James Timberlake on 10/5/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTOrderUtils.h"
#import "TTOrderItemDTO.h"
#import "TTOrderHistoryDTO.h"

@implementation TTOrderUtils

+(void)saveOrder:(TTOrderDTO*)order toDirectoryType:(NSString*)directoryName
{
//    NSLog(@"%@",order.toDictionary);
    
    NSMutableDictionary *dictOrder = order.toDictionary.mutableCopy;
    NSString *fileName = [NSString stringWithFormat:@"%@%@.json",order.customerName,order.createdAt];
//    [NSString stringWithFormat:@"Order_%@.json",order.id];

    if ([dictOrder valueForKey:@"note"]) {
        
        NSArray *arrNote = [dictOrder valueForKey:@"note"];
        NSMutableArray *arrDictNote = [[NSMutableArray alloc]init];
        for (TTOrderNoteDTO *objNote in arrNote) {
            
            NSDictionary *dictNote = [objNote toDictionary];
            [arrDictNote addObject:dictNote];
        }
        
        [dictOrder setObject:arrDictNote forKey:@"note"];
    }
    
    if ([dictOrder valueForKey:@"orderItem"]) {
        
        NSArray *arrItem = [dictOrder valueForKey:@"orderItem"];
        NSMutableArray *arrDictItem = [[NSMutableArray alloc]init];
        for (TTOrderItemDTO *objItem in arrItem) {
            
            NSMutableDictionary *dictNote = [NSMutableDictionary dictionaryWithDictionary:[objItem toDictionary]];
            NSMutableArray* ordHist = [NSMutableArray new];
            for (TTOrderHistoryDTO* historyItem in objItem.OrderHistory) {
                [ordHist addObject:[historyItem toDictionary]];
            }
            [dictNote setObject:ordHist forKey:@"OrderHistory"];
            [arrDictItem addObject:dictNote];
        }
        
        [dictOrder setObject:arrDictItem forKey:@"orderItem"];
    }
    
    
    NSArray *arrOrder = [NSArray arrayWithObjects:dictOrder, nil];
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrOrder options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    BOOL success = [self writeJson:jsonData toDirectory:directoryName toFile:fileName];
    
    NSLog(@"JSON Output file written : %d  JSON STRING is %@ ", success,jsonString);

}

+(NSArray*)getOrderArrayFromDirectoryType:(NSString*)directoryName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",directoryName]];

    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
   
    NSMutableArray *arrTTOrderObjs = [[NSMutableArray alloc]init];
    
    // Parse Each file and store TTOrderDTO objs in above array
    for (int i =0; i<directoryContent.count; i++) {
        
        NSString *jsonFilePath = [NSString stringWithFormat:@"%@/%@", dataPath,[directoryContent objectAtIndex:i]];
        NSError *error;
        NSData *jsonData = [NSData dataWithContentsOfFile:jsonFilePath options:kNilOptions error:&error ];
        NSArray *arrJson = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];

        NSMutableDictionary *dictOrder = [[arrJson objectAtIndex:0] mutableCopy];

        NSArray *arrItem = [dictOrder valueForKey:@"orderItem"];
        NSMutableArray *arrOrderItem = [[NSMutableArray alloc]init];

        for (id objItem in arrItem) {
            
            TTOrderItemDTO *dictOrderItem = [[TTOrderItemDTO alloc]initWithDictionary:objItem];
            [arrOrderItem addObject:dictOrderItem];
        }
        
        [dictOrder setObject:arrOrderItem forKey:@"orderItem"];

        arrItem = [dictOrder valueForKey:@"note"]; //--- REUSING OBJECTS ---
        arrOrderItem = [[NSMutableArray alloc]init];
        
        for (id objItem in arrItem) {
            
            TTOrderNoteDTO *dictOrderNote = [[TTOrderNoteDTO alloc]initWithDictionary:objItem];
            [arrOrderItem addObject:dictOrderNote];
        }
        
        [dictOrder setObject:arrOrderItem forKey:@"note"];
        
        // Form a TTOrderDTO object and add
        TTOrderDTO *orderDTO = [[TTOrderDTO alloc]initWithDictionary:dictOrder];
        [arrTTOrderObjs addObject:orderDTO];
        
    }
    
    
    //if (arrTTOrderObjs.count >0) {
        
        return arrTTOrderObjs;
   // }
    //return nil;
}

+(BOOL)deleteOrder:(TTOrderDTO*)order inDirectory:(NSString*)directoryName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *directoryPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",directoryName]];
    NSString *fileName = [NSString stringWithFormat:@"Order_%@.json",order.id];

    NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        NSError *error;
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        if (success) {
            
            NSLog(@"Deleted file %@ ",fileName);
            return true;
        }
        else
        {
            NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
            return false;
        }
    
    }else{
        
        //File Do not exist
        
        return true;
    }
    
    
    return false;
}

+(BOOL)writeJson:(NSData *)jsonData toDirectory:(NSString *)directory toFile:(NSString *)fileName
{
    //applications Documents dirctory path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",directory]];
    
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
    //make a file name to write the data to using the documents directory:
    
    BOOL success =  [[NSFileManager defaultManager] createFileAtPath:filePath contents:jsonData attributes:nil];
    return success;
}

@end
