//
//  TTOrderHistoryDTO.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTOrderHistoryDTO : TTBaseResponse
@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSString* qtyOrdered;
@property (nonatomic, strong) NSString* qtyFront;
@property (nonatomic, strong) NSString* qtyBack;
@property (nonatomic, strong) NSString* qtyDelivered;
@end
