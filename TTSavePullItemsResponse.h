//
//  TTSavePullItemsResponse.h
//  Houshi
//
//  Created by James Timberlake on 10/6/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTSavePullItemsResponse : TTBaseResponse
@property NSNumber* success;
@end
