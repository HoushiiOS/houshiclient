
//
//  TTValidOrderResponse.h
//  Houshi
//
//  Created by James Timberlake on 12/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#ifndef Houshi_TTValidOrderResponse_h
#define Houshi_TTValidOrderResponse_h

#import "TTBaseResponse.h"

@interface TTValidOrderResponse : TTBaseResponse

@property (nonatomic, strong) NSNumber* isValid;

@end

#endif
