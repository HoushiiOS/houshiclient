//
//  TTSaveCustomerContactResponse.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTSaveCustomerContactResponse : TTBaseResponse
@property NSNumber* success;
@end
