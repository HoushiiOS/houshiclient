//
//  TTStartSessionRequest.h
//  Houshi
//
//  Created by JT on 5/16/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTBaseRequest.h"

@interface TTStartSessionRequestParams : TTBaseRequest
@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* pin;
@end
