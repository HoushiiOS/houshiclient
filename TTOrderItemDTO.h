//
//  TTOrderItem.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTProductDTO.h"
#import "TTProductPullDTO.h"
#import "TTProductDeliveryItem.h"
#import "TTOrderReport.h"

@interface TTOrderItemDTO : TTBaseResponse
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSString* customerId;
@property (nonatomic, strong) NSString* customerProductId;
@property (nonatomic, strong) NSString* productId;
@property (nonatomic, strong) NSString* qtyOrdered;
@property (nonatomic, strong) NSString* qtyOrderSuggested;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* qtyFront;
@property (nonatomic, strong) NSString* qtyBack;
@property (nonatomic, strong) NSString* frontStock;
@property (nonatomic, strong) NSString* backStock;
@property (nonatomic, strong) TTProductDTO* product; // the product object for this OrderItem
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@property (nonatomic, strong) NSArray* OrderHistory;
@property (nonatomic, strong) TTOrderReport* OrderReport;
@property (nonatomic, strong) TTProductPullDTO* productPull;
@property (nonatomic, strong) TTProductDeliveryItem* productDelivery;
@end
