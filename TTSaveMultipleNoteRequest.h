//
//  TTSaveMultipleNoteRequest.h
//  Houshi
//
//  Created by James Timberlake on 9/9/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTSaveMultipleNoteRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* orderId;
@property (nonatomic, strong) NSMutableArray* notes;
@end
