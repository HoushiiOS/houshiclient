//
//  TTCustomerDTO.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTCustomerDTO : TTBaseResponse
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* quickbookId;
@property (nonatomic, strong) NSString* vendorId;
@property (nonatomic, strong) NSString* messageTypeId;
@property (nonatomic, strong) NSString* secondaryMessageTypeId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* storeName;
@property (nonatomic, strong) NSString* dba;
@property (nonatomic, strong) NSString* careof;
@property (nonatomic, strong) NSString* address1;
@property (nonatomic, strong) NSString* address2;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* zipcode;
@property (nonatomic, strong) NSString* zipcode4;
@property (nonatomic, strong) NSString* mailAddress1;
@property (nonatomic, strong) NSString* mailAddress2;
@property (nonatomic, strong) NSString* mailCity;
@property (nonatomic, strong) NSString* mailState;
@property (nonatomic, strong) NSString* mailZipcode;
@property (nonatomic, strong) NSString* mailZipcode4;
@property (nonatomic, strong) NSString* locationName;
@property (nonatomic, strong) NSString* pricingTier;
@property (nonatomic, strong) NSString* sku;
@property (nonatomic, strong) NSString* quickbookAccountNumber;
@property (nonatomic, strong) NSString* isPriced;
@property (nonatomic, strong) NSString* isPacked;
@property (nonatomic, strong) NSString* isFastChecked;
@property (nonatomic, strong) NSString* useUniversalScreen;
@property (nonatomic, strong) NSString* sendNutshellSchedule;
@property (nonatomic, strong) NSString* sigRequired;
@property (nonatomic, strong) NSString* storeNumber;
@property (nonatomic, strong) NSString* printNameAs;
@property (nonatomic, strong) NSString* deliveryMadeBy;
@property (nonatomic, strong) NSString* inventoryDisplayOrder;
@property (nonatomic, strong) NSString* orderDisplayOrder;
@property (nonatomic, strong) NSString* pullDisplayOrder;
@property (nonatomic, strong) NSString* deliveryDisplayOrder;
@property (nonatomic, strong) NSString* inventoryEmail;
@property (nonatomic, strong) NSString* secondaryEmail;
@property (nonatomic, strong) NSString* inventoryFrequency;
@property (nonatomic, strong) NSString* nextInventoryDate;
@property (nonatomic, strong) NSString* currentInventoryDate;
@property (nonatomic, strong) NSString* scheduleId;
@property (nonatomic, strong) NSString* active;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@property (nonatomic, strong) NSString* skuId;
@end
