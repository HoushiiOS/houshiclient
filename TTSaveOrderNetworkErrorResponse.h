//
//  TTSaveOrderNetworkError.h
//  Houshi
//
//  Created by James Timberlake on 11/28/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#ifndef Houshi_TTSaveOrderNetworkError_h
#define Houshi_TTSaveOrderNetworkError_h

#import "TTBaseResponse.h"

@interface TTSaveOrderNetworkErrorResponse : TTBaseResponse

@end

#endif
