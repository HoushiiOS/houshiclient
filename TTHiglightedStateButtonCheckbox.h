//
//  TTHiglightedStateButton.h
//  Houshi
//
//  Created by James Timberlake on 9/4/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HighlightedButtonCheckboxDelegate
-(void)onButtonHighlightToggle:(BOOL)isHighlighted;
@end
@interface TTHiglightedStateButtonCheckbox : UIImageView
@property (nonatomic, weak) id<HighlightedButtonCheckboxDelegate> delegate;
-(void)setHighlightModeTo:(BOOL)isHighlighted;
@end
