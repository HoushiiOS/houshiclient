//
//  TTCustomerContactDTO.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTCustomerContactDTO : TTBaseResponse
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* customerId;
@property (nonatomic, strong) NSString* contactId;
@property (nonatomic, strong) NSString* primary;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@end
