//
//  TTSearchCustomerResponse.h
//  Houshi
//
//  Created by James Timberlake on 10/8/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTSearchCustomerResponse : TTBaseResponse
@property (nonatomic, strong) NSArray* customer;
@end
