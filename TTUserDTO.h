//
//  TTUserDTO.h
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTUserDTO : TTBaseResponse
@property (nonatomic,strong) NSString* active;
@property (nonatomic,strong) NSString* createdAt;
@property (nonatomic,strong) NSString* email;
@property (nonatomic,strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* updatedAt;
@property (nonatomic, strong) NSString* userRoleId;
@property (nonatomic, strong) NSString* username;
@end
