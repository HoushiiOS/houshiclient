//
//  TTCloseOrderRequest.h
//  Houshi
//
//  Created by James Timberlake on 11/22/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTCloseOrderRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* orderId;

@end
