//
//  TTOrderDTO.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTCustomerDTO.h"
#import "TTOrderNoteDTO.h"
#import "TTContactDTO.h"


@interface TTOrderDTO : TTBaseResponse
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* customerId;
@property (nonatomic, strong) NSString* customerScheduleId;
@property (nonatomic, strong) NSString* customerName;
@property (nonatomic, strong) NSString* customerSku;
@property (nonatomic, strong) NSString* eventName;
@property (nonatomic, strong) NSString* occupancyRate;
@property (nonatomic, strong) TTCustomerDTO* customer;
@property (nonatomic, strong) NSString* parentOrder;
@property (nonatomic, strong) NSString* workingUserId;
@property (nonatomic, strong) NSString* workingStart;
@property (nonatomic, strong) NSString* statusId;
@property (nonatomic, strong) NSString* numBoxes;
@property (nonatomic, strong) NSString* invoiceNumber;
@property (nonatomic, strong) NSString* tobInvoiceNumber;
@property (nonatomic, strong) NSString* purchaseOrderNumber;
@property (nonatomic, strong) NSString* inventoryBy;
@property (nonatomic, strong) NSString* orderBy;
@property (nonatomic, strong) NSString* pullBy;
@property (nonatomic, strong) NSString* deliveryBy;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@property (nonatomic,strong) NSArray* orderItem;
//@property (nonatomic,strong) NSMutableArray* productPull;
//@property (nonatomic,strong) NSMutableArray* productDeliveryItem;
@property (nonatomic,strong) NSMutableArray* note;

@property (nonatomic, strong) NSString* saved_Deliverynumber;
@property (nonatomic, strong) NSString* saved_DeliverySignature;
@property (nonatomic,strong) TTContactDTO* saved_DeliveryContact;
@end
