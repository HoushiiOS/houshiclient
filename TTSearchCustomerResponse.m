//
//  TTSearchCustomerResponse.m
//  Houshi
//
//  Created by James Timberlake on 10/8/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTSearchCustomerResponse.h"
#import "TTCustomerDTO.h"   
@implementation TTSearchCustomerResponse
+(Class) customer_class                                                                                                   {
    return [TTCustomerDTO class];
}
@end
