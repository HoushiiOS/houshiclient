//
//  TTInventoryListResponse.m
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTInventoryListResponse.h"
#import "TTCustomerScheduleDTO.h"

@implementation TTInventoryListResponse
+(Class) customerSchedule_class                                                                                                   {
    return [TTCustomerScheduleDTO class];
}
@end
