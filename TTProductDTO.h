//
//  TTProductDTO.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTProductDTO : TTBaseResponse
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* quickbookId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSString* shortDesc;
@property (nonatomic, strong) NSString* sku;
@property (nonatomic, strong) NSString* modelNumber;
@property (nonatomic, strong) NSString* packageType;
@property (nonatomic, strong) NSString* warehouseBin;
@property (nonatomic, strong) NSString* nutshellBin;
@property (nonatomic, strong) NSString* upc;
@property (nonatomic, strong) NSString* manufacturerNum;
@property (nonatomic, strong) NSString* category;
@property (nonatomic, strong) NSString* brand;
@property (nonatomic, strong) NSString* productDescription;
@property (nonatomic, strong) NSString* weight;
@property (nonatomic, strong) NSString* warningsLabels;
@property (nonatomic, strong) NSString* availableDate;
@property (nonatomic, strong) NSString* unavailableDate;
@property (nonatomic, strong) NSString* inventoryAmt;
@property (nonatomic, strong) NSString* active;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@end
