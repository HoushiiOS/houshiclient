//
//  TTSaveCustomerContactRequest.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"
#import "TTContactDTO.h"

@interface TTSaveCustomerContactRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* customerId;
@property (nonatomic, strong) NSNumber* primaryContact; //Is this a primary contact for the customer. //default is false
@property (nonatomic, strong) NSDictionary* contact;// A contact object
@end
