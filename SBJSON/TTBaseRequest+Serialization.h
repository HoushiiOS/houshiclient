//
//  TTBaseRequest+Serialization.h
//  Houshi
//
//  Created by JT on 5/17/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTBaseRequest.h"

@interface TTBaseRequest (TTBaseRequest_Serialization)
+ (id)objectFromDictionary:(NSDictionary*)dictionary;

- (id)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary*)toDictionary;

- (NSDictionary *)map;

@end
