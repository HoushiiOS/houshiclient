//
//  NSString+NSString_Capitalized.h
//  Houshi
//
//  Created by Vishal on 26/09/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_Capitalized)
+ (NSString *)capitalizeString:(NSString *)text;
@end
