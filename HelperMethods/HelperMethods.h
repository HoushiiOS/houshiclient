//
//  HelperMethods.h
//  Houshi
//
//  Created by Vishal on 21/09/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelperMethods : NSObject

+(NSString *)convertImageToStringData:(UIImage *)image;
+(UIImage *)imageFromString:(NSString *)image;
+(void)convertCapitalForView:(UIView *)viewContainer;
@end
