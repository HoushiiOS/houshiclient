//
//  HelperMethods.m
//  Houshi
//
//  Created by Vishal on 21/09/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "HelperMethods.h"
#import "Base64.h"

@implementation HelperMethods


+(NSString *)convertImageToStringData:(UIImage *)image{
    
    NSData *imageData;
    
    imageData= UIImagePNGRepresentation(image);
    
    if (!imageData) {
        
        imageData= UIImageJPEGRepresentation(image, 0.0);
        
    }
    
    NSString  *strImageData;// = [[NSString alloc] initWithData:imageData encoding:NSDataBase64EncodingEndLineWithCarriageReturn];
    strImageData  = [Base64 encode:imageData];//  [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    //strImageData = [[NSString alloc] initWithData:imageData encoding:NSDataBase64EncodingEndLineWithLineFeed];
    //strImageData = [[NSString alloc] initWithData:imageData encoding:NSUTF8StringEncoding];
    return strImageData;
    
}

+(UIImage *)imageFromString:(NSString *)image{
    if (!image) {
        return nil;
    }
    NSData *data = [Base64 decode:image];
    if (!data ) {
        @try {
            data = [Base64 decode:[image substringToIndex:(image.length - 3)]];
        }
        @catch (NSException *exception) {
            
        }
    }
//    NSData *data = [[NSData alloc]initWithBase64EncodedString:image options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
    
}

+(void)convertCapitalForView:(UIView *)viewContainer{
    
   
    for (UIView *view in viewContainer.subviews) {
        
        if ([view isKindOfClass:[UILabel class]]) {
            
            UILabel *vwLabel = (UILabel *)view;
            vwLabel.text = [NSString capitalizeString:vwLabel.text];
            vwLabel.numberOfLines = 1;
            vwLabel.minimumScaleFactor = 8./vwLabel.font.pointSize;
            vwLabel.adjustsFontSizeToFitWidth = YES;
        }
        else if ([view isKindOfClass:[UIButton class]]){
            
            UIButton *vwBtn = (UIButton *)view;
            vwBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [vwBtn setTitle:[NSString capitalizeString:vwBtn.titleLabel.text] forState:UIControlStateNormal];
            [vwBtn setTitle:[NSString capitalizeString:vwBtn.titleLabel.text] forState:UIControlStateSelected];
            [vwBtn setTitle:[NSString capitalizeString:vwBtn.titleLabel.text] forState:UIControlStateHighlighted];
           
        }
        
        else if ([view isKindOfClass:[UIView class]]){
        
            [HelperMethods convertCapitalForView:view];
        }
    }
}


@end
