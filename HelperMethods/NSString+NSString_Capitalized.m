//
//  NSString+NSString_Capitalized.m
//  Houshi
//
//  Created by Vishal on 26/09/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "NSString+NSString_Capitalized.h"

@implementation NSString (NSString_Capitalized)


+(NSString *)capitalizeString:(NSString *)text{
    
    return [text uppercaseString]; // or just return mutableSelf
}
@end
