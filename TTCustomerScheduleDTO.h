//
//  TTCustomerScheduleDTO.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTCustomerDTO.h"

@interface TTCustomerScheduleDTO : TTBaseResponse
@property (nonatomic,strong) NSString* id;
@property (nonatomic,strong) NSString* customerId;
@property (nonatomic,strong) NSString* scheduleId;
@property (nonatomic,strong) NSString* date;
@property (nonatomic,strong) NSString* active;
@property (nonatomic,strong) NSString* createdAt;
@property (nonatomic,strong) NSString* updatedAt;
@property (nonatomic,strong) TTCustomerDTO* customer;
@end
