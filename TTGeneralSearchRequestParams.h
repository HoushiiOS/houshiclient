//
//  TTGeneralSearchRequestParams.h
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTGeneralSearchRequestParams : TTBaseRequest
@property (nonatomic,strong) NSString* sessionToken;
@property (nonatomic,strong) NSArray* searchField;
@property (nonatomic,strong) NSArray* searchValue;
@property (nonatomic,strong) NSNumber* offset;
@property (nonatomic,strong) NSNumber* limit;
@property (nonatomic,strong) NSArray* returnFields;
@property (nonatomic,strong) NSArray* orderByFields;
@property (nonatomic,strong) NSArray* orderByDirection;
@end
