//
//  ErrorHandlingFromApiResponse.h
//  Houshi
//
//  Created by Vishal on 02/09/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorHandlingFromApiResponse : NSObject
+(NSDictionary *)checkErrorInResponse:(NSDictionary *)dictResponse;
+(UIAlertView *)displayErrorAlert:(NSString *)message;

@end
