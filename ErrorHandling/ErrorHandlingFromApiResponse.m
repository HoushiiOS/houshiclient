//
//  ErrorHandlingFromApiResponse.m
//  Houshi
//
//  Created by Vishal on 02/09/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "ErrorHandlingFromApiResponse.h"

@implementation ErrorHandlingFromApiResponse

+(NSDictionary *)checkErrorInResponse:(NSDictionary *)dictResponse{
    
    NSString *message;
    NSNumber *errorOccured;
    if (dictResponse) {
       
        NSDictionary *dictError = [dictResponse valueForKey:@"error"];
        if (dictError) {
            
            NSLog(@"ERROR IS PRESENT");
            NSInteger codeValue = [[dictError valueForKey:@"code"] integerValue];
            message = [dictError valueForKey:@"message"];
            errorOccured = [NSNumber numberWithInt:1];
            
        }else if([dictResponse valueForKey:@"result"] && [[dictResponse valueForKey:@"result"] isKindOfClass:[NSDictionary class]]
                 && ![[[dictResponse valueForKey:@"result"]valueForKey:@"errorCode"] isKindOfClass:[NSArray class]]){  /************ THIS IS GONNA CHANGE LATER TO MAKE IT GENERIC **********/
            NSNumber *errorcode = [[dictResponse valueForKey:@"result"] valueForKey:@"errorCode"];
            
            
            if (errorcode != nil) {
                
                message = [[dictResponse valueForKey:@"result"] valueForKey:@"errorMessage"];
                errorOccured = [NSNumber numberWithInt:1];
 
            }else{
               
                errorOccured = [NSNumber numberWithInt:0];
                message = @"";
            }
            
        }else{
            
            errorOccured = [NSNumber numberWithInt:0];
            message = @"Success";
        }
    }else{
        
        NSLog(@"UNKNOWN ERROR - RESPONSE IS NIL");
        message = @"UNKNOWN ERROR - RESPONSE IS NIL";
        errorOccured = [NSNumber numberWithInt:1];
    }
   
    
    NSDictionary *dictStatus = @{@"message": message,@"errorOccured": errorOccured};
    return dictStatus;
}

+(UIAlertView *)displayErrorAlert:(NSString *)message{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:message delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
    
    [alert show];
    return alert;
}

@end
