//
//  TTCurrentWorkingObjects.m
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTCurrentWorkingObjects.h"

@interface TTCurrentWorkingObjects()
    //@property TTOrderDTO* order;
@end

static TTOrderDTO* order;
static UIImage* signatureImage;
static TTContactDTO* currentContact;

static UIViewController* menuViewController;

@implementation TTCurrentWorkingObjects
+(TTOrderDTO*)currentOrder{
    return order;
}

+(void)setCurrentOrder:(TTOrderDTO*)newOrder{
    order = newOrder;
}

+(UIViewController*) homeViewController{
    return menuViewController;
}

+(void)setHomeViewController:(UIViewController*)viewController{
    menuViewController = viewController;
}

+(UIImage*)signatureImage{
    return signatureImage;
}

+(void)setSignatureImage:(UIImage*)sigImage{
    signatureImage = sigImage;
}

+(TTContactDTO*) currentContact{
    return currentContact;
}

+(void)setCurrentContact:(TTContactDTO*)contact{
    currentContact = contact;
}

@end
