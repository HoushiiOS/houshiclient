//
//  TTSettingsViewController.m
//  Houshi
//
//  Created by James Timberlake on 7/24/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTSettingsViewController.h"
#import "TTAppConatants.h"
#import "TTInventoryEntryController.h"
#import "LHDropDownControlView.h"

@interface TTSettingsViewController () <LHDropDownControlViewDelegate>

@end

@implementation TTSettingsViewController{
    LHDropDownControlView* dropDownView;
    NSMutableArray *titles;
    NSMutableArray *options;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   /* dropDownView = [[LHDropDownControlView alloc] initWithFrame:CGRectMake(410, 235, 150, 40)];
    dropDownView.title = @"Font Size";
    dropDownView.delegate = self;
    
    // Add a bunch of options
    options = [NSMutableArray arrayWithCapacity:0];
    titles = [NSMutableArray arrayWithCapacity:0];
    for (int i=10; i<=33; i++) {
            [options addObject:[NSNumber numberWithInt:i]];
            [titles addObject:[NSString stringWithFormat:@"Font Size: %i",i]];
    }
    [dropDownView setSelectionOptions:options withTitles:titles];
    [self.view addSubview:dropDownView];
    
    [dropDownView setTitle:[NSString stringWithFormat:@"Font Size: %i",[TTAppConatants orderItemFontSize]]];*/
    [_scrollingTypeSegment setSelectedSegmentIndex:[TTAppConatants isScrollingTableView] ? 0 : 1];
    if ([TTAppConatants orderItemFontSize] == 14) {
        [_fontSizeSegment setSelectedSegmentIndex:0];
    }else if([TTAppConatants orderItemFontSize] == 18){
        [_fontSizeSegment setSelectedSegmentIndex:1];
    }else if([TTAppConatants orderItemFontSize] == 22){
        [_fontSizeSegment setSelectedSegmentIndex:2];
    }
    [_fontSizeSegment setTransform:CGAffineTransformMakeScale(2, 2)];
    [_scrollingTypeSegment setTransform:CGAffineTransformMakeScale(2, 2)];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onButtonClicked:(id)sender {
//    if ([self.presentingViewController isKindOfClass:[TTInventoryEntryController class]]) {
//        [((TTInventoryEntryController*)self.presentingViewController) setTableToScrollType:[TTAppConatants isScrollingTableView]];
//        [((TTInventoryEntryController*)self.presentingViewController) refreshTableView:nil];
//    }
//
    if (((UIButton*)sender).tag == 1) {
        [TTAppConatants setScrollingTableView:(_scrollingTypeSegment.selectedSegmentIndex == 0)];
        int fontSize = 14;
        if (_fontSizeSegment.selectedSegmentIndex == 1) {
            fontSize = 18;
        }else if(_fontSizeSegment.selectedSegmentIndex == 2){
            fontSize = 22;
        }
        [TTAppConatants setOrderItemFontSize:fontSize];

    }
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onPagingValueChanged:(id)sender {
    
}

- (void)dropDownControlView:(LHDropDownControlView *)view didFinishWithSelection:(id)selection{
    NSLog(@"Ok");
    @try {
        [dropDownView setTitle:[NSString stringWithFormat:@"Font Size: %i",[TTAppConatants orderItemFontSize]]];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}


@end
