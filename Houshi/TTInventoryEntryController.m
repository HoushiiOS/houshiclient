//
//  TTInventoryEntryController.m
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//
#define INVENTORY_SELECTION_TYPES typedef enum{ FRONT, BACK, ORDER }


#import "TTInventoryEntryController.h"
#import "TTInventoryTableViewCell.h"
#import "TTEntryPageTableViewCell.h"
#import "TTInvSingleSectionView.h"
#import "TTInvAllSectionView.h"
#import "TTCurrentWorkingObjects.h"
#import "TTOrderDTO.h"
#import "TTOrderItemDTO.h"
#import "LNNumberpad.h"
#import "DropDownListView.h"
#import "TTVerifyPageViewController.h"
#import "TTAppConatants.h"
#import "LHDropDownControlView.h"
#import "TTHoushiApi.h"
#import "TTOrderUtils.h"
#import "CRToast.h"

NSComparator sortQuantities = ^(id num1, id num2)
{
    float v1 = ((NSString*)num1).floatValue;
    float v2 = ((NSString*)num2).floatValue;
    
    if( v1 < v2) return NSOrderedAscending;
    else if (v1 > v2) return NSOrderedDescending;
    else return NSOrderedSame;
};



@interface TTInventoryEntryController ()<UITableViewDelegate, UITableViewDataSource, kDropDownListViewDelegate, UITextFieldDelegate, LNNumberPadDelegate, SingleHeaderDelegate, AllHeaderDelegate, LHDropDownControlViewDelegate>
{
    /*int _invState;
    int _currentRowIndex;
    int _currentTextFieldIndex;
    int _currentPagingNum;*/
    BOOL changingRows;
    int firstTimeOpen;
    NSMutableArray* cells;
    UISwipeGestureRecognizer* gesture;
    UISwipeGestureRecognizer* gesture1;
    NSTimer* timer;
}
@property (nonatomic, strong) TTOrderDTO* order;
@property (nonatomic, strong) UIView* ddView;
@property (nonatomic, strong) LNNumberpad* ln;
@property  int currentRowIndex;
@property  int currentScrollingRowIndex;
@property  int currentTextFieldIndex;
@property  int currentPagingNum;
@property  int invState;
@end


@implementation TTInventoryEntryController{
    LHDropDownControlView* dropDownView;
    NSMutableArray *titles;
    NSMutableArray *options;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setTableToScrollType:(BOOL)isSetToScroll{
    //[TTAppConatants setScrollingTableView:isSetToScroll];
    //JT TOD REMOVE WHEN THIS IS FIXED
    //return;
    if(!isSetToScroll)
    {
        [_tableView setScrollEnabled:NO];
        gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onTableSwipedDown:)];
        gesture.direction = UISwipeGestureRecognizerDirectionUp;
        gesture1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onTableSwipedUp:)];
        gesture1.direction = UISwipeGestureRecognizerDirectionDown;
        [_tableView addGestureRecognizer:gesture];
        [_tableView addGestureRecognizer:gesture1];
    }else{
        [_tableView setScrollEnabled:YES];
        [_tableView removeGestureRecognizer:gesture];
        [_tableView removeGestureRecognizer:gesture1];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstTimeOpen = 0;
    cells = [NSMutableArray new];
    NSLog(@"Opened");
    _ln = [LNNumberpad defaultLNNumberpad];
    _ln.delegate = self;
    [_numberPadView addSubview:_ln];
    
    /*[self demoOrderItems];*/
    _invState = 0;
    _currentPagingNum = 0;
    
    _currentRowIndex = 0;
    _currentScrollingRowIndex = _currentRowIndex;
    
    //constant
    AUTO_SAVE_COUNT_MAX = 10;
    autoSaveCount = 0;
    
    _order = [TTCurrentWorkingObjects currentOrder];
   // NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"product.warehouseBin" ascending:YES comparator:sortQuantities];
   // _order.orderItem = [_order.orderItem sortedArrayUsingDescriptors:[NSArray arrayWithObject: sorter]];
    _headerLabel.text = _order.customer.name;
    _headerEventName.text = ( _order.eventName && ![_order.eventName isEqualToString:@""]) ? [[NSString alloc] initWithFormat:@"EVENT NAME: %@",_order.eventName] : @"";
    _headerOccupancyRate.text = _order.occupancyRate ? [[NSString alloc] initWithFormat:@"OCCUPANCY RATE: %@",_order.occupancyRate] : @"";
    
    //open the notes sections when this page first loads
    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:nil];
    
    [self setTableToScrollType:YES];

    // Do any additional setup after loading the view.
    dropDownView = [[LHDropDownControlView alloc] initWithFrame:CGRectMake(530, 260, 150, 40)];
    dropDownView.title = @"OCCUPANCY RATE";
    dropDownView.delegate = self;
    
    // Add a bunch of options
    options = [NSMutableArray arrayWithCapacity:0];
    titles = [NSMutableArray arrayWithCapacity:0];
    for (int i=-1; i<=20; i++) {
        if (((i*5)+50) > 100) {
            break;
        }
        
        if (i == -1) {
            [options addObject:[NSNumber numberWithInt:i]];
            [titles addObject:@"N/A"];
            continue;
        }
        [options addObject:[NSNumber numberWithInt:(i*
                                                    5)+50]];
        [titles addObject:[NSString stringWithFormat:@"%d%%", (i*
                                                               5)+50]];
    }
    [dropDownView setSelectionOptions:options withTitles:titles];
    [_eventView addSubview:dropDownView];
    [self onItemWithColumnType:@"product.warehouseBin" inAscendingOrder:YES];

    
}


-(void)onTableSwipedDown:(id)sender
{
    //_currentRowIndex += [TTAppConatants orderItemPagingSize];
    _currentScrollingRowIndex = [self findNearestRowPagingIndex:_currentRowIndex goingUpOrDown:NO];
    if (_currentScrollingRowIndex >= self.order.orderItem.count) {
        _currentScrollingRowIndex = (self.order.orderItem.count -1);
    }
    [self scrollTableTo:_currentScrollingRowIndex];
}

-(void)onTableSwipedUp:(id)sender
{
   // _currentRowIndex -= [TTAppConatants orderItemPagingSize];
    _currentScrollingRowIndex = [self findNearestRowPagingIndex:_currentRowIndex goingUpOrDown:YES];
    if (_currentScrollingRowIndex < 0) {
        _currentScrollingRowIndex = 0;
    }
    [self scrollTableTo:_currentScrollingRowIndex];
}

-(int)findNearestRowPagingIndex:(int)currentIndex goingUpOrDown:(BOOL)isUp
{
    float multiple;
    if (!isUp) {
        multiple = roundf((currentIndex/[TTAppConatants orderItemPagingSize])) + 1;
    }else{
       multiple = roundf((currentIndex/[TTAppConatants orderItemPagingSize])) - 1;
    }
    return (int)(multiple*[TTAppConatants orderItemPagingSize]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onEventValuesChanged:(id)sender {
    [TTCurrentWorkingObjects currentOrder].eventName = _eventName.text;
    _headerEventName.text = [TTCurrentWorkingObjects currentOrder].eventName || [[TTCurrentWorkingObjects currentOrder].eventName isEqualToString:@""] ? [[NSString alloc] initWithFormat:@"EVENT NAME: %@",[TTCurrentWorkingObjects currentOrder].eventName] : @"";
    _headerOccupancyRate.text = [TTCurrentWorkingObjects currentOrder].occupancyRate ? [[NSString alloc] initWithFormat:@"OCCUPANCY RATE: %@",[TTCurrentWorkingObjects currentOrder].occupancyRate] : @"";
    [self.eventView setHidden:YES];
}

- (IBAction)onEventCanceled:(id)sender {
    [self.eventView setHidden:YES];
}

-(void)switchTypes:(id)sender
{
    _invState ++;
    if(_invState > 3)
        _invState = 0;
    [cells removeAllObjects];
    [_tableView reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [TTAppConatants orderItemRowHeight];
}

-(void)updateOrderItem:(int)index inRowItemNum:(int)row  changingAmtTo:(NSString*)amount
{
    TTOrderItemDTO* orderItem = (TTOrderItemDTO*)_order.orderItem[index];
    if (_invState == 0) {
        orderItem.qtyFront = amount;
    }else if(_invState == 1)
    {
        orderItem.qtyBack = amount ;
    }else if(_invState == 2)
    {
        orderItem.qtyOrderSuggested = amount;
    }
    else if(_invState == 3) {
        if (row == 0) {
            orderItem.qtyFront = amount;
        }else if(row == 1)
        {
            orderItem.qtyBack = amount ;
        }else if(row == 2)
        {
            orderItem.qtyOrderSuggested = amount;
        }
    }
}

-(void)onTextFieldValueChanged:(UITextField*)textField
{
    [self updateOrderItem:_currentRowIndex inRowItemNum:textField.tag changingAmtTo:textField.text];
}

-(void)onSpecialKeyPressed:(NSString *)keyName fromTextField:(UITextField*)textField
{
    
    if([keyName isEqualToString:@"done"])
    {
        
        //select correct row
        //set correct
        _currentRowIndex += 1;
        _currentScrollingRowIndex = _currentRowIndex;
        int numIndex = _currentRowIndex;
        if (![TTAppConatants isScrollingTableView] && (_currentRowIndex)% [TTAppConatants orderItemPagingSize] == 0 ) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:(_currentRowIndex) inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
//            CGPoint point = _tableView.contentOffset;
//            point .y -= _tableView.rowHeight;
//            _tableView.contentOffset = point;
            if (timer) {
                [timer invalidate];
            }
            timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                     target:self
                                                   selector:@selector(selectFirstTable:)
                                                   userInfo:nil
                                                    repeats:YES];
                    //[self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:(_currentRowIndex) inSection:0]];
        }else{
            if ( _currentRowIndex <_order.orderItem.count ) {
                if (timer) {
                    [timer invalidate];
                }
                timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                         target:self
                                                       selector:@selector(selectFirstTable:)
                                                       userInfo:nil
                                                        repeats:YES];
                //[self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0]];
            }
        }

        //[self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        if (numIndex == (_order.orderItem.count -1)) {
            NSDictionary *options = @{
                                      kCRToastTextKey : @"YOU ARE AT THE LAST ORDER ITEM",
                                      kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                      kCRToastBackgroundColorKey : [UIColor blackColor],
                                      kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                                      kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
                                      kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                                      kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                                      };
            [CRToastManager showNotificationWithOptions:options
                                        completionBlock:^{
                                            
                                        }];
        }
        
        
        if (numIndex >= _order.orderItem.count) {
            [self onEndOfTable];
            _currentRowIndex = 0;
            _currentScrollingRowIndex = _currentRowIndex;
            return;
        }
    }
}

-(void)onEndOfTable
{
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if ([[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(self.currentRowIndex -1) inSection:0]] isKindOfClass:[TTEntryPageTableViewCell class]]) {
        _invState++;
        if (_invState == 2) {
            _invState = 3;
            _currentTextFieldIndex = 2;
        }
        [_tableView reloadData];
        _currentRowIndex = 0;
        _currentScrollingRowIndex = _currentRowIndex;
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        if (timer) {
            [timer invalidate];
        }
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                 target:self
                                               selector:@selector(selectFirstTable:)
                                               userInfo:nil
                                                repeats:YES];         //[TTCurrentWorkingObjects setCurrentOrder:_order];
        //[self performSegueWithIdentifier:@"VerifySegue" sender:self];
    }else{
        if (_currentTextFieldIndex >= 2) {
            [TTCurrentWorkingObjects setCurrentOrder:_order];
            [self goToVerifyScreen];
        }else{
            _currentTextFieldIndex++;
            _currentRowIndex = 0;
            _currentScrollingRowIndex = _currentRowIndex;
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            if (timer) {
                [timer invalidate];
            }
            
            timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(selectFirstTable:)
                                           userInfo:nil
                                            repeats:YES];        }
    }
}

-(void)selectFirstTable:(id)sender{
    TTInventoryTableViewCell* cell = (TTInventoryTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0]];
    if (cell != nil) {
        [timer invalidate];
        timer = nil;
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0]];
    }else{
        TTInventoryTableViewCell* cell2 = (TTInventoryTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

        if (cell2) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:[TTAppConatants orderItemScrollType]];
        }else{
            [self.tableView reloadData];
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//TODO add view header field

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _order.orderItem.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    if (_invState == 0) {
        TTEntryPageTableViewCell* cell =
        (TTEntryPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"EntryCell"];
        [cell setDataObject:_order.orderItem[indexPath.row]];
        [cell setRowIndex:indexPath.row];
        cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyFront;
        return cell;
    }else if(_invState == 1){
        TTEntryPageTableViewCell* cell =
        (TTEntryPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"EntryCell"];
        [cell setDataObject:_order.orderItem[indexPath.row]];
        [cell setRowIndex:indexPath.row];
        cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyBack;
        return cell;
    }else if(_invState == 2){
        TTEntryPageTableViewCell* cell =
        (TTEntryPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"EntryCell"];
        [cell setDataObject:_order.orderItem[indexPath.row]];
        [cell setRowIndex:indexPath.row];
        cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrderSuggested;
        return cell;
    }else{
        TTInventoryTableViewCell* cell =
        (TTInventoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"InvCell"];
        [cell setDataObject:_order.orderItem[indexPath.row]];
        [cell setRowIndex:indexPath.row];
        return cell;
    }
    
    return cell;
}

-(void)demoOrderItems{
    _order = [TTOrderDTO new];
    NSMutableArray* orderItems = [NSMutableArray new];
    //_order.orderItem =
    
    TTOrderItemDTO* item = [TTOrderItemDTO new];
    item.product = [TTProductDTO new];
    item.product.desc = @"Huggies 2 PCT";
    item.product.sku = @"6190031";
    item.qtyBack = @"20";
    item.qtyFront = @"40";
    item.qtyOrdered = @"0";
    
    [orderItems addObject:item];
    
    TTOrderItemDTO* item2;
    item2 = [TTOrderItemDTO new];
    item2.product = [TTProductDTO new];
    item2.product.desc = @"Johnson & Johnson Baby Lotion";
    item2.product.sku = @"6190032";
    item2.qtyBack = @"30";
    item2.qtyFront = @"10";
    item2.qtyOrdered = @"4";
    
    [orderItems addObject:item2];
    
    TTOrderItemDTO* item3;
    item3 = [TTOrderItemDTO new];
    item3.product = [TTProductDTO new];
    item3.product.desc = @"Frosted Flakes 24 CT";
    item3.product.sku = @"6190033";
    item3.qtyBack = @"10";
    item3.qtyFront = @"3";
    item3.qtyOrdered = @"1";
    
    
    [orderItems addObject:item3];
    
    for (int i = 0; i<50; i++) {
        TTOrderItemDTO* item2;
        item2 = [TTOrderItemDTO new];
        item2.product = [TTProductDTO new];
        item2.product.desc = @"Johnson & Johnson Baby Lotion";
        item2.product.sku = @"6190032";
        item2.qtyBack = @"30";
        item2.qtyFront = @"10";
        item2.qtyOrdered = @"4";
        [orderItems addObject:item2];
    }
    
    _order.orderItem = [orderItems copy];
    
    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (_invState != 3) {
        TTInvSingleSectionView * view = [[TTInvSingleSectionView alloc] init];
        
        if(_invState == 0)
            view.itemType.text = @"PRES";
        
        if(_invState == 1)
            view.itemType.text = @"BACK";
        
        if(_invState == 2)
            view.itemType.text = @"ORDER";
        [view.itemType setFont:[UIFont fontWithName:@"HelveticaNeue" size:12 ]];
        view.delegate = self;
        [view setActiveColor];
        return view;
    }
    
    TTInvAllSectionView* view = [[TTInvAllSectionView alloc] init];
    view.itemType1.text = @"PRES";
    view.itemType2.text = @"BACK";
    view.itemType3.text = @"ORDER";
  
    view.delegate = self;
   [view setActiveColor];
    return view;
}
/**/

-(void)onItemWithColumnType:(NSString*)type inAscendingOrder:(BOOL)isAscending;
{
    NSLog(@"Sort Array");
    NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:type ascending: isAscending];
    
    if ([type isEqualToString:@"Quantity"]) {
       
        if(_invState == 0 || _invState == 3)
            type = @"frontStock";
        
        if(_invState == 1)
            type = @"backStock";
        
        if(_invState == 2)
            type = @"qtyOrderSuggested";
        
        sortOrder = [[NSSortDescriptor alloc] initWithKey:type ascending:isAscending comparator:sortQuantities];
    }
    
    if ([type isEqualToString:@"Quantity2"]) {
            type = @"backStock";
        sortOrder = [[NSSortDescriptor alloc] initWithKey:type ascending:isAscending comparator:sortQuantities];
    }
    
    if ([type isEqualToString:@"Quantity3"]) {
            type = @"qtyOrderSuggested";        
        sortOrder = [[NSSortDescriptor alloc] initWithKey:type ascending:isAscending comparator:sortQuantities];
    }
    
    _order.orderItem = [_order.orderItem sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]];

    [_tableView reloadData];

}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[TTVerifyPageViewController class]]) {
        ((TTVerifyPageViewController*)segue.destinationViewController).verifyType = @"Inventory";
    }
}


-(void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    
    //@"VERIFY INVENTORY (REQUIRES POP UP CONFIRMATION)",
            if( anIndex == 0){
                [self goToVerifyScreen];
            }
    
    //@"SHOW ALL COLUMNS",
            if( anIndex == 1){
                _invState = 3;
                [_tableView reloadData];
            }
    
    //@"SHOW FRONT STOCK COLUMN",
            //show front
            if( anIndex == 2){
                _invState = 0;
                [_tableView reloadData];
            }
    
    //@"SHOW BACK STOCK COLUMN",
            if( anIndex == 3){
                _invState = 1;
                [_tableView reloadData];
            }
    
    
    //@"",
            if( anIndex == 4){

            }
    
    //@"SETTINGS",
            if( anIndex == 5){
                [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsView"] animated:YES completion:nil];
            }

    //@"CHANGE LOCATION INFORMATION",
            if( anIndex == 6){
                [_eventView setHidden:NO];
            }
    
    //@"SWITCH SIDES",
            if( anIndex == 7){
                if (_tableView.frame.origin.x == 0) {
                    [_tableView setFrame:CGRectOffset(_tableView.frame, _sideView.frame.size.width, 0)];
                    [_sideView setFrame:CGRectOffset(_sideView.frame, -(self.view.frame.size.width - _sideView.frame.size.width), 0)];
                }else{
                    [_tableView setFrame:CGRectOffset(_tableView.frame, -_sideView.frame.size.width, 0)];
                    [_sideView setFrame:CGRectOffset(_sideView.frame, (self.view.frame.size.width - _sideView.frame.size.width), 0)];
                }
            }
    
    //@"OPEN NOTES",
            if( anIndex == 8){
                [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:nil];
            }
    
    //@SPACER
            if(anIndex == 9){
        
            }
    
    
    
    //ENTER PO NUMBER
    if (anIndex == 10) {
        _purchaseNumberView.hidden = NO;
    }
    
    //@"RETURN TO MAIN MENU (REQUIRES POP UP CONFIRMATION)",
            if( anIndex == 11){
                [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO TO THE MAIN MENU SCREEN. DO YOU WANT TO CONTINUE?"
                                              withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                    //main menu
                    [TTHoushiApi closeOrder];
                    [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
                }];
            }

    //@"EXIT OPTIONS"
            if(anIndex == 12){
        
            }
    
    [_ddView removeFromSuperview];
    _ddView = nil;
}

-(void)paginateTableTo:(int)position
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:position inSection:0];
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)scrollTableTo:(int)position
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:position inSection:0];
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop  animated:YES];
}

- (IBAction)onOptionSelect:(id)sender {
   
    _ddView = [[UIView alloc] initWithFrame:self.view.frame];
    [_ddView setBackgroundColor:[UIColor clearColor]];
    [_ddView setAlpha:1.0];
    DropDownListView* dView = [[DropDownListView alloc] initWithTitle:@"OPTIONS" options:@[@"VERIFY INVENTORY",
                                                                                           @"SHOW ALL COLUMNS",
                                                                                           @"SHOW FRONT STOCK COLUMN",
                                                                                           @"SHOW BACK STOCK COLUMN",
                                                                                           @"",
                                                                                           @"SETTINGS",
                                                                                           @"CHANGE LOCATION INFORMATION",
                                                                                           @"SWITCH SIDES",
                                                                                           @"OPEN NOTES",
                                                                                           @"",
                                                                                           @"ENTER PO NUMBER",
                                                                                           @"RETURN TO MAIN MENU",
                                                                                           @"EXIT OPTIONS"] xy:CGPointMake(350, 100) size:CGSizeMake(300,575)
                                                           isMultiple:NO];
    [dView SetBackGroundDropDwon_R:42.0 G:43.0 B:44.0 alpha:1.0];
    [self.view addSubview:_ddView];
    dView.delegate = self;///42	43	44
    [_ddView addSubview:dView];/*****/
    /*
     
     @"SHOW FRONT STOCK",@"SHOW BACK STOCK",@"SHOW ORDER", @"SHOW ALL AMOUNTS", @"VERIFY", @"MAIN MENU",@"SWITCH SIDES", @"OPEN NOTES", @"CHANGE EVENT NAME/OCCUPANCY RATE", @"SETTINGS",@"EXIT"
     
     
     */
    
    
}

-(void)refreshTableView:(id)sender
{
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self refreshTableView:nil];
    [self setTableToScrollType:[TTAppConatants isScrollingTableView]];
  //  [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:(_currentRowIndex) inSection:0] animated:YES scrollPosition:[TTAppConatants orderItemScrollType]];
   // [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:(_currentRowIndex) inSection:0]];
    _headerEventName.text = [TTCurrentWorkingObjects currentOrder].eventName || [[TTCurrentWorkingObjects currentOrder].eventName isEqualToString:@""] ? [[NSString alloc] initWithFormat:@"EVENT NAME: %@",[TTCurrentWorkingObjects currentOrder].eventName] : @"";
    _headerOccupancyRate.text = [TTCurrentWorkingObjects currentOrder].occupancyRate ? [[NSString alloc] initWithFormat:@"OCCUPANCY RATE: %@",[TTCurrentWorkingObjects currentOrder].occupancyRate] : @"";
    if (YES) {
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    firstTimeOpen++;
    //if (firstTimeOpen == 2) {

    if (timer) {
        [timer invalidate];
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(selectFirstTable:)
                                           userInfo:nil
                                            repeats:YES];    //}
}

-(BOOL)isReadyToPage
{
    int num = _currentRowIndex % [TTAppConatants orderItemPagingSize];
    return ( (_currentRowIndex) % [TTAppConatants orderItemPagingSize] == 0 && _currentRowIndex != 0);
}



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [_ln setCurrentTextField:textField];
    
    _currentTextFieldIndex = textField.tag;
    UIView* cellView = textField.superview;
    while (![cellView isKindOfClass:[TTBaseTableViewCell class]]) {
        cellView = cellView.superview;
        if (!cellView) {
            break;
        }
    }
   // if([textField.superview.superview.superview isKindOfClass:[TTBaseTableViewCell class]])
    {
        _currentRowIndex = [((TTBaseTableViewCell*)cellView) getRowIndex];
        _currentScrollingRowIndex = _currentRowIndex;
    }/*else{
        _currentRowIndex = [((TTInventoryTableViewCell*)textField.superview.superview.superview) getRowIndex];
    }*/ 
    changingRows = YES;
    if (![TTAppConatants isScrollingTableView] && ![self isReadyToPage] /*_currentPagingNum <= [TTAppConatants orderItemPagingSize]*/) {
        _currentPagingNum++;
             [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0] animated:YES scrollPosition:[TTAppConatants orderItemScrollType]];
        //[self scrollTableTo:_currentRowIndex];
    }else if(![TTAppConatants isScrollingTableView] && [self isReadyToPage] /*_currentPagingNum > [TTAppConatants orderItemPagingSize]*/){
        //_currentPagingNum = 0;
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
       // [self scrollTableTo:_currentRowIndex];
    }else{
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0] animated:YES scrollPosition:[TTAppConatants orderItemScrollType]];
    }
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:_currentRowIndex inSection:0]];
    changingRows = NO;
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (changingRows) {
        if (_invState != 3) {
            TTEntryPageTableViewCell* cell = (TTEntryPageTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell.quantityField setBackgroundColor:[UIColor yellowColor]];
        }else{
            TTInventoryTableViewCell* cell = (TTInventoryTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            if (_currentTextFieldIndex == 0) {
                [cell.frontField setBackgroundColor:[UIColor yellowColor]];
            }else if (_currentTextFieldIndex == 1){
                [cell.backField setBackgroundColor:[UIColor yellowColor]];
            }else{
                [cell.orderField setBackgroundColor:[UIColor yellowColor]];
            }
        }
        
        return;
    }
    if (_invState != 3) {
        TTEntryPageTableViewCell* cell = (TTEntryPageTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [self textFieldShouldBeginEditing:cell.quantityField];
        //[cell.quantityField setBackgroundColor:[UIColor yellowColor]];
    }else{
        TTInventoryTableViewCell* cell = (TTInventoryTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        if (_currentTextFieldIndex == 0) {
            [self textFieldShouldBeginEditing:cell.frontField];
           // [cell.frontField setBackgroundColor:[UIColor yellowColor]];
        }else if (_currentTextFieldIndex == 1){
            [self textFieldShouldBeginEditing:cell.backField];
           // [cell.backField setBackgroundColor:[UIColor yellowColor]];
        }else{
            [self textFieldShouldBeginEditing:cell.orderField];
          //  [cell.orderField setBackgroundColor:[UIColor yellowColor]];
        }
    }
    autoSaveCount++;
    if (autoSaveCount >= AUTO_SAVE_COUNT_MAX) {
        [self autoSaveTempOrder];
        autoSaveCount = 0;
    }
}


- (void)dropDownControlView:(LHDropDownControlView *)view didFinishWithSelection:(id)selection{
    NSLog(@"Ok");
    @try {
        view.title = (NSString*)[titles objectAtIndex:[((NSNumber*)selection) intValue]];
        [TTCurrentWorkingObjects currentOrder].occupancyRate = (NSString*)[titles objectAtIndex:[((NSNumber*)selection) intValue]];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(void)goToVerifyScreen{
    if (![self areAllQuantitiesFilled]) {
        [[[UIAlertView alloc] initWithTitle:@"NOT FILLED OUT" message:@"THIS ORDER IS NOT FILLED OUT ENTIRELY!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO TO THE VERIFY SCREEN. DO YOU WANT TO CONTINUE?"
                                  withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                                      //@"Verify"
                                      [TTCurrentWorkingObjects setCurrentOrder:_order];
                                      [self performSegueWithIdentifier:@"VerifySegue" sender:self];
                                  }];
    
}


- (IBAction)onPOCancel:(id)sender {
    _purchaseNumberView.hidden = YES;
}

- (IBAction)onPONumSubmit:(id)sender {
    _purchaseNumberView.hidden = YES;
    _order.purchaseOrderNumber = _purchaseNumberField.text;
}
@end
