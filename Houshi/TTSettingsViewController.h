//
//  TTSettingsViewController.h
//  Houshi
//
//  Created by James Timberlake on 7/24/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTSettingsViewController : UIViewController
- (IBAction)onButtonClicked:(id)sender;
- (IBAction)onPagingValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scrollingTypeSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *fontSizeSegment;

@end
