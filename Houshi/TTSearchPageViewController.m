//
//  TTSearchPageViewController.m
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTSearchPageViewController.h"
#import "TTCustomerScheduleDTO.h"
#import "TTSearchPageTableViewCell.h"
#import "TTCurrentWorkingObjects.h"
#import "LHDropDownControlView.h"
#import "TTOrderDTO.h"
#import "TTOrderItemDTO.h"
#import "TTHoushiApi.h"
#import "TTInventoryListResponse.h"
#import "TTCreateOrderResponse.h"
#import "TTAppConatants.h"
#import "TTOrderListResponse.h"
#import "TTSearchOrderItemRespone.h"
#import "TTSearchProductPullResponse.h"
#import "TTSearchCustomerResponse.h"
#import "TTDefaultEntryPageViewController.h"
#import "TTOrderUtils.h"

NSComparator sortNumbers = ^(id num1, id num2)
{
    float v1 = ((NSString*)num1).floatValue;
    float v2 = ((NSString*)num2).floatValue;
    
    if( v1 < v2) return NSOrderedAscending;
    else if (v1 > v2) return NSOrderedDescending;
    else return NSOrderedSame;
};

NSComparator sortDates = ^(NSString* date1, NSString* date2)
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *firstDate = [dateFormat dateFromString:date1];
    NSDate *secondDate = [dateFormat dateFromString:date2];
    
    return [firstDate compare:secondDate];
};

@interface TTSearchPageViewController () <UITableViewDataSource, UITableViewDelegate, LHDropDownControlViewDelegate, TTHoughiApiDelegate>
{
    TTCustomerDTO* currentCustomer;
    TTOrderDTO* currentOrder;
    TTCustomerScheduleDTO* currentCustomerSchedule;
    int currentSection;
    NSMutableArray *titles;
    NSMutableArray *options;
    BOOL btnActionClicked;
}
@property (nonatomic,strong) NSMutableArray* customerSchedules;
@property (nonatomic,strong) NSMutableArray* ordersArray;
@property (nonatomic,strong) NSMutableArray* rhInvArray;
@property (nonatomic,strong) NSMutableArray* rhOrdArray;
@property (nonatomic,strong) NSMutableArray* rhPPPArray;
@property (nonatomic,strong) NSMutableArray* rhQOrdArray;
@property (nonatomic,strong) NSMutableArray* rhDelArray;
@property (nonatomic,strong) NSMutableArray* rhCreditArray;
@property (nonatomic,strong) NSMutableArray* customersArray;
@end


@implementation TTSearchPageViewController{
    LHDropDownControlView* dropDownView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY ) {
        _searchHeader.text = @"INVENTORY";
        _inventoryButton.hidden = NO;
        _inventoryButton.tag = 0;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_ORDER ) {
        _searchHeader.text = @"ORDER";
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP ) {
        _searchHeader.text = @"PICK/PACK/PULL";
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER ) {
        _searchHeader.text = @"QUICK ORDER";
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY ) {
        _searchHeader.text = @"DELIVERY";
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY ) {
        _searchHeader.text = @"PREVIEW DELIVERY";
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY ) {
        _searchHeader.text = @"RECENT HISTORY";
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT ) {
        _searchHeader.text = @"CREDIT ORDER";
    }


    //JT- comment this out later
    //TTAppConatants.CURRENT_ORDER_TYPE = ORDER_TYPE_INVENTORY;//340, 135
    dropDownView = [[LHDropDownControlView alloc] initWithFrame:CGRectMake(365, 135, 150, 40)];
    dropDownView.title = @"Occupancy Rate";
    dropDownView.delegate = self;
    
    //int *x = NULL; *x = 42;
    
    // Add a bunch of options
    options = [NSMutableArray arrayWithCapacity:0];
    titles = [NSMutableArray arrayWithCapacity:0];
    for (int i=-1; i<=20; i++) {
        if (((i*5)+50) > 100) {
            break;
        }
        if (i == -1) {
            [options addObject:[NSNumber numberWithInt:i]];
            [titles addObject:@"N/A"];
            continue;
        }
        [options addObject:[NSNumber numberWithInt:(i*
                            5)+50]];
        [titles addObject:[NSString stringWithFormat:@"%d%%", (i*
                                                               5)+50]];
    }
    [dropDownView setSelectionOptions:options withTitles:titles];
    [_invEventView setHidden:NO];
    [_invEventView addSubview:dropDownView];
    
    /*TTCustomerScheduleDTO* cSched = [TTCustomerScheduleDTO new];
    cSched.customer = [TTCustomerDTO new];
    cSched.customer.name = @"IOA Spiderman";
    _customerSchedules = [NSMutableArray new];
    [_customerSchedules addObject:cSched];*/
    
    [TTHoushiApi setDelegate:self];
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY) {
       [TTHoushiApi showInventoryList];
    }else if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT){
        [TTHoushiApi showCustomerList];
    }else if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY){
        NSArray* orders = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_INVENTORY];
        NSMutableArray* iOrders = [NSMutableArray arrayWithArray:orders];
        NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:NO comparator:sortDates];
        
        [iOrders sortUsingDescriptors:@[sorter]];
        _rhInvArray = [NSMutableArray arrayWithArray:iOrders];
        
        orders = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_ORDER];
        iOrders = [NSMutableArray arrayWithArray:orders];
        [iOrders sortUsingDescriptors:@[sorter]];
        _rhOrdArray = [NSMutableArray arrayWithArray:iOrders];
        
        orders = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_QUICK_ORDER];
        iOrders = [NSMutableArray arrayWithArray:orders];
        [iOrders sortUsingDescriptors:@[sorter]];
        _rhQOrdArray = [NSMutableArray arrayWithArray:iOrders];
        
        orders = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_PPP];
        iOrders = [NSMutableArray arrayWithArray:orders];
        [iOrders sortUsingDescriptors:@[sorter]];
        _rhPPPArray = [NSMutableArray arrayWithArray:iOrders];
        
        orders = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_DELIVERY];
        iOrders = [NSMutableArray arrayWithArray:orders];
        [iOrders sortUsingDescriptors:@[sorter]];
        _rhDelArray = [NSMutableArray arrayWithArray:iOrders];
        
        orders = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_CREDIT];
        iOrders = [NSMutableArray arrayWithArray:orders];
        [iOrders sortUsingDescriptors:@[sorter]];
        _rhCreditArray = [NSMutableArray arrayWithArray:iOrders];
        [_tableview reloadData];
    }else{
        [TTHoushiApi showOrderList];
    }
    
    [HelperMethods convertCapitalForView:self.view];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY){
        return 6;
    }
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString* sectionName = @"";
    
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY){
        switch (section) {
            case 0:
                sectionName = @"Recent History Inventory";
                break;
            case 1:
                sectionName = @"Recent History Order";
                break;
            case 2:
                sectionName = @"Recent History Quick Order";
                break;
            case 3:
                sectionName = @"Recent History PPP";
                break;
            case 4:
                sectionName = @"Recent History Delivery";
                break;
            case 5:
            default:
                sectionName = @"Recent History Credit";
                break;
        }
    }
    
    
    return sectionName;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY){
        switch (section) {
            case 0:
                return _rhInvArray.count;
                break;
            case 1:
                return _rhOrdArray.count;
                break;
            case 2:
                return _rhQOrdArray.count;
                break;
            case 3:
                return _rhPPPArray.count;
                break;
            case 4:
                return _rhDelArray.count;
                break;
            case 5:
            default:
                return _rhCreditArray.count;
                break;
        }
    }
    
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY)
        if (_inventoryButton.tag == 0){
            return _customerSchedules.count;
        }else if(_inventoryButton.tag == 1){
            return _customersArray.count;
        }
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT)
        return _customersArray.count;
    return _ordersArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TTSearchPageTableViewCell* cell =
    (TTSearchPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SearchCell"];
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY){
        if (_inventoryButton.tag == 0){
            [cell setDataObject:_customerSchedules[indexPath.row]];
        }else if(_inventoryButton.tag == 1){
            [cell setDataObject:_customersArray[indexPath.row]];
        }
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY){
        NSMutableArray* currentOrderArray;
        switch (indexPath.section) {
            case 0:
                currentOrderArray = _rhInvArray;
                break;
            case 1:
                currentOrderArray = _rhOrdArray;
                break;
            case 2:
                currentOrderArray = _rhQOrdArray;
                break;
            case 3:
                currentOrderArray = _rhPPPArray;
                break;
            case 4:
                currentOrderArray = _rhDelArray;
                break;
            case 5:
            default:
                currentOrderArray  = _rhCreditArray;
                break;
        }
        [cell setDataObject:currentOrderArray[indexPath.row]];
        cell.headerField.text = [cell.headerField.text stringByAppendingFormat:@" %@",((TTOrderDTO*)currentOrderArray[indexPath.row]).createdAt];
        
    }
    else if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT)
        [cell setDataObject:_customersArray[indexPath.row]];
    else
        [cell setDataObject:_ordersArray[indexPath.row]];
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY) {
    
        NSInteger numBoxes =0;
        if (((TTOrderDTO*)_ordersArray[indexPath.row]).numBoxes) {
            numBoxes = [((TTOrderDTO*)_ordersArray[indexPath.row]).numBoxes integerValue];
        }
        cell.headerField.text = [[NSString alloc] initWithFormat:@"%@ - %ld BOXES",cell.headerField.text,(long)numBoxes ];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY) {
        if(_inventoryButton.tag ==0){
            currentCustomer = ((TTCustomerScheduleDTO*)_customerSchedules[indexPath.row]).customer;
            currentCustomerSchedule = (TTCustomerScheduleDTO*)_customerSchedules[indexPath.row];
        }else if(_inventoryButton.tag == 1){
            currentCustomer = ((TTCustomerDTO*)_customersArray[indexPath.row]);
        }
    }else if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT){
        currentCustomer = (TTCustomerDTO*)_customersArray[indexPath.row];
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY){
        switch (indexPath.section) {
            case 0:
                currentOrder = _rhInvArray[indexPath.row];
                [TTDefaultEntryPageViewController setOrderHistoryType:ORDER_SAVE_DIRECTORY_HISTORY_INVENTORY];
                break;
            case 1:
                currentOrder = _rhOrdArray[indexPath.row];
                [TTDefaultEntryPageViewController setOrderHistoryType:ORDER_SAVE_DIRECTORY_HISTORY_ORDER];
                break;
            case 2:
                currentOrder = _rhQOrdArray[indexPath.row];
                [TTDefaultEntryPageViewController setOrderHistoryType:ORDER_SAVE_DIRECTORY_HISTORY_QUICK_ORDER];
                break;
            case 3:
                currentOrder = _rhPPPArray[indexPath.row];
                [TTDefaultEntryPageViewController setOrderHistoryType:ORDER_SAVE_DIRECTORY_HISTORY_PPP];
                break;
            case 4:
                currentOrder = _rhDelArray[indexPath.row];
                [TTDefaultEntryPageViewController setOrderHistoryType:ORDER_SAVE_DIRECTORY_HISTORY_DELIVERY];
                break;
            case 5:
            default:
                currentOrder  = _rhCreditArray[indexPath.row];
                [TTDefaultEntryPageViewController setOrderHistoryType:ORDER_SAVE_DIRECTORY_HISTORY_CREDIT];
                break;
        }
        currentSection = indexPath.section;
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
    }else{
        currentOrder = _ordersArray[indexPath.row];
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onInventoryClicked:(id)sender {
    if (_inventoryButton.tag == 1) {
        _inventoryButton.tag = 0;
        [_inventoryButton setTitle:@"INVENTORY SCHEDULES" forState:UIControlStateNormal];
        [TTHoushiApi showInventoryList];
    }else if(_inventoryButton.tag == 0){
        _inventoryButton.tag = 1;
        [_inventoryButton setTitle:@"CUSTOMER LIST" forState:UIControlStateNormal];
        [TTHoushiApi showCustomerList];
    }
}

- (IBAction)onYesButtonClicked:(id)sender {
    [_invEventView setHidden:NO];
    [_invQuestionView setHidden:YES];
}

- (IBAction)onNoButtonClicked:(id)sender {
    [self onCancelEventClicked:nil];
    [_secondView setHidden:YES];
    [self performSegueWithIdentifier:@"InvEntry" sender:self];
}

- (IBAction)onEventSubmitClicked:(id)sender {
   
    [TTCurrentWorkingObjects currentOrder].eventName = _invEventNameField.text;
    [self performSegueWithIdentifier:@"InvEntry" sender:self];
}

- (IBAction)onConfirmClicked:(id)sender {
    
    if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT)
    {
        if (!currentCustomer) {
            [[[UIAlertView alloc] initWithTitle:@"CUSTOMER NOT AVAILABLE" message:@"PLEASE SELECT A CUSTOMER" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            return;
        }
        NSString* additionStr = @"AN INVENTORY";
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER) {
            additionStr = @"A QUICK ORDER";
        }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT){
            additionStr = @"A CREDIT ORDER";
        }
        
        NSString* title = [[NSString alloc] initWithFormat:@"YOU HAVE SELECTED TO DO %@ ON \n %@.\n DO YOU WANT TO CONTINUE?", additionStr,currentCustomer.name ];
        [[TTHoushiApi getInstance] showConfirmAlert:title
                                      withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
            btnActionClicked = YES;
            [TTHoushiApi createOrderWithCustomerId:currentCustomer.id.intValue isQuickOrder:(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT) withCustomerScheduleId:currentCustomerSchedule.id.intValue];
        }];
    }else{
        if (!currentOrder) {
            [[[UIAlertView alloc] initWithTitle:@"ORDER NOT AVAILABLE" message:@"PLEASE SELECT AN ORDER" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            return;
        }
        NSString* title = [[NSString alloc] initWithFormat:@"YOU HAVE SELECTED \n %@. \n DO YOU WANT TO CONTINUE?",currentOrder.customerName ? currentOrder.customerName : currentOrder.customer.name ];
        [[TTHoushiApi getInstance] showConfirmAlert:title
                                      withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
            if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY) {
                switch (currentSection) {
                    case 0:
                        break;
                    case 1:
                    case 2:
                        currentOrder = [self returnOrderItemsForHistory:currentOrder];
                        break;
                    case 3:
                        currentOrder = [self returnPPPItemsForHistory:currentOrder];
                        break;
                    case 4:
                    case 5:
                    default:
                        currentOrder = [self returnDelOrCreditItemsForHistory:currentOrder];
                        break;
                }
                [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
            }else{
                [TTHoushiApi getOrderItems];
            }
        }];
    }
    
    
       //[self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"OrderEntry"] animated:YES];
    
    //demo stuff
    /*
    TTOrderDTO* order = [self demoOrderItems:currentCustomer];
    [TTCurrentWorkingObjects setCurrentOrder:order];
    [_secondView setHidden:NO];
    [self onYesButtonClicked:nil];
     */
}

- (IBAction)onCancelEventClicked:(id)sender {
    //[_invEventView setHidden:YES];
    //[_invQuestionView setHidden:NO];
    [_secondView setHidden:YES];
    [self performSegueWithIdentifier:@"InvEntry" sender:self];
}

- (IBAction)onMainMenuButtonClicked:(id)sender {
    [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
}

- (void)dropDownControlView:(LHDropDownControlView *)view didFinishWithSelection:(id)selection{
    NSLog(@"Ok");
    @try {
        view.title = (NSString*)[titles objectAtIndex:[((NSNumber*)selection) intValue]];
        [TTCurrentWorkingObjects currentOrder].occupancyRate = (NSString*)[titles objectAtIndex:[((NSNumber*)selection) intValue]];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(TTOrderDTO*)demoOrderItems:(TTCustomerDTO*)customer{
    TTOrderDTO* order = [TTOrderDTO new];
    NSMutableArray* orderItems = [NSMutableArray new];
    //_order.orderItem =
    
    TTOrderItemDTO* item = [TTOrderItemDTO new];
    item.product = [TTProductDTO new];
    item.product.desc = @"Huggies 2 PCT";
    item.product.sku = @"6190031";
    item.qtyBack = @"20";
    item.qtyFront = @"40";
    item.qtyOrdered = @"0";
    
    [orderItems addObject:item];
    
    TTOrderItemDTO* item2;
    item2 = [TTOrderItemDTO new];
    item2.product = [TTProductDTO new];
    item2.product.desc = @"Johnson & Johnson Baby Lotion";
    item2.product.sku = @"6190032";
    item2.qtyBack = @"30";
    item2.qtyFront = @"10";
    item2.qtyOrdered = @"4";
    
    [orderItems addObject:item2];
    
    TTOrderItemDTO* item3;
    item3 = [TTOrderItemDTO new];
    item3.product = [TTProductDTO new];
    item3.product.desc = @"Frosted Flakes 24 CT";
    item3.product.sku = @"6190033";
    item3.qtyBack = @"10";
    item3.qtyFront = @"3";
    item3.qtyOrdered = @"1";
    
    
    [orderItems addObject:item3];
    
    for (int i = 0; i<50; i++) {
        TTOrderItemDTO* item2;
        item2 = [TTOrderItemDTO new];
        item2.product = [TTProductDTO new];
        item2.product.desc = @"Johnson & Johnson Baby Lotion";
        item2.product.sku = @"6190032";
        item2.qtyBack = @"30";
        item2.qtyFront = @"10";
        item2.qtyOrdered = @"4";
        [orderItems addObject:item2];
    }
    
    order.orderItem = [orderItems copy];
    order.customerId = customer.id;
    order.customer = customer;
    return order;
    
}

-(void)onOperationSuccess:(TTBaseResponse *)response
{
    if ([response isKindOfClass:[TTInventoryListResponse class]]) {
        _customerSchedules = [NSMutableArray arrayWithArray:((TTInventoryListResponse*)response).customerSchedule];
        NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"customer.inventoryDisplayOrder" ascending:YES comparator:sortNumbers];
        [_customerSchedules sortUsingDescriptors:@[sorter]];
        [_tableview reloadData];
    }
    
    if ([response isKindOfClass:[TTOrderListResponse class]]) {
        _ordersArray = [NSMutableArray arrayWithArray:((TTOrderListResponse*)response).order];
        NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"customer.orderDisplayOrder" ascending:YES comparator:sortNumbers];
        if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_INVENTORY)
            sorter = [[NSSortDescriptor alloc] initWithKey:@"customer.inventoryDisplayOrder" ascending:YES comparator:sortNumbers];
        else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_ORDER)
            sorter = [[NSSortDescriptor alloc] initWithKey:@"customer.orderDisplayOrder" ascending:YES comparator:sortNumbers];
        else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP)
            sorter = [[NSSortDescriptor alloc] initWithKey:@"customer.pullDisplayOrder" ascending:YES comparator:sortNumbers];
        else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY || [TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT)
            sorter = [[NSSortDescriptor alloc] initWithKey:@"customer.deliveryDisplayOrder" ascending:YES comparator:sortNumbers];
        [_ordersArray sortUsingDescriptors:@[sorter]];
        [_tableview reloadData];
    }
    
    if ([response isKindOfClass:[TTSearchCustomerResponse class]]) {
        _customersArray = [NSMutableArray arrayWithArray:((TTSearchCustomerResponse*)response).customer];
        NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"orderDisplayOrder" ascending:YES comparator:sortNumbers];
        if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_INVENTORY)
            sorter = [[NSSortDescriptor alloc] initWithKey:@"inventoryDisplayOrder" ascending:YES comparator:sortNumbers];
        [_ordersArray sortUsingDescriptors:@[sorter]];
        [_customersArray sortUsingDescriptors:@[sorter]];
        [_tableview reloadData];
    }
    
    if ([response isKindOfClass:[TTCreateOrderResponse class]]) {
        ((TTCreateOrderResponse*)response).order.customer = currentCustomer;
        [TTCurrentWorkingObjects setCurrentOrder:((TTCreateOrderResponse*)response).order];
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT) {
            for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem) {
                item.productDelivery = [TTProductDeliveryItem new];
                item.productDelivery.orderId = [TTCurrentWorkingObjects currentOrder].id;
                item.productDelivery.orderItemId = item.id;
            }
        }
        
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT) {
            NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"product.nutshellBin" ascending:YES comparator:sortNumbers];
            NSMutableArray* orderItems = [NSMutableArray arrayWithArray:[TTCurrentWorkingObjects currentOrder].orderItem];
            [orderItems sortUsingDescriptors:@[sorter]];
            [TTCurrentWorkingObjects currentOrder].orderItem = [NSArray arrayWithArray:orderItems];
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];

        }
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY) {
            [_secondView setHidden:NO];
            [self onYesButtonClicked:nil];
        }
    }
    
    if([response isKindOfClass:[TTSearchOrderItemRespone class]])
    {
        
        if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_ORDER)) {
            [TTCurrentWorkingObjects currentOrder].orderItem = ((TTSearchOrderItemRespone*)response).OrderItem;
            NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"product.warehouseBin" ascending:YES comparator:sortNumbers];
            NSMutableArray* orderItems = [NSMutableArray arrayWithArray:[TTCurrentWorkingObjects currentOrder].orderItem];
            [orderItems sortUsingDescriptors:@[sorter]];
            [TTCurrentWorkingObjects currentOrder].orderItem = [NSArray arrayWithArray:orderItems];
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"OrderEntry"] animated:YES];
    
        }else if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP){
            
            NSSortDescriptor* sorter = [[NSSortDescriptor alloc] initWithKey:@"product.warehouseBin" ascending:YES comparator:sortNumbers];
            NSMutableArray* orderItems = [NSMutableArray arrayWithArray:((TTSearchOrderItemRespone*)response).OrderItem];
            [orderItems sortUsingDescriptors:@[sorter]];
            [TTCurrentWorkingObjects currentOrder].orderItem = [orderItems sortedArrayUsingDescriptors:@[sorter]];
            [TTHoushiApi getPPPItems];
    
        }else if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY){
            NSMutableArray* nonZeroOrderItems = [NSMutableArray new];
            for (TTOrderItemDTO* orderItem in ((TTSearchOrderItemRespone*)response).OrderItem)
            {
                orderItem.productPull = orderItem.OrderReport.productPull;
                orderItem.OrderReport.productPull = nil;
                
                if (orderItem.OrderReport.productDelivery.id == nil) {
                    orderItem.productDelivery = [TTProductDeliveryItem new];
                    orderItem.productDelivery.orderItemId = orderItem.id;
                    orderItem.productDelivery.orderId = orderItem.orderId;
                }else{
                    orderItem.productDelivery = orderItem.OrderReport.productDelivery;
                }
                orderItem.OrderReport.productDelivery = nil;
                if (orderItem.productPull.qty.intValue != 0) {
                    [nonZeroOrderItems addObject:orderItem];
                }
            }
            [TTCurrentWorkingObjects currentOrder].orderItem = [NSArray arrayWithArray:nonZeroOrderItems];
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
        }else{
            
            [TTCurrentWorkingObjects currentOrder].orderItem = ((TTSearchOrderItemRespone*)response).OrderItem;
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
        }
    }
    
    //pull success
    if ([response isKindOfClass:[TTSearchProductPullResponse class]]) {
        if(((TTSearchProductPullResponse*)response).ProductPull.count > 0){
            for (TTOrderItemDTO* orderItem in [TTCurrentWorkingObjects currentOrder].orderItem)
            {
                for (TTProductPullDTO* pullProduct in ((TTSearchProductPullResponse*)response).ProductPull) {
                    if (pullProduct.orderItemId.intValue == orderItem.id.intValue) {
                        orderItem.productPull = pullProduct;
                        break;
                    }
                }
                if (orderItem.productPull == nil) {
                    orderItem.productPull = [TTProductPullDTO new];
                    orderItem.productPull.orderItemId = orderItem.id;
                    orderItem.productPull.orderId = orderItem.orderId;
                    orderItem.productPull.productId = orderItem.productId;
                }

            }
        }else{
            for (TTOrderItemDTO* orderItem in [TTCurrentWorkingObjects currentOrder].orderItem)
            {

                if (orderItem.productPull == nil) {
                    orderItem.productPull = [TTProductPullDTO new];
                    orderItem.productPull.orderItemId = orderItem.id;
                    orderItem.productPull.orderId = orderItem.orderId;
                    orderItem.productPull.productId = orderItem.productId;
                }
            }
        }
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
    }
    
}

-(void)onOperationError:(TTBaseResponse *)error
{
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1 && !btnActionClicked) {
        
        [TTHoushiApi showInventoryList];
    }
    else if(btnActionClicked){
        
        btnActionClicked = NO;
        [TTHoushiApi createOrderWithCustomerId:currentCustomer.id.intValue isQuickOrder:false withCustomerScheduleId:currentCustomerSchedule.id.intValue ];
    }
}


-(TTOrderDTO*)returnOrderItemsForHistory:(TTOrderDTO*)currentOrderSelected
{
    NSMutableArray* orderItemArray = [NSMutableArray new];
    for (TTOrderItemDTO* orderItem in currentOrderSelected.orderItem)
    {
        if (orderItem.qtyOrdered != nil && ![orderItem.qtyOrdered isEqualToString:@""]) {
            [orderItemArray addObject:orderItem];
        }
        
    }
    currentOrderSelected.orderItem = [NSArray arrayWithArray:orderItemArray];
    return currentOrderSelected;
}

-(TTOrderDTO*)returnPPPItemsForHistory:(TTOrderDTO*)currentOrderSelected
{
    NSMutableArray* orderItemArray = [NSMutableArray new];
    for (TTOrderItemDTO* orderItem in currentOrderSelected.orderItem)
    {
        if (orderItem.productPull != nil && ![orderItem.productPull.qty isEqualToString:@""]) {
            [orderItemArray addObject:orderItem];
        }
        
    }
    currentOrderSelected.orderItem = [NSArray arrayWithArray:orderItemArray];
    return currentOrderSelected;
}

-(TTOrderDTO*)returnDelOrCreditItemsForHistory:(TTOrderDTO*)currentOrderSelected
{
    NSMutableArray* orderItemArray = [NSMutableArray new];
    for (TTOrderItemDTO* orderItem in currentOrderSelected.orderItem)
    {
        if (orderItem.productDelivery != nil && ![orderItem.productDelivery.qtyDelivered isEqualToString:@""]) {
            [orderItemArray addObject:orderItem];
        }
        
    }
    currentOrderSelected.orderItem = [NSArray arrayWithArray:orderItemArray];
    return currentOrderSelected;
}
@end
