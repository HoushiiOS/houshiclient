//
//  TTSearchPageViewController.h
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTSearchPageViewController : UIViewController
@property (nonatomic, strong) IBOutlet UITableView* tableview;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (nonatomic, weak) IBOutlet UILabel* searchHeader;
@property (weak, nonatomic) IBOutlet UIView *invQuestionView;
@property (weak, nonatomic) IBOutlet UIView *invEventView;
@property (weak, nonatomic) IBOutlet UITextField *invEventNameField;
@property (weak, nonatomic) IBOutlet UISlider *invEventRateField;
@property (weak, nonatomic) IBOutlet UIView *viewMainMenu;
@property (weak, nonatomic) IBOutlet UIButton *inventoryButton;

- (IBAction)onInventoryClicked:(id)sender;
- (IBAction)onYesButtonClicked:(id)sender;
- (IBAction)onNoButtonClicked:(id)sender;
- (IBAction)onEventSubmitClicked:(id)sender;
- (IBAction)onConfirmClicked:(id)sender;
- (IBAction)onCancelEventClicked:(id)sender;
- (IBAction)onMainMenuButtonClicked:(id)sender;
-(void)onItemListComplete;
-(void)onOrderItemListLoad;
@end
