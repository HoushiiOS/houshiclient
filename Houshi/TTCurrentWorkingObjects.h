//
//  TTCurrentWorkingObjects.h
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTOrderDTO.h"
#import "TTContactDTO.h"

@interface TTCurrentWorkingObjects : NSObject
+(TTOrderDTO*)currentOrder;

+(void)setCurrentOrder:(TTOrderDTO*)newOrder;

+(UIViewController*) homeViewController;

+(void)setHomeViewController:(UIViewController*)viewController;

+(UIImage*)signatureImage;

+(void)setSignatureImage:(UIImage*)sigImage;

+(TTContactDTO*) currentContact;

+(void)setCurrentContact:(TTContactDTO*)contact;

@end
