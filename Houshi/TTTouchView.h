//
//  TTTouchView.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TouchViewDelegate <NSObject>

@optional

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@interface TTTouchView : UIView
{
    id delegate;
}

 @property (nonatomic, weak) id<TouchViewDelegate> delegate;
@end
