//
//  TTInventoryEntryController.h
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTEntryPageViewController.h"

@interface TTInventoryEntryController : TTEntryPageViewController
@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UIView *numberPadView;
@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak, nonatomic) IBOutlet UILabel *headerOccupancyRate;

@property (weak, nonatomic) IBOutlet UILabel *headerEventName;
@property (weak, nonatomic) IBOutlet UITextField *eventName;
- (IBAction)onOptionSelect:(id)sender;
-(void)setTableToScrollType:(BOOL)isSetToScroll;
@property (nonatomic,weak) IBOutlet UILabel* headerLabel;
@property (weak, nonatomic) IBOutlet UIView *eventView;
- (IBAction)onEventValuesChanged:(id)sender;
- (IBAction)onEventCanceled:(id)sender;

-(IBAction)switchTypes:(id)sender;
-(void)refreshTableView:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *purchaseNumberView;
@property (weak, nonatomic) IBOutlet UITextField *purchaseNumberField;
- (IBAction)onPOCancel:(id)sender;
- (IBAction)onPONumSubmit:(id)sender;

@end
