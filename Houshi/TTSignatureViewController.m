//
//  TTSignatureViewController.m
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTSignatureViewController.h"
#import "TTCurrentWorkingObjects.h"
#import "HelperMethods.h"

@interface TTSignatureViewController ()<TouchViewDelegate>

@end

@implementation TTSignatureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGRect screenBounds = _m_view.bounds;
	//set up drawing layer
    
	canvasLayer = [CALayer layer];
    
	canvasLayer.bounds = screenBounds;
    
	canvasLayer.position = CGPointMake(screenBounds.size.width/2, screenBounds.size.height/2);
    
	[canvasLayer setDelegate:self];
    
    
    
	//set up storage layer
    
	backgroundLayer = [CALayer layer];
    
	backgroundLayer.bounds = screenBounds;
    
	backgroundLayer.position = CGPointMake(screenBounds.size.width/2, screenBounds.size.height/2);
    
	[backgroundLayer setDelegate:self];
    
    
    
	//set up view and add layers
    
	//m_view = [[TouchView alloc] initWithFrame:screenBounds];
    
	_m_view.backgroundColor = [UIColor whiteColor];
    
	[_m_view.layer addSublayer:canvasLayer];
    
	[_m_view.layer addSublayer:backgroundLayer];
    
    
	[_m_view setDelegate:self];
    
    
	//initialize some other variables
    
	cacheImage = nil;
    
	touching = NO;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
	if (touching) return;
    
    
    
	//start a new path
    
	path = CGPathCreateMutable();
    
    
    
	//set the path's starting point
    
	UITouch *touch = (UITouch *)[touches anyObject];
    
	CGPathMoveToPoint(path, NULL, [touch locationInView:_m_view].x, [touch locationInView:_m_view].y);
    
    
    
	touching = YES;
    
    
    
}



-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
	if (touching)
        
	{
        
		//get the current location of the touch event
        
		UITouch *theTouch = (UITouch *)[touches anyObject];
        
		pathPoint = [theTouch locationInView:_m_view];
        
        
        
		CGPathAddLineToPoint(path, NULL, pathPoint.x, pathPoint.y);
        
        
        
		[canvasLayer setNeedsDisplay];
        
	}
    
}



-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
	if (!touching) return;
    
    
    
	//create a new image context
    
	UIGraphicsBeginImageContext(CGSizeMake(backgroundLayer.bounds.size.width, backgroundLayer.bounds.size.height));
    
    
    
    //grab a reference to the new image context
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    
    
    //push the image context to the top of the drawing stack
    
    UIGraphicsPushContext(ctx);
    
    
    
    //set the blend mode to prevent white pixels from
    
    //covering up the lines that have already been drawn
    
    CGContextSetBlendMode(ctx, kCGBlendModeDarken);
    
    
    
    if (cacheImage != nil) {
        
        //draw the cached state of the image to the image context and release it
        
        [cacheImage drawInRect:CGRectMake(0,0,backgroundLayer.bounds.size.width,backgroundLayer.bounds.size.height)];
        
        cacheImage = nil;
    }
    
    
    
    //blend the drawing layer into the image context
    
    [canvasLayer drawInContext:ctx];
    
    
    
    //we're done drawing to the image context
    
    UIGraphicsPopContext();
    
    
    
    //store the image context so we can add to it again later
    
    cacheImage = UIGraphicsGetImageFromCurrentImageContext();
    
    
    
    
	//we're finished with the image context altogether
    
	UIGraphicsEndImageContext();
    
    
    
	touching = NO;
    
    
    
	//release the path
    
	CGPathRelease(path);
    
    
    
	//update the background layer (we'll need to draw the cached image to the background)
    
	[backgroundLayer setNeedsDisplay];
    
}



- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    
	//this method is handling multiple layers, so first
    
	//determine which layer we're drawing to
    
	if (layer == canvasLayer) {
        
		//we don't want this to fire after the background layer update
        
		//and after the path has been released
        
		if (!touching) return;
        
        
        
		//add the path to the context
        
		CGContextAddPath(ctx, path);
        
        
        
		//set a line width and draw the path
        
		CGContextSetLineWidth(ctx, 2.0f);
        
		CGContextStrokePath(ctx);
        
	}
    
	else if (layer == backgroundLayer) {
        
		//remember the current state of the context
        
		CGContextSaveGState(ctx);
        
        
        
		//the cached image coordinate system is upside down, so do a backflip
        
		CGContextTranslateCTM(ctx, 0, backgroundLayer.bounds.size.height);
        
		CGContextScaleCTM(ctx, 1.0, -1.0);
        
        
        
		//draw the image
        
		CGImageRef ref = cacheImage.CGImage;
        
		CGContextDrawImage(ctx, backgroundLayer.bounds, ref);
        
        
        
		//restore the context to its pre-flipped state
        
		CGContextRestoreGState(ctx);
        
	}
    
}

- (void)dealloc {
    
	canvasLayer = nil;
    
	backgroundLayer = nil;
        
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onCancel:(id)sender {
    [canvasLayer removeFromSuperlayer];
    [backgroundLayer removeFromSuperlayer];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSubmit:(id)sender {
    //save cachedImage
    [TTCurrentWorkingObjects setSignatureImage:cacheImage];
    [canvasLayer removeFromSuperlayer];
    [backgroundLayer removeFromSuperlayer];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
