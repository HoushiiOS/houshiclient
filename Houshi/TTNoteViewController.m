//
//  TTNoteViewController.m
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTNoteViewController.h"
#import "SPHChatData.h"
#import "SPHChatData.h"
#import "SPHBubbleCell.h"
#import "SPHBubbleCellImage.h"
#import "SPHBubbleCellImageOther.h"
#import "SPHBubbleCellOther.h"
#import <QuartzCore/QuartzCore.h>
#import "AsyncImageView.h"
#import "QBPopupMenu.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "MHFacebookImageViewer.h"
#import "TTCurrentWorkingObjects.h"
#import "TTHoushiApi.h"
#import "HelperMethods.h"
#import "UIImage+TTUIImage_Scale.h"

#define messageWidth 700

@interface TTNoteViewController () <UIImagePickerControllerDelegate, UIActionSheetDelegate>{
    NSMutableArray* newOrderNotes;
    BOOL isLeaving;
}

@end

@implementation TTNoteViewController

@synthesize pullToRefreshManager = pullToRefreshManager_;
@synthesize reloads = reloads_;

@synthesize imgPicker;
@synthesize Uploadedimage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    Uploadedimage.hidden = YES;
    isLeaving = NO;
    newOrderNotes = [NSMutableArray new];

    NSLog(@"%@",[TTCurrentWorkingObjects currentOrder]);
    
    sphBubbledata=[[NSMutableArray alloc]init];

    [self setUpTextFieldforIphone];
    
    [self.sphChatTable setFrame:CGRectMake(self.sphChatTable.frame.origin.x, self.sphChatTable.frame.origin.y, self.view.frame.size.width, self.sphChatTable.frame.size.height)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    pullToRefreshManager_ = [[MNMPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0f
                                                                                   tableView:self.sphChatTable
                                                                                  withClient:self];
    //[self setUpDummyMessages];
    [self readTTNoteData];
}

-(void)viewWillDisappear:(BOOL)animated{
    isLeaving = YES;
    [textView resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    isLeaving = NO;
    [textView becomeFirstResponder];
}


-(void)readTTNoteData{
    
    if ([TTCurrentWorkingObjects currentOrder].note.count>0) {
        
        NSArray *arrNote = [TTCurrentWorkingObjects currentOrder].note;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        //[formatter setDateFormat:@"yyyy'-'MM'-'dd'"];
        int i = 0;
        for (TTOrderNoteDTO* note in arrNote) {
            NSDate *date;
            if (note.createdAt) {
                date = [formatter dateFromString:note.createdAt];
            }else{
                date = [NSDate date];
            }
            
            NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
            [formatter2 setDateFormat:@"hh:mm a"];
            NSString *rowNumber=[NSString stringWithFormat:@"%d",sphBubbledata.count];
            UIImage* image = [HelperMethods imageFromString:note.image];
            [self adddBubbledata:@"textbyother" mtext:note.note mtime:[formatter2 stringFromDate:date] mimage:[HelperMethods imageFromString:note.image] msgstatus:@"Sending" andMessageFrom:note.createdBy];
            [self performSelector:@selector(messageSent:) withObject:rowNumber afterDelay:1.0*(0.5*i)];
            i++;
        }
        
    }
}

-(void)addNoteWithMessage:(NSString*)chatMessage andImage:(UIImage*)image
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate* date = [NSDate date];
    TTOrderNoteDTO* noteObj = [TTOrderNoteDTO new];
    noteObj.orderId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue];
    noteObj.note = chatMessage;
    noteObj.createdBy = [TTHoushiApi getInstance].currentUser.username;
    noteObj.createdAt = [formatter stringFromDate:date];
    if (image) {
        noteObj.image = [HelperMethods convertImageToStringData:image];
    }
    
    [newOrderNotes addObject:noteObj];
    
}

- (void)handleURL:(NSURL*)url
{
    NSLog(@"Hit handle url method");
}

-(void)setUpDummyMessages
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSString *rowNumber=[NSString stringWithFormat:@"%d",sphBubbledata.count];
    [self adddBubbledata:@"textByme" mtext:@"Hi!!!!!!!" mtime:[formatter stringFromDate:date] mimage:Uploadedimage.image msgstatus:@"Sending" andMessageFrom:[TTHoushiApi getInstance].currentUser.username];
    [self performSelector:@selector(messageSent:) withObject:rowNumber afterDelay:1.0];
    [self adddBubbledata:@"textbyother" mtext:@"Heloo!!!!!" mtime:[formatter stringFromDate:date] mimage:Uploadedimage.image msgstatus:@"Sent" andMessageFrom:[TTHoushiApi getInstance].currentUser.username];
    rowNumber=[NSString stringWithFormat:@"%d",sphBubbledata.count];
    [self adddBubbledata:@"textByme" mtext:@"How are you doing today?" mtime:[formatter stringFromDate:date] mimage:Uploadedimage.image msgstatus:@"Sending" andMessageFrom:[TTHoushiApi getInstance].currentUser.username];
    [self performSelector:@selector(messageSent:) withObject:rowNumber afterDelay:1.5];
    [self adddBubbledata:@"textbyother" mtext:@"I'm doing great! what abt you?" mtime:[formatter stringFromDate:date] mimage:Uploadedimage.image msgstatus:@"Sent" andMessageFrom:[TTHoushiApi getInstance].currentUser.username];
}

#pragma mark MNMBottomPullToRefreshManagerClient

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [pullToRefreshManager_ tableViewScrolled];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y >=360.0f)
    {
    }
    else
        [pullToRefreshManager_ tableViewReleased];
}

- (void)pullToRefreshTriggered:(MNMPullToRefreshManager *)manager
{
    reloads_++;
    [self performSelector:@selector(getEarlierMessages) withObject:nil afterDelay:0.0f];
}

-(void)getEarlierMessages
{
    NSLog(@"get Earlir Messages And Appand to Array");
    [self performSelector:@selector(loadfinished) withObject:nil afterDelay:1];
}

-(void)loadfinished
{
    [pullToRefreshManager_ tableViewReloadFinishedAnimated:YES];
    [self.sphChatTable reloadData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)growingTextViewShouldEndEditing:(HPGrowingTextView *)growingTextView
{
    return isLeaving;
}


-(void)setUpTextFieldforIphone
{
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height-40, self.view.frame.size.width, 40)];
    textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(40, 3, self.view.frame.size.width-(112+72), 40)];
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 6;
	textView.returnKeyType = UIReturnKeyDefault; //just as an example
	textView.font = [UIFont systemFontOfSize:15.0f];
	textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
    
    // textView.text = @"test\n\ntest";
	// textView.animateHeightChange = NO; //turns off animation
    
    [self.view addSubview:containerView];
	
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(40, 0,self.view.frame.size.width-(108+72), 40);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:textView];
    [containerView addSubview:entryImageView];
    
    UIImage *sendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
    UIImage *camBtnBackground = [[UIImage imageNamed:@"cam.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    
    
    UIImage *selectedSendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
	UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(containerView.frame.size.width - (69*2), 8, 126, 27);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[doneBtn setTitle:@"Save" forState:UIControlStateNormal];
    
    [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    [doneBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
	[containerView addSubview:doneBtn];
    
    
    
    UIButton *doneBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn2.frame = CGRectMake(containerView.frame.origin.x+1,2, 35,40);
    doneBtn2.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
	[doneBtn2 setTitle:@"" forState:UIControlStateNormal];
    
    [doneBtn2 setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn2.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn2.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn2 addTarget:self action:@selector(uploadImage:) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn2 setBackgroundImage:camBtnBackground forState:UIControlStateNormal];
    
    //[doneBtn2 setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
    
	[containerView addSubview:doneBtn2];
    
	/*UIButton *doneBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn3.frame = CGRectMake(containerView.frame.size.width - 69, 8, 63, 27);
    doneBtn3.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[doneBtn3 setTitle:@"Done" forState:UIControlStateNormal];
    
    [doneBtn3 setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn3.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn3.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn3 addTarget:self action:@selector(closeNoteView) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn3 setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    [doneBtn3 setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
	[containerView addSubview:doneBtn3];*/
    
    
    
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    
}

-(void)closeNoteView{
    
    //JT - this should be done every time the person presses the done button
    //this method assumes there is only one note that can be added at a time
    /*TTOrderNoteDTO *objNoteDto = [[TTOrderNoteDTO alloc]init];
    objNoteDto.id = 0;
    objNoteDto.orderId = 0;
    objNoteDto.createdAt = @"";
    objNoteDto.updatedAt = @"";
    objNoteDto.note = @"";
    objNoteDto.image = @"";
    
    NSLog(@"%@",[[TTCurrentWorkingObjects currentOrder] valueForKey:@"id"]);
    objNoteDto.id = [[TTCurrentWorkingObjects currentOrder] valueForKey:@"id"];

    //Convert Image data
    
    NSString *strImage = [HelperMethods convertImageToStringData:Uploadedimage.image];
    
    if (![strImage isEqualToString:@""]) {
       
        objNoteDto.image = strImage;
    }
    
    
    
    //  SAVE NOTE TO THIS METHOD BEFORE CLOSING.
  
    [[TTCurrentWorkingObjects currentOrder].note addObject:objNoteDto];*/
    [TTHoushiApi saveNotes:newOrderNotes]; //Call API
    //[newOrderNotes removeAllObjects];
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)resignTextView{
    if ([textView.text length]<1) {
        
    }
    else
    {
        NSString *chat_Message=textView.text;
        textView.text=@"";
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"hh:mm a"];
        
        NSString *rowNumber=[NSString stringWithFormat:@"%d",sphBubbledata.count];
        
        [self addNoteWithMessage:chat_Message andImage:[Uploadedimage.image copy]];
        
        [self adddBubbledata:@"textByme" mtext:chat_Message mtime:[formatter stringFromDate:date] mimage:[Uploadedimage.image copy] msgstatus:@"Sending" andMessageFrom:[TTHoushiApi getInstance].currentUser.username];
        
        [self performSelector:@selector(messageSent:) withObject:rowNumber afterDelay:2.0];
    }
    [self closeNoteView];
}

-(IBAction)messageSent:(id)sender
{
    NSLog(@"row= %@", sender);
    
    
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data=[sphBubbledata objectAtIndex:[sender intValue]];
    feed_data.messagestatus=@"Sent";
    [sphBubbledata  removeObjectAtIndex:[sender intValue]];
    [sphBubbledata insertObject:feed_data atIndex:[sender intValue]];
    [self.sphChatTable reloadData];
    //Uploadedimage.hidden = YES;
    
    
}


-(IBAction)uploadImage:(id)sender
{
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose A Source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Photo Library", nil];
    [actionSheet showInView:self.view];
}

-(void)makeImageWithSourceType:(id)sourceType//(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        ((TTAppDelegate*)[UIApplication sharedApplication].delegate).usePortait = YES;
        
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = [((NSNumber*)sourceType) integerValue];
        //UIImagePickerControllerSourceTypePhotoLibrary;
        imgPicker.mediaTypes = [NSArray arrayWithObjects:
                                (NSString *) kUTTypeImage,
                                nil];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSLog(@" Button indexed at %i",buttonIndex);
    if (buttonIndex == 1) {
        [self performSelector:@selector(makeImageWithSourceType:) withObject:[NSNumber numberWithInteger:UIImagePickerControllerSourceTypeCamera] afterDelay:1];
        //[self makeImageWithSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    
    if (buttonIndex == 2) {
        [self performSelector:@selector(makeImageWithSourceType:) withObject:[NSNumber numberWithInteger:UIImagePickerControllerSourceTypePhotoLibrary] afterDelay:1];

        //[self makeImageWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}


//http://www.binarytribune.com/wp-content/uploads/2013/06/india_binary_options-274x300.png

-(void)imagePickerControllerDidCancel:
(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^(){
        ((TTAppDelegate*)[UIApplication sharedApplication].delegate).usePortait = NO;
    }];
    
}



-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info
                           objectForKey:UIImagePickerControllerMediaType];
    
    
    
    [self dismissViewControllerAnimated:YES completion:^(){
        ((TTAppDelegate*)[UIApplication sharedApplication].delegate).usePortait = NO;
    }];
    
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info
                          objectForKey:UIImagePickerControllerOriginalImage];
        
        
        Uploadedimage.image=[image scaleToSize:CGSizeMake(480, 360)];
        
        if (newMedia)
            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),
                                           nil);
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
    
    [self performSelector:@selector(uploadToServer) withObject:nil afterDelay:0.0];
}

-(void)uploadToServer
{
    NSString *chat_Message=textView.text;
    textView.text=@"";
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"hh:mm a"];
    
    NSString *rowNumber=[NSString stringWithFormat:@"%d",sphBubbledata.count];
    
    [self addNoteWithMessage:chat_Message andImage:[Uploadedimage.image copy]];
    
    [self adddBubbledata:@"ImageByme" mtext:chat_Message mtime:[formatter stringFromDate:date] mimage:[Uploadedimage.image copy] msgstatus:@"Sending" andMessageFrom:[TTHoushiApi getInstance].currentUser.username];
    
    [self performSelector:@selector(messageSent:) withObject:rowNumber afterDelay:1.0];
}



-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return sphBubbledata.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data=[sphBubbledata objectAtIndex:indexPath.row];
    
    if ([feed_data.messageType isEqualToString:@"textByme"]||[feed_data.messageType isEqualToString:@"textbyother"])
    {
        float cellHeight;
        // text
        NSString *messageText = feed_data.messageText;
        //
        CGSize boundingSize = CGSizeMake(messageWidth-20, 10000000);
        CGSize itemTextSize = [messageText sizeWithFont:[UIFont systemFontOfSize:14]
                                      constrainedToSize:boundingSize
                                          lineBreakMode:NSLineBreakByWordWrapping];
        
        // plain text
        cellHeight = itemTextSize.height;
        
        if (cellHeight<25) {
            
            cellHeight=25;
        }
        return cellHeight+30;
    }
    else{
        return 140;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.sphChatTable deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data=[sphBubbledata objectAtIndex:indexPath.row];
    if (feed_data.messageImage != nil && [feed_data.messageType isEqualToString:@"textbyother"]) {
        feed_data.messageType = @"ImageByOther";
    }
    NSString *messageText = feed_data.messageText;
    
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
    static NSString *CellIdentifier3 = @"Cell3";
    static NSString *CellIdentifier4 = @"Cell4";
    
    CGSize boundingSize = CGSizeMake(messageWidth-20, 10000000);
    CGSize itemTextSize = [messageText sizeWithFont:[UIFont systemFontOfSize:22]
                                  constrainedToSize:boundingSize
                                      lineBreakMode:NSLineBreakByWordWrapping];
    float textHeight = itemTextSize.height+7;
    int x=0;
    if (textHeight>200)
    {
        x=65;
    }else
        if (textHeight>150)
        {
            x=50;
        }
        else if (textHeight>80)
        {
            x=30;
        }else
            if (textHeight>50)
            {
                x=20;
            }else
                if (textHeight>30) {
                    x=8;
                }
    
    // Types= ImageByme  , imageByOther  textByme  ,textbyother
    
    if ([feed_data.messageType isEqualToString:@"textByme"]) {
        
        
        
        SPHBubbleCellOther  *cell = (SPHBubbleCellOther *)[self.sphChatTable dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCellOther" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        else{
            
        }
        
        
        UIImageView *bubbleImage=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"Bubbletyperight"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
        bubbleImage.tag=55;
        [cell.contentView addSubview:bubbleImage];
        [bubbleImage setFrame:CGRectMake((cell.frame.size.width+635)-itemTextSize.width,5,itemTextSize.width+14,textHeight+4)];
        
        
        UITextView *messageTextview=[[UITextView alloc]initWithFrame:CGRectMake((cell.frame.size.width+635) - itemTextSize.width+5,2,itemTextSize.width+10, textHeight-2)];
        [cell.contentView addSubview:messageTextview];
        messageTextview.editable=NO;
        messageTextview.text = messageText;
        messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
        messageTextview.textAlignment=NSTextAlignmentJustified;
        messageTextview.font=[UIFont fontWithName:@"Helvetica Neue" size:20.0];
        messageTextview.backgroundColor=[UIColor clearColor];
        messageTextview.tag=indexPath.row;
       // cell.Avatar_Image.image=[UIImage imageNamed:@"Customer_icon"];
        
        
        
        cell.time_Label.text=feed_data.messageTime;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        messageTextview.scrollEnabled=NO;
        
        //[cell.Avatar_Image setupImageViewer];
        if ([feed_data.messagestatus isEqualToString:@"Sent"]) {
            
            cell.statusindicator.alpha=0.0;
            [cell.statusindicator stopAnimating];
            cell.statusImage.alpha=1.0;
            [cell.statusImage setImage:[UIImage imageNamed:@"success"]];
            
        }else  if ([feed_data.messagestatus isEqualToString:@"Sending"])
        {
            cell.statusImage.alpha=0.0;
            cell.statusindicator.alpha=1.0;
            [cell.statusindicator startAnimating];
            
        }
        else
        {
            cell.statusindicator.alpha=0.0;
            [cell.statusindicator stopAnimating];
            cell.statusImage.alpha=1.0;
            [cell.statusImage setImage:[UIImage imageNamed:@"failed"]];
            
        }
        
        cell.Avatar_Image.layer.cornerRadius = 20.0;
        cell.Avatar_Image.layer.masksToBounds = YES;
        cell.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.Avatar_Image.layer.borderWidth = 2.0;
        cell.created_by.text = feed_data.messagesfrom;
        
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized:)];
        [messageTextview addGestureRecognizer:singleFingerTap];
        singleFingerTap.delegate = self;
        cell.backgroundColor = [UIColor clearColor];
        cell.created_by.text = feed_data.messagesfrom;
        return cell;
    }
    else
        if ([feed_data.messageType isEqualToString:@"textbyother"]) {
            
            
            SPHBubbleCell  *cell = (SPHBubbleCell *)[self.sphChatTable dequeueReusableCellWithIdentifier:CellIdentifier2];
            
            if (cell == nil) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            else{
                
            }
            //[bubbleImage setFrame:CGRectMake(265-itemTextSize.width,5,itemTextSize.width+14,textHeight+4)];
            UIImageView *bubbleImage=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"Bubbletypeleft"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
            [cell.contentView addSubview:bubbleImage];
            [bubbleImage setFrame:CGRectMake(50,5, itemTextSize.width+18, textHeight+4)];
            bubbleImage.tag=56;
            //CGRectMake(260 - itemTextSize.width+5,2,itemTextSize.width+10, textHeight-2)];
            UITextView *messageTextview=[[UITextView alloc]initWithFrame:CGRectMake(60,2,itemTextSize.width+10, textHeight-2)];
            [cell.contentView addSubview:messageTextview];
            messageTextview.editable=NO;
            messageTextview.text = messageText;
            messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
            messageTextview.textAlignment=NSTextAlignmentJustified;
            messageTextview.backgroundColor=[UIColor clearColor];
            messageTextview.font=[UIFont fontWithName:@"Helvetica Neue" size:20.0];
            messageTextview.scrollEnabled=NO;
            messageTextview.tag=indexPath.row;
            messageTextview.textColor=[UIColor whiteColor];
            cell.Avatar_Image.layer.cornerRadius = 20.0;
            cell.Avatar_Image.layer.masksToBounds = YES;
            cell.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.Avatar_Image.layer.borderWidth = 2.0;
            //[cell.Avatar_Image setupImageViewer];
            
            //cell.Avatar_Image.image=[UIImage imageNamed:@"my_icon"];
            cell.time_Label.text=feed_data.messageTime;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            UITapGestureRecognizer *singleFingerTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized:)];
            [messageTextview addGestureRecognizer:singleFingerTap];
            singleFingerTap.delegate = self;
            cell.backgroundColor = [UIColor clearColor];
            cell.created_by.text = feed_data.messagesfrom;
            return cell;
        }
        else
            if ([feed_data.messageType isEqualToString:@"ImageByme"])
            {
                SPHBubbleCellImage  *cell = (SPHBubbleCellImage *)[self.sphChatTable dequeueReusableCellWithIdentifier:CellIdentifier3];
                if (cell == nil)
                {
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCellImage" owner:self options:nil];
                    cell = [topLevelObjects objectAtIndex:0];
                }
                else
                {
                }
                if ([feed_data.messagestatus isEqualToString:@"Sent"])
                {
                    cell.statusindicator.alpha=0.0;
                    [cell.statusindicator stopAnimating];
                    cell.statusImage.alpha=1.0;
                    [cell.statusImage setImage:[UIImage imageNamed:@"success"]];
                    // cell.message_Image.imageURL=[NSURL URLWithString:feed_data.messageImageURL];
                    
                }
                else
                    if ([feed_data.messagestatus isEqualToString:@"Sending"])
                    {
                        cell.message_Image.image=[UIImage imageNamed:@""];
                        //cell.message_Image.imageURL=[NSURL URLWithString:feed_data.messageImageURL];
                        cell.statusImage.alpha=0.0;
                        cell.statusindicator.alpha=1.0;
                        [cell.statusindicator startAnimating];
                    }
                    else
                    {
                        cell.statusindicator.alpha=0.0;
                        [cell.statusindicator stopAnimating];
                        cell.statusImage.alpha=1.0;
                        [cell.statusImage setImage:[UIImage imageNamed:@"failed"]];
                    }
                cell.message_Image.image=feed_data.messageImage;
                cell.Avatar_Image.layer.cornerRadius = 20.0;
                cell.Avatar_Image.layer.masksToBounds = YES;
                cell.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
                cell.Avatar_Image.layer.borderWidth = 2.0;
                //[cell.Avatar_Image setupImageViewer];
                
                //  cell.Buble_image.image= [[UIImage imageNamed:@"Bubbletyperight"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
                //[cell.message_Image setupImageViewer];
                //cell.Avatar_Image.image=[UIImage imageNamed:@"Customer_icon"];
                cell.time_Label.text=feed_data.messageTime;
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor clearColor];
                UITapGestureRecognizer* singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapRecognized:)];
                [cell addGestureRecognizer:singleFingerTap];
                cell.created_by.text = feed_data.messagesfrom;
                return cell;
                
            }else{
                
                
                SPHBubbleCellImageOther  *cell = (SPHBubbleCellImageOther *)[self.sphChatTable dequeueReusableCellWithIdentifier:CellIdentifier4];
                if (cell == nil)
                {
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCellImageOther" owner:self options:nil];
                    cell = [topLevelObjects objectAtIndex:0];
                }
                else{
                    
                }
                
               // [cell.message_Image setupImageViewer];
                // cell.Buble_image.image= [[UIImage imageNamed:@"Bubbletypeleft"] stretchableImageWithLeftCapWidth:15 topCapHeight:14];
                //cell.message_Image.imageURL=[NSURL URLWithString:@"http://www.binarytribune.com/wp-content/uploads/2013/06/india_binary_options-274x300.png"];
                cell.message_Image.image = feed_data.messageImage;
                cell.Avatar_Image.layer.cornerRadius = 20.0;
                cell.Avatar_Image.layer.masksToBounds = YES;
                cell.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
                cell.Avatar_Image.layer.borderWidth = 2.0;
                //[cell.Avatar_Image setupImageViewer];
                
                //cell.Avatar_Image.image=[UIImage imageNamed:@"my_icon"];
                cell.time_Label.text=feed_data.messageTime;
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor clearColor];
                UITapGestureRecognizer* singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapRecognized:)];
                [cell addGestureRecognizer:singleFingerTap];
                cell.created_by.text = feed_data.messagesfrom;
                return cell;
            }
    
    
}


-(void)adddBubbledata:(NSString*)messageType  mtext:(NSString*)messagetext mtime:(NSString*)messageTime mimage:(UIImage*)messageImage  msgstatus:(NSString*)status andMessageFrom:(NSString*)fromUser;
{
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data.messageText=messagetext;
    feed_data.messageImageURL=messagetext;
    feed_data.messageImage=messageImage;
    feed_data.messageTime=messageTime;
    feed_data.messageType=messageType;
    feed_data.messagestatus=status;
    feed_data.messagesfrom = fromUser;
    [sphBubbledata addObject:feed_data];
    [self.sphChatTable reloadData];
    
    [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
}

-(void)adddBubbledataatIndex:(NSInteger)rownum messagetype:(NSString*)messageType  mtext:(NSString*)messagetext mtime:(NSString*)messageTime mimage:(UIImage*)messageImage  msgstatus:(NSString*)status andMessageFrom:(NSString*)fromUser;
{
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data.messageText=messagetext;
    feed_data.messageImageURL=messagetext;
    feed_data.messageImage=messageImage;
    feed_data.messageTime=messageTime;
    feed_data.messageType=messageType;
    feed_data.messagestatus=status;
    feed_data.messagesfrom = fromUser;
    [sphBubbledata  removeObjectAtIndex:rownum];
    [sphBubbledata insertObject:feed_data atIndex:rownum];
    [self.sphChatTable reloadData];
    
    [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
}

-(void)imageTapRecognized:(UITapGestureRecognizer *)tapGR{
    if ([tapGR.view isKindOfClass:[SPHBubbleCellImage class]]) {
        SPHBubbleCellImage *theImageView = (SPHBubbleCellImage *)tapGR.view;
        _previewImage.image = theImageView.message_Image.image;
    }
    if ([tapGR.view isKindOfClass:[SPHBubbleCellImageOther class]]) {
        SPHBubbleCellImageOther *theImageView = (SPHBubbleCellImageOther *)tapGR.view;
        _previewImage.image = theImageView.message_Image.image;
    }
    _previewImageView.hidden = NO;
}




-(void)tapRecognized:(UITapGestureRecognizer *)tapGR

{
    UITextView *theTextView = (UITextView *)tapGR.view;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:theTextView.tag inSection:0];
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data=[sphBubbledata objectAtIndex:indexPath.row];
    selectedRow=indexPath.row;
    [self.sphChatTable reloadData];
    
    if ([feed_data.messageType isEqualToString:@"textByme"])
    {
        SPHBubbleCellOther *mycell=(SPHBubbleCellOther*)[self.sphChatTable cellForRowAtIndexPath:indexPath];
        UIImageView *bubbleImage=(UIImageView *)[mycell viewWithTag:55];
        bubbleImage.image=[[UIImage imageNamed:@"Bubbletyperight_highlight"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
    }else
        if ([feed_data.messageType isEqualToString:@"textbyother"])
        {
            SPHBubbleCell *mycell=(SPHBubbleCell*)[self.sphChatTable cellForRowAtIndexPath:indexPath];
            UIImageView *bubbleImage=(UIImageView *)[mycell viewWithTag:56];
            bubbleImage.image=[[UIImage imageNamed:@"Bubbletypeleft_highlight"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        }
    CGPoint touchPoint = [tapGR locationInView:self.view];
    [self.popupMenu showInView:self.view atPoint:touchPoint];
    
    
    [self.sphChatTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    [self.sphChatTable.delegate tableView:self.sphChatTable didSelectRowAtIndexPath:indexPath];
}

-(IBAction)bookmarkClicked:(id)sender
{
    NSLog( @"Book mark clicked at row : %d",selectedRow);
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}


-(void)scrollTableview
{
    [self.sphChatTable reloadData];
    int lastSection=[self.sphChatTable numberOfSections]-1;
    int lastRowNumber = [self.sphChatTable numberOfRowsInSection:lastSection]-1;
    NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:lastSection];
    [self.sphChatTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

-(void) keyboardWillShow:(NSNotification *)note
{
    if (sphBubbledata.count>2) {
        
        [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
    }
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
    CGRect tableviewframe=self.sphChatTable.frame;
    tableviewframe.size.height-=160;
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	containerView.frame = containerFrame;
    self.sphChatTable.frame=tableviewframe;
    
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    CGRect tableviewframe=self.sphChatTable.frame;
    tableviewframe.size.height+=160;
    
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	// set views with new info
    self.sphChatTable.frame=tableviewframe;
	containerView.frame = containerFrame;
	// commit animations
	[UIView commitAnimations];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
	CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	containerView.frame = r;
}




- (IBAction)endViewedit:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)takePicture:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        if ([mediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            [self presentModalViewController:picker animated:YES];
        }
    }
}

- (void)dismissImagePicker
{
    [self dismissModalViewControllerAnimated:YES];
}


- (NSUInteger) supportedInterfaceOrientations
{
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}
#define MAX_IMAGE_WIDTH 200




- (IBAction)closePreviewView:(id)sender {
    _previewImageView.hidden = YES;
}
@end
