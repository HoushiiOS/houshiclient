//
//  TTNoteViewController.h
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "MNMPullToRefreshManager.h"

@class QBPopupMenu;

@interface TTNoteViewController : UIViewController<HPGrowingTextViewDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MNMPullToRefreshManagerClient>
{
    NSMutableArray *sphBubbledata;
    UIView *containerView;
    HPGrowingTextView *textView;
    int selectedRow;
    BOOL newMedia;
}
@property (nonatomic, readwrite, assign) NSUInteger reloads;
@property (nonatomic, readwrite, strong) MNMPullToRefreshManager *pullToRefreshManager;
@property (weak, nonatomic) IBOutlet UIView *previewImageView;
- (IBAction)closePreviewView:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *Uploadedimage;
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;

@property (nonatomic, strong) QBPopupMenu *popupMenu;
@property (weak, nonatomic) IBOutlet UITableView *sphChatTable;
@property (nonatomic, retain) UIImagePickerController *imgPicker;

- (IBAction)endViewedit:(id)sender;

- (void) handleURL:(NSURL *)url;
- (IBAction)takePicture:(id)sender;
@end