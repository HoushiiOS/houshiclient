//
//  TTViewController.h
//  Houshi
//
//  Created by JT on 5/8/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTViewController : UIViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *Firsttextfield;
@property (weak, nonatomic) IBOutlet UIView *numberPadView;
@property (weak, nonatomic) IBOutlet UIPickerView *namePicker;
- (IBAction)onSignInClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *MainMenuHeader;
- (IBAction)onButtonClicked:(id)sender;
- (IBAction)onLogOutClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vwContainer;
@property (weak, nonatomic) IBOutlet UIView *viewWithUtilButtons;
-(BOOL)savedOrdersRemaining;
@end
