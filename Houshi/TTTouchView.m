//
//  TTTouchView.m
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTTouchView.h"

@implementation TTTouchView


- (id)initWithFrame:(CGRect)frame {
    
	self = [super initWithFrame:frame];
    
    
    
	if (self) {
        
		//this will simplify things later - for a signature capture
        
		//interface, there should only be one finger doing the drawing
        
		self.multipleTouchEnabled = NO;
        
	}
    
	return self;
    
}



//override the touch handlers

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
	[super touchesBegan:touches withEvent:event];
    
    
    
	if ([delegate respondsToSelector:@selector(touchesBegan:withEvent:)])
        
        [delegate touchesBegan:touches withEvent:event];
    
}



-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
	[super touchesEnded:touches withEvent:event];
    
    
    
	if ([delegate respondsToSelector:@selector(touchesEnded:withEvent:)])
        
        [delegate touchesEnded:touches withEvent:event];
    
}



-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
 	[super touchesMoved:touches withEvent:event];
    
    
    
	if ([delegate respondsToSelector:@selector(touchesMoved:withEvent:)])
        
        [delegate touchesMoved:touches withEvent:event];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
