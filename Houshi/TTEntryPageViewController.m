//
//  TTEntryPageViewController.m
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTEntryPageViewController.h"
#import "TTInventoryTableViewCell.h"
#import "TTOrderDTO.h"
#import "TTOrderItemDTO.h"
#import "TTAppConatants.h"
#import "TTOrderUtils.h"
#import "TTCurrentWorkingObjects.h"
#import "CRToast.h"

@interface TTEntryPageViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) TTOrderDTO* order;
@end

@implementation TTEntryPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Opened");
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)autoSaveTempOrder{
    __block NSString* directory;
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY ) {
        directory = ORDER_SAVE_DIRECTORY_TEMP_INVENTORY;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_ORDER ) {
        directory = ORDER_SAVE_DIRECTORY_TEMP_ORDER;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP ) {
        directory = ORDER_SAVE_DIRECTORY_TEMP_PPP;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER ) {
        directory = ORDER_SAVE_DIRECTORY_TEMP_QUICK_ORDER;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY ) {
        directory = ORDER_SAVE_DIRECTORY_TEMP_DELIVERY;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        NSArray* savedOrderArry = [TTOrderUtils getOrderArrayFromDirectoryType:directory];
        for (TTOrderDTO* order in savedOrderArry) {
            [TTOrderUtils deleteOrder:order inDirectory:directory];
        }
        [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:directory];
        NSLog(@"ORDER SUCCESSFULLY SAVED");
    });
    
    NSDictionary *options = @{
                              kCRToastTextKey : @"Auto Save Completed",
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColor blackColor],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];

    

}



-(void)saveOrderToSendLater{
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY)
        return;
    __block NSString* directory;
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY ) {
        directory = ORDER_SAVE_DIRECTORY_INVENTORY;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_ORDER ) {
        directory = ORDER_SAVE_DIRECTORY_ORDER;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP ) {
        directory = ORDER_SAVE_DIRECTORY_PPP;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER ) {
        directory = ORDER_SAVE_DIRECTORY_QUICK_ORDER;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY ) {
        directory = ORDER_SAVE_DIRECTORY_DELIVERY;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:directory];
        NSLog(@"ORDER SUCCESSFULLY SAVED");
    });
    
    NSDictionary *options = @{
                              kCRToastTextKey : @"ORDER WILL BE SAVED FOR LATER",
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColor blackColor],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];

}

-(BOOL)areAllQuantitiesFilled{
    BOOL isNotFilled;
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_INVENTORY ) {
        for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem ) {
            isNotFilled = (item.qtyFront == nil || [item.qtyFront isEqualToString:@""]);
            if (isNotFilled) {
                break;
            }
            isNotFilled = (item.qtyBack == nil || [item.qtyBack isEqualToString:@""]);
            if (isNotFilled) {
                break;
            }
           // isNotFilled = (item.qtyOrderSuggested == nil || [item.qtyOrderSuggested isEqualToString:@""]);
        }
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_ORDER ) {
        for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem ) {
            isNotFilled = (item.qtyOrdered == nil || [item.qtyOrdered isEqualToString:@""]);
            if (isNotFilled) {
                isNotFilled = (item.qtyOrderSuggested == nil || [item.qtyOrderSuggested isEqualToString:@""]);
                if (isNotFilled) {
                    break;
                }
            }
        }
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP ) {
        for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem ) {
            isNotFilled = (item.productPull.qty == nil || [item.productPull.qty isEqualToString:@""]);
            if (isNotFilled) {
                break;
            }
        }
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER ) {
        for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem ) {
            isNotFilled = (item.qtyOrdered == nil || [item.qtyOrdered isEqualToString:@""]);
            if (isNotFilled == false) {
                break;
            }
        }
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY ) {
        for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem ) {
            isNotFilled = (item.productDelivery.qtyDelivered == nil || [item.productDelivery.qtyDelivered isEqualToString:@""]);
            if (isNotFilled) {
                break;
            }
        }
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT ) {
        for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem ) {
            isNotFilled = (item.productDelivery.qtyDelivered == nil || [item.productDelivery.qtyDelivered isEqualToString:@""]);
            if (isNotFilled == false) {
                break;
            }
        }
    }
    return !isNotFilled;
}

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
////TODO add view header field
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return 1;
//}
//
//-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    TTInventoryTableViewCell* cell =
//    (TTInventoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"InvCell"];
//    [cell setDataObject:_order.orderItem[indexPath.row]];
//    return cell;
//}
//
//-(void)demoOrderItems{
//    _order = [TTOrderDTO new];
//    NSMutableArray* orderItems = [NSMutableArray new];
//    //_order.orderItem =
//    
//    TTOrderItemDTO* item = [TTOrderItemDTO new];
//    item.product.desc = @"Huggies 2 PCT";
//    item.product.sku = @"6190031";
//    item.qtyBack = @"20";
//    item.qtyFront = @"40";
//    item.qtyOrdered = @"0";
//    
//    [orderItems addObject:item];
//    
//    item = [TTOrderItemDTO new];
//    item.product.desc = @"Johnson & Johnson Baby Lotion";
//    item.product.sku = @"6190032";
//    item.qtyBack = @"30";
//    item.qtyFront = @"10";
//    item.qtyOrdered = @"4";
//    
//    [orderItems addObject:item];
//    
//    item = [TTOrderItemDTO new];
//    item.product.desc = @"Frosted Flakes 24 CT";
//    item.product.sku = @"6190033";
//    item.qtyBack = @"10";
//    item.qtyFront = @"3";
//    item.qtyOrdered = @"1";
//    
//    [orderItems addObject:item];
//    
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
