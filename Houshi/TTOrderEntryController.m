//
//  TTOrderEntryController.m
//  Houshi
//
//  Created by James Timberlake on 9/7/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTOrderEntryController.h"
#import "TTCurrentWorkingObjects.h"
#import "TTOrderDTO.h"
#import "TTOrderItemDTO.h"
#import "LNNumberpad.h"
#import "DropDownListView.h"
#import "TTVerifyPageViewController.h"
#import "TTAppConatants.h"
#import "LHDropDownControlView.h"
#import "TTOrderHistoryTableViewCell.h"
#import "TTOrderHistoryDTO.h"
#import "TTHoushiApi.h"
#import "TTCustomerProductDTO.h"
#import "TTSearchCustomerProductResponse.h"
#import "TTOrderItemSelectPopup.h"
#import "TTOrderUtils.h"

NSComparator sortQuantities4 = ^(id num1, id num2)
{
    float v1 = ((NSString*)num1).floatValue;
    float v2 = ((NSString*)num2).floatValue;
    
    if( v1 < v2) return NSOrderedAscending;
    else if (v1 > v2) return NSOrderedDescending;
    else return NSOrderedSame;
};

@interface TTOrderEntryController ()<UITableViewDelegate, UITableViewDataSource, kDropDownListViewDelegate, UITextFieldDelegate, LNNumberPadDelegate,   LHDropDownControlViewDelegate, TTHoughiApiDelegate, TTOrderItemSelectPopupDelegate>
{
    int currentItemIndex;
    TTOrderDTO* currentOrder;
    TTCustomerProductDTO* currentCustProd;
    int invState;
    int currentRowIndex;
    int currentTextFieldIndex;
    int currentPagingNum;
    BOOL changingRows;
    BOOL changedNumberFromOrderSuggested;
    NSMutableArray* cells;
    UISwipeGestureRecognizer* gesture;
    UISwipeGestureRecognizer* gesture1;
}
@property (nonatomic, strong) TTOrderDTO* order;
@property (nonatomic, strong) UIView* ddView;
@property (nonatomic, strong) LNNumberpad* ln;
@property (nonatomic, strong) LNNumberpad* hex;
@end

@implementation TTOrderEntryController{
    LHDropDownControlView* dropDownView;
    NSMutableArray *titles;
    NSMutableArray *options;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)getOrderItemInfo
{
    [TTHoushiApi getCustomerProductByProductId:((TTOrderItemDTO*)currentOrder.orderItem[currentItemIndex]).productId.intValue];
}

-(void)setCurrentOrderItem
{
    
    TTOrderItemDTO* orderItem = (TTOrderItemDTO*)currentOrder.orderItem[currentItemIndex];
    
    //set top bar
    int invTotal = orderItem.qtyBack.intValue +
    orderItem.qtyFront.intValue;
    _modelStock.text = currentCustProd.recommendedQty;
    _invAmt.text = [NSString stringWithFormat:@"%i",invTotal];
    _orderAmt.text = (orderItem.qtyOrdered && ![orderItem.qtyOrdered isEqualToString:@""]) ? orderItem.qtyOrdered : (orderItem.qtyOrderSuggested && ![orderItem.qtyOrderSuggested isEqualToString:@""]) ? orderItem.qtyOrderSuggested : @"";
    if (!orderItem.qtyOrdered || [orderItem.qtyOrdered isEqualToString:@""] ) {
        orderItem.qtyOrdered = orderItem.qtyOrderSuggested;
    }
    _skuLabel.text = orderItem.product.sku;
    _descLabel.text = orderItem.product.desc;
    
    //set table view
    [self.tableview reloadData];
    
    
    //set bottom view
    [_hex setNumberPadByInt:currentCustProd.casePack.intValue];
    
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    invState = 0;
    currentPagingNum = 0;
    currentItemIndex = 0;
    _orderAmt.userInteractionEnabled = YES;
    _orderAmt.enabled = YES;
    _orderAmt.delegate = self;
    
    currentOrder = [TTCurrentWorkingObjects currentOrder];
    cells = [NSMutableArray new];
    NSLog(@"Opened");
    _ln = [LNNumberpad defaultLNNumberpad];
    _ln.delegate = self;
    [_numberPadView addSubview:_ln];
    _hex = [LNNumberpad hexLNNumberpad];
    _hex.delegate = self;
    [_hex setHexNumberPad];
    [_hexNotePadView addSubview:_hex];
    /*[self demoOrderItems];*/
    [_ln setCurrentTextField:_orderAmt];
    [_hex setCurrentTextField:_orderAmt];
    

    
    //constant
    AUTO_SAVE_COUNT_MAX = 10;
    autoSaveCount = 0;
    
    _order = [TTCurrentWorkingObjects currentOrder];
    _headerLabel.text = _order.customer.name;
    _headerEventName.text = ( _order.eventName && ![_order.eventName isEqualToString:@""]) ? [[NSString alloc] initWithFormat:@"EVENT NAME: %@",_order.eventName] : @"";
    _headerOccupancyRate.text = _order.occupancyRate ? [[NSString alloc] initWithFormat:@"OCCUPANCY RATE: %@",_order.occupancyRate] : @"";
    
    [TTHoushiApi setDelegate:self];
    
    NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"product.warehouseBin" ascending: YES];
    
    _order.orderItem = [_order.orderItem sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]];
    
    //open the notes sections when this page first loads
    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:nil];
    
    [self getOrderItemInfo];
    
    [HelperMethods convertCapitalForView:self.view];
    
}

-(int)findNearestRowPagingIndex:(int)currentIndex goingUpOrDown:(BOOL)isUp
{
    float multiple;
    if (!isUp) {
        multiple = roundf((currentIndex/[TTAppConatants orderItemPagingSize])) + 1;
    }else{
        multiple = roundf((currentIndex/[TTAppConatants orderItemPagingSize])) - 1;
    }
    return (int)(multiple*[TTAppConatants orderItemPagingSize]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onEventValuesChanged:(id)sender {
    [TTCurrentWorkingObjects currentOrder].eventName = _eventName.text;
    _headerEventName.text = [TTCurrentWorkingObjects currentOrder].eventName || [[TTCurrentWorkingObjects currentOrder].eventName isEqualToString:@""] ? [[NSString alloc] initWithFormat:@"EVENT NAME: %@",[TTCurrentWorkingObjects currentOrder].eventName] : @"";
    _headerOccupancyRate.text = [TTCurrentWorkingObjects currentOrder].occupancyRate ? [[NSString alloc] initWithFormat:@"OCCUPANCY RATE: %@",[TTCurrentWorkingObjects currentOrder].occupancyRate] : @"";
}

- (IBAction)onBackButtonPressed:(id)sender {
    currentItemIndex -= 1;
    if (currentItemIndex <= 0) {
        currentItemIndex = 0;
    }    
    [self getOrderItemInfo];
}

-(void)switchTypes:(id)sender
{
    invState ++;
    if(invState > 3)
        invState = 0;
    [cells removeAllObjects];
    [_tableView reloadData];
    
}

-(void)updateOrderItem:(int)index  changingAmtTo:(NSString*)amount
{
    @try {
        TTOrderItemDTO* orderItem = (TTOrderItemDTO*)_order.orderItem[index];
        if (orderItem.qtyOrderSuggested && ![orderItem.qtyOrderSuggested isEqualToString:@""] && orderItem.qtyOrderSuggested.intValue != amount.intValue) {
            changedNumberFromOrderSuggested = true;
        }
        orderItem.qtyOrdered = amount;
    }
    @catch (NSException *exception) {
        
    }
}

-(void)onTextFieldValueChanged:(UITextField*)textField
{
    [self updateOrderItem:currentItemIndex changingAmtTo:textField.text];
}

-(void)onSpecialKeyPressed:(NSString *)keyName fromTextField:(UITextField*)textField
{
 
    NSLog(@"Text value %@",textField.text);
    if ([textField.text isEqualToString:@""]) {
        
        [[TTHoushiApi getInstance] showAlertWithOnlyOkButton:@"PLEASE ENTER ORDER NUMBER." performingTheResutInBlock:^{
            
            [_orderAmt becomeFirstResponder];
            
        } andFailure:^{
            
        }];
        return;
    }
    if([keyName isEqualToString:@"done"])
    {
        if (changedNumberFromOrderSuggested) {
            NSString* qtySuggested = ((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).qtyOrderSuggested;
            NSString* qtyOrder = textField.text;
            [[TTHoushiApi getInstance] showConfirmAlert:
             [[NSString alloc] initWithFormat:@"YOU ARE CHANGING THE ORDER FROM THE SUGGESTED AMOUNT (%@). DO YOU WANT TO KEEP THE ORIGINAL OR CHANGE IT (%@)?",qtySuggested,qtyOrder]
                                          withNoMessage:qtySuggested andYesText:qtyOrder performingTheResutInBlock:^(){
                                              changedNumberFromOrderSuggested = NO;
                [self onSpecialKeyPressed:keyName fromTextField:textField];
                                          }andFailure:^(){
                                              changedNumberFromOrderSuggested = NO;
                textField.text = ((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).qtyOrderSuggested;
                                              ((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).qtyOrdered = qtySuggested;
                [self onSpecialKeyPressed:keyName fromTextField:textField];
            }];
            return;
        }
        
        currentItemIndex += 1;
        if (currentItemIndex >= currentOrder.orderItem.count) {
            currentItemIndex = (currentOrder.orderItem.count - 1);
            [self onEndOfTable];
        }else{
            [self getOrderItemInfo];
        }
        autoSaveCount++;
        if (autoSaveCount >= AUTO_SAVE_COUNT_MAX) {
            [self autoSaveTempOrder];
            autoSaveCount = 0;
        }
        
    }
}

-(void)onEndOfTable
{
    [self performSelector:@selector(goToVerifyScreen) withObject:nil afterDelay:1];
    //[self goToVerifyScreen];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[TTVerifyPageViewController class]]) {
        ((TTVerifyPageViewController*)segue.destinationViewController).verifyType = @"Order";
    }
}

//this is the return for selecting an order item in a list after TTOrderItemSelectPopup occurs
-(void)onItemSelected:(int)index
{
    currentItemIndex = index;
    [self getOrderItemInfo];
}


-(void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    NSMutableArray* itemNames = [NSMutableArray  new];
   // switch (anIndex) {
            //Switch Order Items
    
    //@"VERIFY ORDER"
    if( anIndex ==0){
        [self goToVerifyScreen];
        
    }

    //@"SWITCH ORDER ITEMS"
    if (anIndex ==1){
            //
            for (TTOrderItemDTO* item in _order.orderItem) {
                [itemNames addObject:item.product.desc];
            }
        
            [[TTOrderItemSelectPopup alloc] initIntoView:self.view withItems:itemNames havingTheTitle:@"CHOOSE AN ORDER ITEM" withDelegate:self];
        
    }
    
    //@""
    if( anIndex ==2){
        
    }
    
    //@"SETTINGS"
    if(anIndex == 3){
        //@"Settings"
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsView"] animated:YES completion:nil];
    }
    
    //@"SWITCH SIDES",
    if(anIndex == 4){
        
        if (_contentView.frame.origin.x == 0) {
            [_contentView setFrame:CGRectOffset(_contentView.frame, _sideView.frame.size.width, 0)];
            [_sideView setFrame:CGRectOffset(_sideView.frame, -(self.view.frame.size.width - _sideView.frame.size.width), 0)];
        }else{
            [_contentView setFrame:CGRectOffset(_contentView.frame, -_sideView.frame.size.width, 0)];
            [_sideView setFrame:CGRectOffset(_sideView.frame, (self.view.frame.size.width - _sideView.frame.size.width), 0)];
        }
    }
    
    //@"OPEN NOTES",
    if(anIndex == 5){
        // @"Open Notes"
        //open notes
        
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:nil];
        
    }
    
    //@"",
    if( anIndex ==6){
        
    }
    
    //@"RETURN TO MAIN MENU",
    if(anIndex == 7){
        [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO THE MAIN MENU SCREEN. DO YOU WANT TO CONTINUE?"
                                      withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
            //@"Main Menu"
            [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
                                          
        }];
    }
    
    //@"EXIT OPTIONS"
    if(anIndex == 8){
            //@"Exit"
    }
//    if(anIndex == 7){
//
//            break;
//        case 8:
//            //[self scrollTableTo:20];
//            break;
//        case 9:
//
//            //[self scrollTableTo:20];
//            break;
//        default:
//            break;
 //   }
    [_ddView removeFromSuperview];
    _ddView = nil;
}

-(void)goToVerifyScreen{
    if (![self areAllQuantitiesFilled]) {
        [[[UIAlertView alloc] initWithTitle:@"NOT FILLED OUT" message:@"THIS ORDER IS NOT FILLED OUT ENTIRELY!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    //we need to set the quantity ordered field if it is not set
    for (TTOrderItemDTO* item in _order.orderItem ) {
        BOOL isNotFilled = (item.qtyOrdered == nil || [item.qtyOrdered isEqualToString:@""]);
        if (isNotFilled) {
            item.qtyOrdered = item.qtyOrderSuggested;
            
        }
    }
    
    [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO TO THE VERIFY SCREEN. DO YOU WANT TO CONTINUE?"
                                  withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                                      //@"Verify"
                                      [TTCurrentWorkingObjects setCurrentOrder:_order];
                                      [self performSegueWithIdentifier:@"VerifySegue" sender:self];
                                  }];

}

-(void)paginateTableTo:(int)position
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:position inSection:0];
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)scrollTableTo:(int)position
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:position inSection:0];
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop  animated:YES];
}

- (IBAction)onOptionSelect:(id)sender {
    
    _ddView = [[UIView alloc] initWithFrame:self.view.frame];
    [_ddView setBackgroundColor:[UIColor grayColor]];
   /* [_ddView setAlpha:0.7];
    DropDownListView* dView = [[DropDownListView alloc] initWithTitle:@"MENU OPTIONS" options:@[ @"SWITCH ORDER ITEMS", @"VERIFY", @"MAIN MENU",@"SWITCH SIDES", @"OPEN NOTES", @"SETTINGS",@"EXIT"] xy:CGPointMake(350, 200) size:CGSizeMake(300,535)
                                                           isMultiple:NO];
    [dView SetBackGroundDropDwon_R:42.0 G:43.0 B:44.0 alpha:0.70];*/
    [_ddView setAlpha:1.0];
    DropDownListView* dView = [[DropDownListView alloc] initWithTitle:@"OPTIONS" options:@[@"VERIFY ORDER",
                                                                                           @"SELECT LINE ITEM",
                                                                                           @"",
                                                                                           @"SETTINGS",
                                                                                           @"SWITCH SIDES",
                                                                                           @"OPEN NOTES",
                                                                                           @"",
                                                                                           @"RETURN TO MAIN MENU",
                                                                                           @"EXIT OPTIONS"] xy:CGPointMake(350, 100) size:CGSizeMake(300,497)
                                                           isMultiple:NO];
    [dView SetBackGroundDropDwon_R:42.0 G:43.0 B:44.0 alpha:1.0];
    [self.view addSubview:_ddView];
    dView.delegate = self;///42	43	44
    [_ddView addSubview:dView];/*****/
}

-(void)refreshTableView:(id)sender
{
    //[self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    currentOrder = [TTCurrentWorkingObjects currentOrder];
    [TTHoushiApi setDelegate:self];
}


- (void)dropDownControlView:(LHDropDownControlView *)view didFinishWithSelection:(id)selection{
    NSLog(@"Ok");
    @try {
        view.title = (NSString*)[titles objectAtIndex:[((NSNumber*)selection) intValue]];
        [TTCurrentWorkingObjects currentOrder].occupancyRate = (NSString*)[titles objectAtIndex:[((NSNumber*)selection) intValue]];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//TODO add view header field

-(void)populateeroHistoryItems
{
    NSMutableArray* temp = (NSMutableArray*)[((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).OrderHistory mutableCopy];
    if (!temp) {
        temp = [NSMutableArray new];
    }
    for(int i = (temp.count >0) ? (temp.count-1) : 0; i<4; i++)
    {
        TTOrderHistoryDTO* hist = [TTOrderHistoryDTO new];
        hist.qtyFront = @"0";
        hist.qtyBack = @"0";
        hist.qtyDelivered = @"0";
        hist.qtyOrdered = @"0";
        [temp addObject:hist];
        
    }
    ((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).orderHistory = [NSArray arrayWithArray:temp];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).OrderHistory.count < 4) {
        [self populateeroHistoryItems];
    }
    
    return ((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).OrderHistory.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTOrderHistoryTableViewCell* cell = (TTOrderHistoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"OrderHistCell"];;
    switch (indexPath.row) {
        case 0:
            cell.titleLabel.text = @"LAST ORDER";
            break;
            
        default:
            cell.titleLabel.text = [NSString stringWithFormat:@"%i ORDERS AGO",indexPath.row];
            break;
    }
    TTOrderHistoryDTO* ordHist = (TTOrderHistoryDTO*)((TTOrderItemDTO*)_order.orderItem[currentItemIndex]).OrderHistory[indexPath.row];
    cell.histInvAmt.text = [NSString stringWithFormat:@"%i",(ordHist.qtyFront.intValue + ordHist.qtyBack.intValue)];
    cell.histOrdAmt.text = ordHist.qtyOrdered;
    cell.histDelAmt.text = ordHist.qtyDelivered;
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSArray* arry = [[NSBundle mainBundle] loadNibNamed:@"TTOrderHistSectionView" owner:nil options:nil];
    return arry[0];
}

-(void)onOperationSuccess:(TTBaseResponse *)response
{
    if([response isKindOfClass:[TTSearchCustomerProductResponse class]])
    {
        currentCustProd = (TTCustomerProductDTO*)((TTSearchCustomerProductResponse*)response).customerProduct[0];
        [self setCurrentOrderItem];
    }
    
}

-(void)onOperationError:(TTBaseResponse *)error
{
    
}


@end
