//
//  TTOrderEntryController.h
//  Houshi
//
//  Created by James Timberlake on 9/7/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTEntryPageViewController.h"

@interface TTOrderEntryController : TTEntryPageViewController
@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UIView *numberPadView;
@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *headerOccupancyRate;
@property (weak, nonatomic) IBOutlet UITextField *modelStock;
@property (weak, nonatomic) IBOutlet UITextField *invAmt;
@property (weak, nonatomic) IBOutlet UITextField *orderAmt;
@property (weak, nonatomic) IBOutlet UIView *hexNotePadView;
@property (weak, nonatomic) IBOutlet UILabel *skuLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerEventName;
@property (weak, nonatomic) IBOutlet UITextField *eventName;
- (IBAction)onOptionSelect:(id)sender;
-(void)setTableToScrollType:(BOOL)isSetToScroll;
@property (nonatomic,weak) IBOutlet UILabel* headerLabel;
- (IBAction)onEventValuesChanged:(id)sender;
- (IBAction)onEventCanceled:(id)sender;
- (IBAction)onBackButtonPressed:(id)sender;

-(IBAction)switchTypes:(id)sender;
-(void)refreshTableView:(id)sender;
@end
