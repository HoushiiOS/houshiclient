//
//  TTVerifyPageViewController.m
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTVerifyPageViewController.h"
#import "TTCurrentWorkingObjects.h"
#import "TTOrderDTO.h"
#import "TTOrderItemDTO.h"
#import "TTVerifyPageTableViewCell.h"
#import "TTHoushiApi.h"
#import "TTSaveMultipleNoteResponse.h"
#import "TTSavePullItemsResponse.h"
#import "TTInvAllSectionView.h"
#import "TTOrderUtils.h"
#import "TTSaveOrderNetworkErrorResponse.h"

@interface TTVerifyPageViewController ()<UITableViewDelegate, UITableViewDataSource, TTHoughiApiDelegate>{
    BOOL areOrdersAllZero;
    BOOL arePPPOrdersAllZero;
}
@property (nonatomic, strong) TTOrderDTO* order;
@end

@implementation TTVerifyPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _order = [TTCurrentWorkingObjects currentOrder];
    [TTHoushiApi setDelegate:self];
    areOrdersAllZero = YES;
    arePPPOrdersAllZero = YES;
    if ([_verifyType isEqual:@"Order"] ||
        [_verifyType isEqual:@"PPP"] ) {
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:nil];
    }

    //[_tableView reloadData];
    // Do any additional setup after loading the view.
    [self setOrderTypeButtonHighlights];
}

-(void)setOrderTypeButtonHighlights{
    _dryGButton.nonHighlightedImage = [UIImage imageNamed:@"button_default.png"];
    _dryGButton.highlightedImage = [UIImage imageNamed:@"button_click.png"];
    
    _sundriesButton.nonHighlightedImage = [UIImage imageNamed:@"button_default.png"];
    _sundriesButton.highlightedImage = [UIImage imageNamed:@"button_click.png"];
    
    _coldGButton.nonHighlightedImage = [UIImage imageNamed:@"button_default.png"];
    _coldGButton.highlightedImage = [UIImage imageNamed:@"button_click.png"];
    [_coldGButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    _frzButton.nonHighlightedImage = [UIImage imageNamed:@"button_default.png"];
    _frzButton.highlightedImage = [UIImage imageNamed:@"button_click.png"];
    [_frzButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    _rushButton.nonHighlightedImage = [UIImage imageNamed:@"button_default.png"];
    _rushButton.highlightedImage = [UIImage imageNamed:@"button_click.png"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onVerifySelect:(id)sender{

    if ([_verifyType isEqual:@"Inventory"] || [_verifyType isEqual:@"QOrder"]) {
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:^(){
           [_orderTypeView setHidden:NO];
        }];
    }else if([_verifyType isEqual:@"Delivery"] || [_verifyType isEqual:@"Credit"] || [_verifyType isEqual:@"Credit"]){
        [self onOrderTypeVerifyConfirm:nil];
    }else{
        [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO TO SUBMIT AN ORDER. DO YOU WANT TO CONTINUE?"
                                      withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                                          [self onOrderTypeVerifyConfirm:nil];
                                      }];
    }
   // [TTHoushiApi saveOrder:ORDER_STATE_INVENTORY_COMPLETED];
//    [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
}

- (IBAction)onBackSelect:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//TODO add view header field

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _order.orderItem.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    if ([_verifyType isEqual:@"Inventory"]) {
        TTVerifyPageTableViewCell* cell =
        (TTVerifyPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VerifyCell"];
        
        [cell setDataObject:_order.orderItem[indexPath.row] atVerifyType:0];
        
        return cell;
    }else if([_verifyType isEqual:@"Order"] || [_verifyType isEqual:@"QOrder"]){
        TTVerifyPageTableViewCell* cell =
        (TTVerifyPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VerifyCell"];
        if (!((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered ||  [((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered isEqualToString:@""]) {
            ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrderSuggested;
        }
        
        [cell setDataObject:_order.orderItem[indexPath.row] atVerifyType:1];
        if(((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered.intValue != 0){
            areOrdersAllZero = NO;
        }
        
        return cell;
    }else if([_verifyType isEqual:@"PPP"]){
        TTVerifyPageTableViewCell* cell =
        (TTVerifyPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VerifyCell"];
        
        [cell setDataObject:_order.orderItem[indexPath.row] atVerifyType:2];
        if(((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty.intValue != 0){
            arePPPOrdersAllZero = NO;
        }

        return cell;
    }else if([_verifyType isEqual:@"Delivery"] || [_verifyType isEqual:@"Credit"]){
        TTVerifyPageTableViewCell* cell =
        (TTVerifyPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VerifyCell"];
        
        [cell setDataObject:_order.orderItem[indexPath.row] atVerifyType:3];
        
        return cell;
    }
    
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if (invState != 3) {
//        TTInvSingleSectionView * view = [[TTInvSingleSectionView alloc] init];
//        
//        if(invState == 0)
//            view.itemType.text = @"Pres";
//        
//        if(invState == 1)
//            view.itemType.text = @"Back";
//        
//        if(invState == 2)
//            view.itemType.text = @"Order";
//        
//        return view;
//    }
//
/*    if ([_verifyType isEqual:@"PPP"]) {
        
        TTInvAllSectionView* view = [[TTInvAllSectionView alloc] init];
        view.itemType1.hidden = YES;
        view.itemType2.text = @"ORDERED";
        view.itemType3.text = @"PULLED";
        return view;
    }else if([_verifyType isEqual:@"Delivery"]) {
        TTInvAllSectionView* view = [[TTInvAllSectionView alloc] init];
        view.itemType1.hidden = YES;
        view.itemType2.text = @"PULLED";
        view.itemType3.text = @"DELIVERED";
        return view;
    }*/
    return nil;
}

-(void)onOperationSuccess:(TTBaseResponse *)response
{
    if (![response isKindOfClass:[TTSaveMultipleNoteResponse class]]) {
        if([response isKindOfClass:[TTSavePullItemsResponse class]])
            if (arePPPOrdersAllZero)
                [TTHoushiApi saveOrder:ORDER_STATE_INCOMPLETE];
        NSString* orderDirectory;
        NSString* orderRecentHistory;
        if ([_verifyType isEqual:@"Inventory"]) {
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_INVENTORY;
            orderRecentHistory = ORDER_SAVE_DIRECTORY_HISTORY_INVENTORY;
        }else if([_verifyType isEqual:@"Order"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_ORDER;
            orderRecentHistory = ORDER_SAVE_DIRECTORY_HISTORY_ORDER;
        }else if([_verifyType isEqual:@"QOrder"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_QUICK_ORDER;
            orderRecentHistory = ORDER_SAVE_DIRECTORY_HISTORY_QUICK_ORDER;
        }else if([_verifyType isEqual:@"PPP"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_PPP;
            orderRecentHistory = ORDER_SAVE_DIRECTORY_HISTORY_PPP;
        }else if([_verifyType isEqual:@"Delivery"] ){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_DELIVERY;
            orderRecentHistory = ORDER_SAVE_DIRECTORY_HISTORY_DELIVERY;
        }else if([_verifyType isEqual:@"Credit"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_CREDIT;
            orderRecentHistory = ORDER_SAVE_DIRECTORY_HISTORY_CREDIT;
        }
        NSArray* orderArry =  [TTOrderUtils getOrderArrayFromDirectoryType:orderDirectory];
        for (TTOrderDTO* order in orderArry ) {
            [TTOrderUtils deleteOrder:order inDirectory:orderDirectory];
        }
        if(![_verifyType isEqual:@"Delivery"] && ![_verifyType isEqual:@"Credit"]){
            [TTCurrentWorkingObjects currentOrder].workingUserId = nil;
            [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:orderRecentHistory];
        }
        [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
    }
}

-(void)onOperationError:(TTBaseResponse *)error{
    if ([error isKindOfClass:[TTSaveOrderNetworkErrorResponse class]]) {
        NSString* orderDirectory;
        if ([_verifyType isEqual:@"Inventory"]) {
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_INVENTORY;
        }else if([_verifyType isEqual:@"Order"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_ORDER;
        }else if([_verifyType isEqual:@"QOrder"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_QUICK_ORDER;
        }else if([_verifyType isEqual:@"PPP"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_PPP;
        }else if([_verifyType isEqual:@"Delivery"] ){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_DELIVERY;
        }else if([_verifyType isEqual:@"Credit"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_CREDIT;
        }
        NSArray* orderArry =  [TTOrderUtils getOrderArrayFromDirectoryType:orderDirectory];
        for (TTOrderDTO* order in orderArry ) {
            [TTOrderUtils deleteOrder:order inDirectory:orderDirectory];
        }
        if ([_verifyType isEqual:@"Inventory"]) {
            orderDirectory = ORDER_SAVE_DIRECTORY_INVENTORY;
        }else if([_verifyType isEqual:@"Order"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_ORDER;
        }else if([_verifyType isEqual:@"QOrder"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_QUICK_ORDER;
        }else if([_verifyType isEqual:@"PPP"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_PPP;
        }else if([_verifyType isEqual:@"Delivery"] ){
            orderDirectory = ORDER_SAVE_DIRECTORY_DELIVERY;
        }else if([_verifyType isEqual:@"Credit"]){
            orderDirectory = ORDER_SAVE_DIRECTORY_CREDIT;
        }
        
        [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:orderDirectory];
        [[[UIAlertView alloc] initWithTitle:@"INCOMPLETE NETWORK OPERATION" message:@"THIS ORDER WILL BE SAVED AND SENT LATER" delegate:nil
                          cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        
       // [self onVerifySelect:nil];
    }
}

- (IBAction)onOrderTypeVerifyConfirm:(id)sender {

    if ([_verifyType isEqual:@"Inventory"] ||
        [_verifyType isEqual:@"QOrder"]) {
        if(![_sundriesButton getSelected] && ![_dryGButton getSelected] && ![_coldGButton getSelected] && ![_frzButton getSelected] ){
            [[[UIAlertView alloc] initWithTitle:@"INCOMPLETE ORDER TYPE" message:@"PLEASE SELECT AND ORDER TYPE TO CONTINUE" delegate:nil
                              cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            return;
        }
        NSString* orderType;
        if([_verifyType isEqual:@"Inventory"])
            orderType = @"INVENTORY";
        if([_verifyType isEqual:@"QOrder"])
            orderType = @"QUICK ORDER";
        
        [[TTHoushiApi getInstance] showConfirmAlert:[[NSString alloc] initWithFormat:@"YOU ARE ABOUT TO GO SUBMIT AN %@. DO YOU WANT TO CONTINUE?",orderType]
                                      withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
            NSString*custName = [TTCurrentWorkingObjects currentOrder].customerName = [TTCurrentWorkingObjects currentOrder].customer.name;
    
            if ([_sundriesButton getSelected]) {
                custName = [custName stringByAppendingString:@" [SUN]"];
            }
    
            if ([_dryGButton getSelected]) {
                custName = [custName stringByAppendingString:@" [DRY]"];
            }
    
            if([_coldGButton getSelected]){
                custName = [custName stringByAppendingString:@" [CLD]"];
            }
                                          
            if ([_frzButton getSelected]) {
                custName = [custName stringByAppendingString:@" [FRZ]"];
            }
                                          
            if ([_rushButton getSelected]) {
                custName = [custName stringByAppendingString:@" [RSH]"];
            }
                                          
                 
            [TTCurrentWorkingObjects currentOrder].customerName = custName;
            if([_verifyType isEqual:@"Inventory"]){
                [TTHoushiApi saveOrder:ORDER_STATE_INVENTORY_COMPLETED];
            }else if([_verifyType isEqual:@"QOrder"]){
                [TTHoushiApi saveOrder:ORDER_STATE_ORDER_COMPLETED];
            }
        }];
    }
    
    if ([_verifyType isEqual:@"Credit"]) {
        NSString*custName = [TTCurrentWorkingObjects currentOrder].customerName;
        
        if ([_verifyType isEqual:@"Credit"]){
            custName = [custName stringByAppendingString:@" - CREDIT ORDER"];
        }
        
        [TTCurrentWorkingObjects currentOrder].customerName = custName;
    }
     
    
    if ([_verifyType isEqual:@"Order"] ) {
        if (areOrdersAllZero)
            [TTHoushiApi saveOrder:ORDER_STATE_INCOMPLETE];
        else
            [TTHoushiApi saveOrder:ORDER_STATE_ORDER_COMPLETED];
    }
    
    if ([_verifyType isEqual:@"PPP"]) {
        [_pppNumBoxesView setHidden:NO];
        [_pppNumBoxesField becomeFirstResponder];
    }
    
    
    if ([_verifyType isEqual:@"Delivery"] || [_verifyType isEqual:@"Credit"]) {
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"PurchaseOverview"] animated:YES];
    }
    
    
}

-(IBAction)onPPPSubmit:(id)sender
{
    if (!_pppNumBoxesField.text || [[_pppNumBoxesField.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
        return;
    }
    NSLog(@"%d",_pppNumBoxesField.text.intValue);
    [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO SUBMIT A PPP. DO YOU WANT TO CONTINUE?"
                                  withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                                      [TTHoushiApi savePullStatusWithNumberOfBoxes:_pppNumBoxesField.text.intValue];
                                  }];
}

-(IBAction)onPPPCancel:(id)sender
{
    [_pppNumBoxesView setHidden:YES];
}

- (IBAction)onOrderTypeVerifyCancel:(id)sender {
        [_orderTypeView setHidden:YES];

}
@end
