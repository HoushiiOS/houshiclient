//
//  TTDefaultEntryPageViewController.h
//  Houshi
//
//  Created by James Timberlake on 9/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTEntryPageViewController.h"

@interface TTOrderCheckStatus : NSObject
    @property (nonatomic) BOOL isChecked;
@end

@interface TTDefaultEntryPageViewController : TTEntryPageViewController
@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UIView *numberPadView;
@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak, nonatomic) IBOutlet UILabel *headerOccupancyRate;
@property (weak, nonatomic) IBOutlet UILabel *headerEventName;
@property (weak, nonatomic) IBOutlet UILabel *headerMiddleLabel;
@property (weak, nonatomic) IBOutlet UITextField *eventName;
- (IBAction)onOptionSelect:(id)sender;
-(void)setTableToScrollType:(BOOL)isSetToScroll;
@property (nonatomic,weak) IBOutlet UILabel* headerLabel;
@property (weak, nonatomic) IBOutlet UIView *eventView;
- (IBAction)onEventValuesChanged:(id)sender;
- (IBAction)onEventCanceled:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *purchaseOrderNumberField;
- (IBAction)onPONumCancel:(id)sender;
- (IBAction)onPONumSubmit:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *purchaseOrderNumView;
@property (weak, nonatomic) IBOutlet UILabel *poHeaderLabel;

-(IBAction)switchTypes:(id)sender;
-(void)refreshTableView:(id)sender;
+(void)setOrderHistoryType:(NSString*)historyType;
@end
