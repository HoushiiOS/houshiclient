//
//  TTAppConatants.h
//  Houshi
//
//  Created by James Timberlake on 7/24/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>


#define ORDER_TYPE_INVENTORY 1
#define ORDER_TYPE_ORDER 2
#define ORDER_TYPE_PPP 3
#define ORDER_TYPE_DELIVERY 4
#define ORDER_TYPE_QUICK_ORDER 5
#define ORDER_TYPE_CREDIT 6
#define ORDER_TYPE_PREVIEW_DELIVERY 7
#define ORDER_TYPE_RECENT_HISTORY 8



@interface TTAppConatants : NSObject
+(int)orderItemFontSize;
+(void)setOrderItemFontSize:(int)size;
+(int)orderItemRowHeight;
+(int)orderItemPagingSize;
+(BOOL)isScrollingTableView;
+(void)setScrollingTableView:(BOOL)isScrolling;
+(UITableViewScrollPosition)orderItemScrollType;
+(void)setOrderItemScrollType:(UITableViewScrollPosition)scrollType;
+(int)CURRENT_ORDER_TYPE;
+(void)setCURRENT_ORDER_TYPE:(int)orderType;
@end
