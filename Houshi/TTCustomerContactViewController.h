//
//  TTCustomerContactViewController.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTEntryPageViewController.h"

@interface TTCustomerContactViewController : TTEntryPageViewController
- (IBAction)onDeleteContactClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *contactView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIView *addContactView;
- (IBAction)onCancelClicked:(id)sender;
- (IBAction)switchToAddPage:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *addContactEmail;
@property (weak, nonatomic) IBOutlet UITextField *addContactPhone;
@property (weak, nonatomic) IBOutlet UITextField *addContactFName;
@property (weak, nonatomic) IBOutlet UITextField *addContactLName;
@property (weak, nonatomic) IBOutlet UITextField *addContactAddress1;
@property (weak, nonatomic) IBOutlet UITextField *addContactAddress2;
@property (weak, nonatomic) IBOutlet UITextField *addContactCity;
@property (weak, nonatomic) IBOutlet UITextField *addContactState;
@property (weak, nonatomic) IBOutlet UIButton *addContactPrimaryCheck;
@property (weak, nonatomic) IBOutlet UITextField *addContactZip;
- (IBAction)onSubmitAddContact:(id)sender;
- (IBAction)onCancelAddContact:(id)sender;
- (IBAction)onSubmitClicked:(id)sender;

@end
