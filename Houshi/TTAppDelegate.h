//
//  TTAppDelegate.h
//  Houshi
//
//  Created by JT on 5/8/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL calledResetCustomerScheduleAPISuccessfully;
@property (nonatomic) BOOL usePortait;
@end
