//
//  TTViewController.m
//  Houshi
//
//  Created by JT on 5/8/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTViewController.h"
#import "LNNumberpad.h"
#import "TTCurrentWorkingObjects.h"
#import "TTHoushiApi.h"
#import "TTUserListResponse.h"
#import "TTUserDTO.h"
#import "TTLoginResponse.h"
#import "TTAppConatants.h"
#import "TTOrderUtils.h"
#import "TTSaveOrderResponse.h"
#import "TTSavePullItemsResponse.h"
#import "TTSaveDeliveryResponse.h"
#import "TTCloseOrderResponse.h"
#import "TTValidOrderResponse.h"

@interface TTViewController () <UIPickerViewDelegate, UIPickerViewDataSource, TTHoughiApiDelegate, UITextFieldDelegate>
{
    NSMutableArray* users;
    int selectedRow;
    BOOL ifButtonPressed;
    TTOrderDTO* currentOrder;
}
@end

@implementation TTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [HelperMethods convertCapitalForView:self.view];
    
    [TTOrderUtils deleteOrder:nil inDirectory:nil];
    
    //login screen
    if (self.view.tag == 7) {
    LNNumberpad* num = [LNNumberpad defaultLNNumberpad];
   // [num setFrame:CGRectMake(num.frame.origin.x, num.frame.origin.y, _numberPadView.frame.size.width/3 , _numberPadView.frame.size.height)];
    [_numberPadView addSubview:num];
    [num setCurrentTextField:_Firsttextfield];
        _Firsttextfield.delegate = self;
    [TTHoushiApi getInstance].delegate = self;
    [TTHoushiApi showUserList];
    }
    
    //main home screen
    if (self.view.tag == 49) {
        [TTCurrentWorkingObjects setHomeViewController:self];
        [TTAppConatants setCURRENT_ORDER_TYPE :ORDER_TYPE_INVENTORY ];
        _MainMenuHeader.text =
        [[NSString alloc] initWithFormat:@"Welcome, %@", [TTHoushiApi getInstance].currentUser.username ];
        _MainMenuHeader.text = [NSString capitalizeString:_MainMenuHeader.text];
        [TTCurrentWorkingObjects setCurrentContact:nil];
        [TTCurrentWorkingObjects setCurrentOrder:nil];
        if( [self savedOrdersRemaining]){
            [[TTHoushiApi getInstance] showConfirmAlert:@"YOU HAVE ORDERS THAT NEED TO BE SENT TO THE SERVER, SYNC NOW?" withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                [self sendAllSavedOrders];
            }];
        }
    }
    
	// Do any additional setup after loading the view, typically from a nib.
}


-(BOOL)savedOrdersRemaining
{
    NSMutableArray* orderArray = [NSMutableArray arrayWithArray:[TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_INVENTORY]];
    [orderArray addObjectsFromArray:[TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_ORDER]];
    [orderArray addObjectsFromArray:[TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_QUICK_ORDER]];
    [orderArray addObjectsFromArray:[TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_PPP]];
    [orderArray addObjectsFromArray:[TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_CREDIT]];
    [orderArray addObjectsFromArray:[TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_DELIVERY]];
    
    return (orderArray.count > 0);
}

-(void)sendAllSavedOrders{
    NSArray* orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_INVENTORY];
    if (orderArray.count > 0) {
        currentOrder = (TTOrderDTO*)orderArray[0];
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
        [TTAppConatants setCURRENT_ORDER_TYPE:ORDER_TYPE_INVENTORY];
        [TTHoushiApi saveOrder:ORDER_STATE_INVENTORY_COMPLETED];
        return;
    }
 
    orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_ORDER];
    if (orderArray.count > 0) {
        currentOrder = (TTOrderDTO*)orderArray[0];
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
        [TTAppConatants setCURRENT_ORDER_TYPE:ORDER_TYPE_ORDER];
        [TTHoushiApi saveOrder:ORDER_STATE_ORDER_COMPLETED];
        return;
    }
    
    orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_QUICK_ORDER];
    if (orderArray.count > 0) {
        currentOrder = (TTOrderDTO*)orderArray[0];
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
        [TTAppConatants setCURRENT_ORDER_TYPE:ORDER_TYPE_QUICK_ORDER];
        [TTHoushiApi saveOrder:ORDER_STATE_ORDER_COMPLETED];
        return;
    }
    
    orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_PPP];
    if (orderArray.count > 0) {
        currentOrder = (TTOrderDTO*)orderArray[0];
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
        [TTAppConatants setCURRENT_ORDER_TYPE:ORDER_TYPE_PPP];
        [TTHoushiApi savePullStatusWithNumberOfBoxes:currentOrder.numBoxes.intValue];
        return;
    }
    
    orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_DELIVERY];
    if (orderArray.count > 0) {
        currentOrder = (TTOrderDTO*)orderArray[0];
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
        [TTCurrentWorkingObjects setCurrentContact:currentOrder.saved_DeliveryContact];
        [TTCurrentWorkingObjects setSignatureImage:[HelperMethods imageFromString:currentOrder.saved_DeliverySignature ]];
        [TTAppConatants setCURRENT_ORDER_TYPE:ORDER_TYPE_DELIVERY];
        [TTHoushiApi saveDeliveryStatustatusWithDelNumber:currentOrder.saved_Deliverynumber];
        return;
    }
    
    orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:ORDER_SAVE_DIRECTORY_CREDIT];
    if (orderArray.count > 0) {
        currentOrder = (TTOrderDTO*)orderArray[0];
        [TTCurrentWorkingObjects setCurrentOrder:currentOrder];
        [TTCurrentWorkingObjects setCurrentContact:currentOrder.saved_DeliveryContact];
        [TTCurrentWorkingObjects setSignatureImage:[HelperMethods imageFromString:currentOrder.saved_DeliverySignature ]];        [TTAppConatants setCURRENT_ORDER_TYPE:ORDER_TYPE_CREDIT];
        [TTAppConatants setCURRENT_ORDER_TYPE:ORDER_TYPE_CREDIT];
        [TTHoushiApi saveOrder:ORDER_STATE_DELIVERY_COMPLETED];
        return;
    }
    
    [[[UIAlertView alloc] initWithTitle:@"ALL SAVED ORDERS COMPLETED!" message:@"YOU HAVE COMPETED SENDING ALL ORDERS" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

-(NSArray*)orderInDirectory:(NSString*)directoryName{
    return [TTOrderUtils getOrderArrayFromDirectoryType:directoryName];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.view.tag == 49) {
        [TTAppConatants setCURRENT_ORDER_TYPE :ORDER_TYPE_INVENTORY ];
        [TTCurrentWorkingObjects setCurrentContact:nil];
        [TTCurrentWorkingObjects setCurrentOrder:nil];
        if( [self savedOrdersRemaining]){
            [[TTHoushiApi getInstance] showConfirmAlert:@"YOU HAVE ORDERS THAT NEED TO BE SENT TO THE SERVER, SYNC NOW?" withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                [self sendAllSavedOrders];
            }];
        }
    }
    [TTHoushiApi getInstance].delegate = self;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onOperationSuccess:(TTBaseResponse*)response
{
    
    //userlist response
    if ([response isKindOfClass:[TTUserListResponse class]]) {
        users = [NSMutableArray arrayWithArray:((TTUserListResponse*)response).user];
        [_namePicker reloadAllComponents];
    }else if ([response isKindOfClass:[TTLoginResponse class]])
    {
        [self performSegueWithIdentifier:@"MainMenuSegue" sender:self];

    }else if([response isKindOfClass:[TTSaveOrderResponse class]] || [response isKindOfClass:[TTCloseOrderResponse class]]){
        NSString* orderDirectory;
        if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_INVENTORY ) {
            orderDirectory = ORDER_SAVE_DIRECTORY_INVENTORY;
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_ORDER){
            orderDirectory = ORDER_SAVE_DIRECTORY_ORDER;
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER){
            orderDirectory = ORDER_SAVE_DIRECTORY_QUICK_ORDER;
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP){
            orderDirectory = ORDER_SAVE_DIRECTORY_PPP;
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY ){
            orderDirectory = ORDER_SAVE_DIRECTORY_DELIVERY;
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
            orderDirectory = ORDER_SAVE_DIRECTORY_CREDIT;
        }
        
        [TTOrderUtils deleteOrder:currentOrder inDirectory:orderDirectory];
        [self sendAllSavedOrders];
    }else if([response isKindOfClass:[TTSavePullItemsResponse class]] || [response isKindOfClass:[TTSaveDeliveryResponse class]]){
        [TTHoushiApi closeOrder];
    }else if([response isKindOfClass:[TTValidOrderResponse class]]){
        if (((TTValidOrderResponse*)response).isValid.boolValue) {
            [self onValidTempOrder];
        }else{
            [self onInvalidTempOrder];
        }
    }
    
}
-(void)onOperationError:(TTBaseResponse*)error{
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return users.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    TTUserDTO* user = (TTUserDTO*)users[row];
    user.username = [NSString capitalizeString:user.username];
    return user.username;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedRow = row;
}

-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* label = [[UILabel alloc] initWithFrame: pickerView.frame];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    TTUserDTO* user = (TTUserDTO*)users[row];
    user.username = [NSString capitalizeString:user.username];
    [label setText:user.username];
    return label;
}

-(void)viewDidDisappear:(BOOL)animated{
 _Firsttextfield.text = @"";
}


/******** Alert Confirmation Usage example ***********/

- (IBAction)onSignInClicked:(id)sender {
    
    ifButtonPressed = YES;
    [TTHoushiApi loginWithUsername:((TTUserDTO*)users[selectedRow]).username andPassword:_Firsttextfield.text];                                      
                                  
}

-(void)alertConfirmClicked{ /********** Required for confirm action event *********/


}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (!APPDELEGATE_OBJ.calledResetCustomerScheduleAPISuccessfully && buttonIndex == 1) {
        
        [TTHoushiApi resetCustomerSchedule];
    }
    else  if (buttonIndex == 1 && !ifButtonPressed ) {

        [TTHoushiApi showUserList];
        ifButtonPressed = NO;
    }
    else if (buttonIndex == 0){
        
       //Do nothing just exits
    }
    else if (ifButtonPressed){
        
        [self onSignInClicked:nil];
    }else{
        
        
    }
}

- (IBAction)onButtonClicked:(id)sender {
   [TTAppConatants setCURRENT_ORDER_TYPE :((UIButton*)sender).tag ];
    NSString* orderDirectory;
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_INVENTORY ) {
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_INVENTORY;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_ORDER){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_ORDER;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_QUICK_ORDER;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_PPP;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY ){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_DELIVERY;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_CREDIT;
    }
    
    if (orderDirectory == nil) {
        [self performSegueWithIdentifier:@"SearchPageSegue" sender:self];
        return;
    }
    
    NSArray* orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:orderDirectory];
    if (orderArray.count > 0) {
        [TTHoushiApi isSavedOrderValidWithOrderId:[NSNumber numberWithInt:((TTOrderDTO*)orderArray[0]).id.intValue] andOrderState:[TTAppConatants CURRENT_ORDER_TYPE]];
    }else{
        [self performSegueWithIdentifier:@"SearchPageSegue" sender:self];
    }
}

-(void)onValidTempOrder{
    NSString* orderDirectory;
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_INVENTORY ) {
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_INVENTORY;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_ORDER){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_ORDER;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_QUICK_ORDER;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_PPP;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY ){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_DELIVERY;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_CREDIT;
    }
    
    if (orderDirectory == nil) {
        [self performSegueWithIdentifier:@"SearchPageSegue" sender:self];
        return;
    }
    
    NSArray* orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:orderDirectory];
    if (orderArray.count > 0) {
        [[TTHoushiApi getInstance] showConfirmAlert:@"YOU HAVE AN ORDER THAT NEEDS TO BE FILLED. YOU WILL BE TAKEN TO THAT ORDER. YOU CANNOT CONTINUE WITHOUT COMPLETING IT." withNoMessage:@"WAIT" andYesText:@"OK" performingTheResutInBlock:^(){
            [TTCurrentWorkingObjects setCurrentOrder:(TTOrderDTO*)orderArray[0]];
            if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_INVENTORY ) {
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"UniqueEntry"] animated:YES];
            }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_ORDER){
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"OrderEntry"] animated:YES];
            }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER){
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
            }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP){
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
            }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY ){
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
            }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DefaultEntry"] animated:YES];
            }
        }];
    }
}

-(void)onInvalidTempOrder{
    NSString* orderDirectory;
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_INVENTORY ) {
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_INVENTORY;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_ORDER){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_ORDER;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_QUICK_ORDER;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_PPP;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY ){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_DELIVERY;
    }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
        orderDirectory = ORDER_SAVE_DIRECTORY_TEMP_CREDIT;
    }
    
    if (orderDirectory == nil) {
        [self performSegueWithIdentifier:@"SearchPageSegue" sender:self];
        return;
    }
    
    NSArray* orderArray = [TTOrderUtils getOrderArrayFromDirectoryType:orderDirectory];
    for (TTOrderDTO* order in orderArray) {
        [TTOrderUtils deleteOrder:order inDirectory:orderDirectory];
    }
    
    [self performSegueWithIdentifier:@"SearchPageSegue" sender:self];
}

- (IBAction)onLogOutClicked:(id)sender {
    [TTHoushiApi logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSUInteger) supportedInterfaceOrientations
{
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}


-(BOOL)shouldAutorotate{
    return  UIInterfaceOrientationIsLandscape([UIDevice currentDevice].orientation);
}
@end
