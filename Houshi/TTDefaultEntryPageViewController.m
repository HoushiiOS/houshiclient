//
//  TTDefaultEntryPageViewController.m
//  Houshi
//
//  Created by James Timberlake on 9/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTDefaultEntryPageViewController.h"
#import "TTInventoryTableViewCell.h"
#import "TTEntryPageTableViewCell.h"
#import "TTInvSingleSectionView.h"
#import "TTInvAllSectionView.h"
#import "TTCurrentWorkingObjects.h"
#import "TTOrderDTO.h"
#import "TTOrderItemDTO.h"
#import "LNNumberpad.h"
#import "DropDownListView.h"
#import "TTVerifyPageViewController.h"
#import "TTAppConatants.h"
#import "LHDropDownControlView.h"
#import "TTHoushiApi.h"
#import "CRToast.h"
#import "TTOrderItemSelectPopup.h"
#import "TTOrderUtils.h"

#define CHECK_ALL_STATUS_NONE 0
#define CHECK_ALL_STATUS_CHECKED 1
#define CHECK_ALL_STATUS_UNCHECKED 2

static NSString* ORDER_HISTORY_TYPE;
NSComparator sortQuantities3 = ^(id num1, id num2)
{
    float v1 = ((NSString*)num1).floatValue;
    float v2 = ((NSString*)num2).floatValue;
    
    if( v1 < v2) return NSOrderedAscending;
    else if (v1 > v2) return NSOrderedDescending;
    else return NSOrderedSame;
};

@implementation  TTOrderCheckStatus : NSObject 

@end

@interface TTDefaultEntryPageViewController ()<UITableViewDelegate, UITableViewDataSource, kDropDownListViewDelegate, UITextFieldDelegate, LNNumberPadDelegate, SingleHeaderDelegate, AllHeaderDelegate, LHDropDownControlViewDelegate, SingleHeaderDelegate, EntryCellDelegate, TTOrderItemSelectPopupDelegate>
{
    int invState;
    int currentRowIndex;
    int currentScrollingRowIndex;
    int currentTextFieldIndex;
    int currentPagingNum;
    int previousActiveRowIndex;
    BOOL changingRows;
    BOOL showLongDesc;
    BOOL deliveryChangedItemByPressingEnter;
    int checkAllSelectedStatus;
    NSMutableArray* cells;
    NSTimer* timer;
    UISwipeGestureRecognizer* gesture;
    UISwipeGestureRecognizer* gesture1;
    NSArray* quantityChangeReasonsDel;
}
@property (nonatomic, strong) TTOrderDTO* order;
@property (nonatomic, strong) UIView* ddView;
@property (nonatomic, strong) LNNumberpad* ln;
@property (nonatomic, strong) NSMutableArray* checkAllStatusArray;

@end

@implementation TTDefaultEntryPageViewController
{
LHDropDownControlView* dropDownView;
NSMutableArray *titles;
NSMutableArray *options;
}

+(void)setOrderHistoryType:(NSString*)historyType{
    ORDER_HISTORY_TYPE = historyType;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setTableToScrollType:(BOOL)isSetToScroll{
    //[TTAppConatants setScrollingTableView:isSetToScroll];
    //JT TOD REMOVE WHEN THIS IS FIXED
    //return;
    if(!isSetToScroll)
    {
        [_tableView setScrollEnabled:NO];
        gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onTableSwipedDown:)];
        gesture.direction = UISwipeGestureRecognizerDirectionUp;
        gesture1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onTableSwipedUp:)];
        gesture1.direction = UISwipeGestureRecognizerDirectionDown;
        [_tableView addGestureRecognizer:gesture];
        [_tableView addGestureRecognizer:gesture1];
    }else{
        [_tableView setScrollEnabled:YES];
        [_tableView removeGestureRecognizer:gesture];
        [_tableView removeGestureRecognizer:gesture1];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    cells = [NSMutableArray new];
    NSLog(@"Opened");
    _ln = [LNNumberpad defaultLNNumberpad];
    if (TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_RECENT_HISTORY && TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_PREVIEW_DELIVERY) {
        _ln.delegate = self;
        [_numberPadView addSubview:_ln];
    }else{
        _numberPadView.hidden = YES;
        _ln.hidden = YES;
    }
    
    checkAllSelectedStatus = CHECK_ALL_STATUS_NONE;
    
    /*[self demoOrderItems];*/
    invState = 0;
    currentPagingNum = 0;
    
    currentRowIndex = 0;
    currentScrollingRowIndex = currentRowIndex;
    
    //constant
    AUTO_SAVE_COUNT_MAX = 10;
    autoSaveCount = 0;
    
    _order = [TTCurrentWorkingObjects currentOrder];
    _headerLabel.text = _order.customer.name;
    _headerEventName.text = ( _order.eventName && ![_order.eventName isEqualToString:@""]) ? [[NSString alloc] initWithFormat:@"EVENT NAME: %@",_order.eventName] : @"";
    _headerOccupancyRate.text = _order.occupancyRate ? [[NSString alloc] initWithFormat:@"OCCUPANCY RATE: %@",_order.occupancyRate] : @"";
    
    
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY) {
        _checkAllStatusArray = [NSMutableArray new];
        for (TTOrderItemDTO* orderItem in _order.orderItem) {
            [_checkAllStatusArray addObject:[TTOrderCheckStatus new]];
        }
    }
    
    NSString* delBy = (_order.customer.deliveryMadeBy && ![_order.customer.deliveryMadeBy isEqualToString:@""]) ? [[NSString alloc] initWithFormat:@" DEL BY: %@",_order.customer.deliveryMadeBy ] : @"";
    
    if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP){
        showLongDesc = NO;
        NSString* packageType = _order.customer.isPacked.boolValue ? @"BINS" : @"CARDBOARD";
        
        if (!_order.customer.isPriced || ![_order.customer.isPriced isEqualToString:@"1"]) {
            _headerMiddleLabel.text = @"NOT PRICED";
            [_headerMiddleLabel setBackgroundColor:[UIColor blueColor]];
        }else{
           // _headerMiddleLabel.text = [[NSString alloc] initWithFormat:@"%@ - %@",packageType, delBy];
           // [_headerMiddleLabel setBackgroundColor:[UIColor blueColor]];
        }
        
        _headerEventName.text = packageType;
        _headerOccupancyRate.text = delBy;
    }else{
       showLongDesc = YES;
    }
    
    //open the notes sections when this page first loads
    if ([TTAppConatants CURRENT_ORDER_TYPE] != ORDER_TYPE_RECENT_HISTORY && [TTAppConatants CURRENT_ORDER_TYPE] != ORDER_TYPE_PREVIEW_DELIVERY)
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:nil];
    
    
    [self setTableToScrollType:YES];
    
    quantityChangeReasonsDel = @[
                              @"DAMAGED PRODUCT",
                              @"MISSING PRODUCT",
                              @"PRODUCT REFUSED"
                              ];
    
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP && _order.purchaseOrderNumber && ![_order.purchaseOrderNumber isEqualToString:@""]) {
        _poHeaderLabel.hidden = NO;
        _poHeaderLabel.text = [[NSString alloc] initWithFormat:@"PURCHASE ORDER NUMBER: %@", _order.purchaseOrderNumber ];
    }
    
    deliveryChangedItemByPressingEnter = YES;
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER) {
        [self onItemWithColumnType:@"product.nutshellBin" inAscendingOrder:YES];
    }else if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_PPP){
        [self onItemWithColumnType:@"product.warehouseBin" inAscendingOrder:NO];
    }else{
        [self onItemWithColumnType:@"product.warehouseBin" inAscendingOrder:YES];
    }
}

-(void)onTableSwipedDown:(id)sender
{
    //_currentRowIndex += [TTAppConatants orderItemPagingSize];
    currentScrollingRowIndex = [self findNearestRowPagingIndex:currentRowIndex goingUpOrDown:NO];
    if (currentScrollingRowIndex >= self.order.orderItem.count) {
        currentScrollingRowIndex = (self.order.orderItem.count -1);
    }
    [self scrollTableTo:currentScrollingRowIndex];
}

-(void)onTableSwipedUp:(id)sender
{
    // _currentRowIndex -= [TTAppConatants orderItemPagingSize];
    currentScrollingRowIndex = [self findNearestRowPagingIndex:currentRowIndex goingUpOrDown:YES];
    if (currentScrollingRowIndex < 0) {
        currentScrollingRowIndex = 0;
    }
    [self scrollTableTo:currentScrollingRowIndex];
}

-(int)findNearestRowPagingIndex:(int)currentIndex goingUpOrDown:(BOOL)isUp
{
    float multiple;
    if (!isUp) {
        multiple = roundf((currentIndex/[TTAppConatants orderItemPagingSize])) + 1;
    }else{
        multiple = roundf((currentIndex/[TTAppConatants orderItemPagingSize])) - 1;
    }
    return (int)(multiple*[TTAppConatants orderItemPagingSize]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onEventValuesChanged:(id)sender {
    [self.eventView setHidden:YES];
}

- (IBAction)onEventCanceled:(id)sender {
    [self.eventView setHidden:YES];
}

- (IBAction)onPONumCancel:(id)sender {
    _purchaseOrderNumView.hidden = YES;
}

- (IBAction)onPONumSubmit:(id)sender {
    _purchaseOrderNumView.hidden = YES;
    _order.purchaseOrderNumber = _purchaseOrderNumberField.text;
}

-(void)switchTypes:(id)sender
{
    invState ++;
    if(invState > 3)
        invState = 0;
    [cells removeAllObjects];
    [_tableView reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [TTAppConatants orderItemRowHeight];
}

-(void)updateOrderItem:(int)index inRowItemNum:(int)row  changingAmtTo:(NSString*)amount
{
    TTOrderItemDTO* orderItem = (TTOrderItemDTO*)_order.orderItem[index];
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP) {
        orderItem.productPull.qty = amount;
    }else  if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER) {
        orderItem.qtyOrdered = amount;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY) {
        orderItem.productDelivery.qtyDelivered = amount;
    }else if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT) {
        orderItem.qtyOrdered = [[NSString alloc] initWithFormat:@"-%@",amount];
        orderItem.productDelivery.qtyDelivered = [[NSString alloc] initWithFormat:@"-%@",amount];
    }
}

-(void)onTextFieldValueChanged:(UITextField*)textField
{
    //will make the values negative on the verify page
//    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT && ![textField.text isEqualToString:@""] && ![textField.text isEqualToString:@"0"]) {
//        textField.text = [[NSString alloc] initWithFormat:@"-%@",textField.text];
//    }
    [self updateOrderItem:currentRowIndex inRowItemNum:textField.tag changingAmtTo:textField.text];
}

-(void)onSpecialKeyPressed:(NSString *)keyName fromTextField:(UITextField*)textField
{
    
    if([keyName isEqualToString:@"done"])
    {
        TTOrderItemDTO* orderItem = (TTOrderItemDTO*)_order.orderItem[currentRowIndex];
        if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY &&  orderItem.productPull.qty.intValue > orderItem.productDelivery.qtyDelivered.intValue && orderItem.productDelivery.qtyDelivered != nil) {
            NSString* alert = [[NSString alloc] initWithFormat:@"THE ITEM %@ HAS BEEN CHANGED FROM %@ TO %@. DO YOU WANT TO CHANGE IT OR LEAVE THE ORIGINAL?",
                              orderItem.product.desc,
                              orderItem.productPull.qty,
                              orderItem.productDelivery.qtyDelivered
                            ];
            
            [[TTHoushiApi getInstance] showConfirmAlert:alert withNoMessage:[[NSString alloc] initWithFormat:@"KEEP %@",orderItem.productPull.qty] andYesText:[[NSString alloc] initWithFormat:@"CHANGE TO %@",orderItem.productDelivery.qtyDelivered] performingTheResutInBlock:^(){

                [[TTOrderItemSelectPopup alloc] initIntoView:self.view withItems:quantityChangeReasonsDel havingTheTitle:@"WHY DID THIS QUANTITY CHANGE" withDelegate:self];
            } andFailure:^(){
                orderItem.productDelivery.qtyDelivered = orderItem.productPull.qty;
                textField.text = orderItem.productPull.qty;
                deliveryChangedItemByPressingEnter = YES;
                [self processToNextItem];
            }];
            return;
        }else{
            deliveryChangedItemByPressingEnter = YES;
            [self processToNextItem];
        }
        

    }
}

-(void)processToNextItem{
    //select correct row
    //set correct
    currentRowIndex += 1;
    currentScrollingRowIndex = currentRowIndex;
    if (![TTAppConatants isScrollingTableView] && (currentRowIndex)% [TTAppConatants orderItemPagingSize] == 0 ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:(currentRowIndex) inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
        //            CGPoint point = _tableView.contentOffset;
        //            point .y -= _tableView.rowHeight;
        //            _tableView.contentOffset = point;
        if (timer) {
            [timer invalidate];
        }
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                 target:self
                                               selector:@selector(selectFirstTable:)
                                               userInfo:nil
                                                repeats:YES];
    }else{
        if ( currentRowIndex <_order.orderItem.count || _order.orderItem.count == 0) {
            if (timer) {
                [timer invalidate];
            }
            timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                     target:self
                                                   selector:@selector(selectFirstTable:)
                                                   userInfo:nil
                                                    repeats:YES];
        }
    }
    
    //[self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    if (currentRowIndex == (_order.orderItem.count -1)) {
        NSDictionary *options = @{
                                  kCRToastTextKey : @"YOU ARE AT THE LAST ORDER ITEM",
                                  kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                  kCRToastBackgroundColorKey : [UIColor blackColor],
                                  kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                                  kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
                                  kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                                  kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                                  };
        [CRToastManager showNotificationWithOptions:options
                                    completionBlock:^{
                                        
                                    }];
    }
    
    if (currentRowIndex >= self.order.orderItem.count) {
        [self onEndOfTable];
        currentRowIndex = 0;
        currentScrollingRowIndex = currentRowIndex;
        return;
    }
}

-(void)onEndOfTable
{
    
//    if ([[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(currentRowIndex-1) inSection:0]] isKindOfClass:[TTEntryPageTableViewCell class]]) {
//        invState++;
//        if (invState == 2) {
//            invState = 3;
//            currentTextFieldIndex = 2;
//        }
//        [_tableView reloadData];
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
//        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//        //[TTCurrentWorkingObjects setCurrentOrder:_order];
//        //[self performSegueWithIdentifier:@"VerifySegue" sender:self];
//    }else{
//        if (currentTextFieldIndex >= 2) {
    currentRowIndex = 0;
    currentScrollingRowIndex = currentRowIndex;
            [self goToVerifyScreen];
//        }else{
//            currentTextFieldIndex++;
//            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
//            [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//        }
//    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//TODO add view header field

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _order.orderItem.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell* cell;
   // if (invState == 0) {
        TTEntryPageTableViewCell* cell =
        (TTEntryPageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"EntryCell"];
        [cell setDataObject:_order.orderItem[indexPath.row]];
        [cell setRowIndex:(int)indexPath.row];
        if (!showLongDesc) {
            cell.headerField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).product.shortDesc;
        }
    
    
        if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY){
            
            [cell.highlightButton setHidden:NO];
            [cell.pastItemField setHidden:NO];
            cell.delegate = self;
           
            if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP)) {
                cell.pastItemField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered;
                BOOL highlighted = [((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered isEqualToString:((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty];
                [cell.highlightButton setSelected:highlighted];
                cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty;
            }
            
            if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY) || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY) {
                cell.pastItemField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty;
                 BOOL highlighted =  [((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty isEqualToString:((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productDelivery.qtyDelivered];
//                if (!highlighted) {
//                    [cell.highlightButton setSelected:highlighted];
//                    [cell.highlightButton performSelectorOnMainThread:@selector(toggleHighlight:) withObject:nil waitUntilDone:YES];                }
//                if (highlighted) {
//                    [cell.highlightButton setSelected:highlighted];
//                    //[cell.highlightButton setHighlightModeTo:highlighted];
//                    [cell.highlightButton performSelectorOnMainThread:@selector(toggleHighlight:) withObject:nil waitUntilDone:YES];
//                    [cell.highlightButton setBackgroundImage:highlighted ? cell.highlightButton.highlightedImage : cell.highlightButton.nonHighlightedImage  forState:UIControlStateHighlighted];
//                }
               
                [cell.highlightButton setSelected:highlighted];
                cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productDelivery.qtyDelivered;
                cell.highlighted = highlighted;
                
                if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY){
                    [cell.highlightButton setHidden:YES];
                    [cell.pastItemField setHidden:YES];
                    cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty;
                }
            }
            
            if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY ) {
                TTOrderItemDTO* item = (TTOrderItemDTO*)_order.orderItem[indexPath.row];
                cell.highlightButton.hidden = YES;
                cell.pastItemField.hidden = YES;
                if ([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_INVENTORY]) {
                    cell.pastItemField.hidden = NO;
                    cell.pastItemField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyBack;
                    cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyFront;
                }else if([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_ORDER] || [ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_QUICK_ORDER]){
                    cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered;
                }else if([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_PPP] ){
                    cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty;
                }else if([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_DELIVERY] || [ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_CREDIT]){
                    cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productDelivery.qtyDelivered;
                }
                
            }
            
            if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY){
                cell.pastItemField.enabled = NO;
                cell.quantityField.enabled = NO;
            }
            
        }else{
            if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER)) {
               // cell.skuField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).product.warehouseBin;
                cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered;
            }else if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT)) {
                cell.quantityField.text = ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productDelivery.qtyDelivered;
            }
        }
    //TODO
        if(checkAllSelectedStatus != CHECK_ALL_STATUS_NONE)
        {
            BOOL isHighlighted = (checkAllSelectedStatus == CHECK_ALL_STATUS_CHECKED);
            
            NSString* quantityField = (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP) ?
            ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).qtyOrdered : ((TTOrderItemDTO*)_order.orderItem[indexPath.row]).productPull.qty;
            
            if (checkAllSelectedStatus == CHECK_ALL_STATUS_UNCHECKED) {
                quantityField = @"";
            }
            
            cell.quantityField.text = quantityField;
            
            //[cell.highlightButton setHighlightModeTo:isHighlighted ];
            
            if (indexPath.row+1 == _order.orderItem.count) {
                checkAllSelectedStatus = CHECK_ALL_STATUS_NONE;

            }
           
        }

    
    return cell;
}

-(void)onEntryItemCheckedAtIndex:(int)rowIndex  withQuantity:(NSString*)qty isHighlighted:(BOOL)isHighlighted 
{
    if (isHighlighted) {
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP) {
            ((TTOrderItemDTO*)_order.orderItem[rowIndex]).productPull.qty =  qty;
        }
        if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT))
        {
            ((TTOrderItemDTO*)_order.orderItem[rowIndex]).productDelivery.qtyDelivered =  qty;
        }
    }else{
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP) {
            ((TTOrderItemDTO*)_order.orderItem[rowIndex]).productPull.qty =  nil;
        }
        if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY))
        {
            ((TTOrderItemDTO*)_order.orderItem[rowIndex]).productDelivery.qtyDelivered =  nil;
        }
    }
    [self checkAutoSaveStatus];
    
}

-(void)onSingleSectionAllCheckMarkChecked:(BOOL)isHighlighted
{
    checkAllSelectedStatus = isHighlighted ? CHECK_ALL_STATUS_CHECKED : CHECK_ALL_STATUS_UNCHECKED;
    for (int i = 0; i < _order.orderItem.count; i++) {
        TTOrderCheckStatus* statusObject = (TTOrderCheckStatus*)_checkAllStatusArray[i];
        TTOrderItemDTO* orderItem = (TTOrderItemDTO*)_order.orderItem[i];
        statusObject.isChecked = isHighlighted;
        if (statusObject.isChecked) {
            orderItem.productDelivery.qtyDelivered = orderItem.productPull.qty;
        }else{
            orderItem.productDelivery.qtyDelivered = @"";
        }
    }

    [_tableView reloadData];
    if (isHighlighted)
        [self goToVerifyScreen];
}

-(void)demoOrderItems{
    _order = [TTOrderDTO new];
    NSMutableArray* orderItems = [NSMutableArray new];
    //_order.orderItem =
    
    TTOrderItemDTO* item = [TTOrderItemDTO new];
    item.product = [TTProductDTO new];
    item.product.desc = @"Huggies 2 PCT";
    item.product.sku = @"6190031";
    item.qtyBack = @"20";
    item.qtyFront = @"40";
    item.qtyOrdered = @"0";
    
    [orderItems addObject:item];
    
    TTOrderItemDTO* item2;
    item2 = [TTOrderItemDTO new];
    item2.product = [TTProductDTO new];
    item2.product.desc = @"Johnson & Johnson Baby Lotion";
    item2.product.sku = @"6190032";
    item2.qtyBack = @"30";
    item2.qtyFront = @"10";
    item2.qtyOrdered = @"4";
    
    [orderItems addObject:item2];
    
    TTOrderItemDTO* item3;
    item3 = [TTOrderItemDTO new];
    item3.product = [TTProductDTO new];
    item3.product.desc = @"Frosted Flakes 24 CT";
    item3.product.sku = @"6190033";
    item3.qtyBack = @"10";
    item3.qtyFront = @"3";
    item3.qtyOrdered = @"1";
    
    
    [orderItems addObject:item3];
    
    for (int i = 0; i<50; i++) {
        TTOrderItemDTO* item2;
        item2 = [TTOrderItemDTO new];
        item2.product = [TTProductDTO new];
        item2.product.desc = @"Johnson & Johnson Baby Lotion";
        item2.product.sku = @"6190032";
        item2.qtyBack = @"30";
        item2.qtyFront = @"10";
        item2.qtyOrdered = @"4";
        [orderItems addObject:item2];
    }
    
    _order.orderItem = [orderItems copy];
    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        TTInvSingleSectionView * view = [[TTInvSingleSectionView alloc] init];
        
        if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP){
            view.itemType.text = @"PPP";
            [view.checkMark setHidden:NO];
            view.pastItemType.text = @"ORDER";
            [view.pastItemType setHidden:NO];
            [view.checkMark setHidden:YES];

            view.delegate = self;
        }
    
        if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY){
            view.itemType.text = @"DELIVERY";
            [view.checkMark setHidden:NO];
            view.pastItemType.text = @"PPP";
            [view.pastItemType setHidden:NO];
            view.delegate = self;
//           // if(checkAllSelectedStatus != CHECK_ALL_STATUS_NONE)
//            {
//                BOOL isHighlighted = (checkAllSelectedStatus == CHECK_ALL_STATUS_CHECKED);
//                [view.checkMark setHighlighted:YES];
//            }
            if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_RECENT_HISTORY) {
                view.checkMark.hidden = YES;
                view.pastItemType.hidden = YES;
                if ([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_INVENTORY]) {
                    view.pastItemType.hidden = NO;
                    view.itemType.text = @"FRONT";
                    view.pastItemType.text = @"BACK";
                }else if([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_ORDER] || [ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_QUICK_ORDER]){
                    view.itemType.text = @"ORDERED";
                }else if([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_PPP] ){
                    view.itemType.text = @"PULLED";
                }else if([ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_DELIVERY] || [ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_CREDIT]){
                    view.itemType.text = [ORDER_HISTORY_TYPE isEqualToString:ORDER_SAVE_DIRECTORY_HISTORY_DELIVERY] ?  @"DELIVERED" : @"CREDITED";
                }
                
            }
            
            if ( TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY) {
                [view.checkMark setHidden:YES];
                view.pastItemType.hidden = YES;
                view.itemType.text = @"PULLED";
            }

        }
    
    if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER){
            view.skuTitle.text = @"SLC SKU";
            view.itemType.text = @"ORDER";
    }
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT)
            view.itemType.text = @"CREDIT";
        
        view.delegate = self;
        [view setActiveColor];
        return view;
}
/**/

-(void)onItemWithColumnType:(NSString*)type inAscendingOrder:(BOOL)isAscending;
{
    if ([type isEqualToString:@"Quantity"])
        return;
    
    if ([type isEqualToString:@"product.warehouseBin"] && [TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER) {
        type = @"product.nutshellBin";
    }
    NSLog(@"Sort Array");
    NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:type ascending: isAscending];
    
    /*if ([type isEqualToString:@"Quantity"]) {
        
        if(invState == 0 || invState == 3)
            type = @"frontStock";
        
        if(invState == 1)
            type = @"backStock";
        
        if(invState == 2)
            type = @"qtyOrderSuggested";
        
        sortOrder = [[NSSortDescriptor alloc] initWithKey:type ascending:isAscending comparator:sortQuantities3];
    }
    
    if ([type isEqualToString:@"Quantity2"]) {
        type = @"backStock";
        sortOrder = [[NSSortDescriptor alloc] initWithKey:type ascending:isAscending comparator:sortQuantities3];
    }
    
    if ([type isEqualToString:@"Quantity3"]) {
        type = @"qtyOrderSuggested";
        sortOrder = [[NSSortDescriptor alloc] initWithKey:type ascending:isAscending comparator:sortQuantities3];
    }*/
    
    _order.orderItem = [_order.orderItem sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]];
    
    [_tableView reloadData];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[TTVerifyPageViewController class]]) {
        NSString* verifyType;
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP) {
            verifyType = @"PPP";
        }
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_QUICK_ORDER) {
            verifyType = @"QOrder";
        }
        if ((TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY))
        {
            verifyType = @"Delivery";
        }
        if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_CREDIT) {
            verifyType = @"Credit";
        }
        ((TTVerifyPageViewController*)segue.destinationViewController).verifyType = verifyType;
    }
}


-(void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    
    //VERIFY ORDER
    if( anIndex == 0){
        if(TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_RECENT_HISTORY && TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_PREVIEW_DELIVERY)
            [self goToVerifyScreen];
    }
    
    //SHORT DESCRIPTION
    if( anIndex == 1){
        showLongDesc = NO;
        [_tableView reloadData];
    }
    
    //LONG DESCRIPTION
    if( anIndex == 2){
        showLongDesc = YES;
        [_tableView reloadData];
    }

    //""
    if( anIndex == 3){

    }
    
    //SETTINGS VIEW
    if( anIndex == 4){
        if(TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_RECENT_HISTORY && TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_PREVIEW_DELIVERY)
            [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsView"] animated:YES completion:nil];
        //[self scrollTableTo:20];
    }
    
    //SWITCH SIDES
    if( anIndex == 5){
        //switch sides
        ///_tableView.constraints
        if (_tableView.frame.origin.x == 0) {
            [_tableView setFrame:CGRectOffset(_tableView.frame, _sideView.frame.size.width, 0)];
            [_sideView setFrame:CGRectOffset(_sideView.frame, -(self.view.frame.size.width - _sideView.frame.size.width), 0)];
        }else{
            [_tableView setFrame:CGRectOffset(_tableView.frame, -_sideView.frame.size.width, 0)];
            [_sideView setFrame:CGRectOffset(_sideView.frame, (self.view.frame.size.width - _sideView.frame.size.width), 0)];
        }
    }
    
    //NOTES VIEW
    if( anIndex == 6){
        //open notes
        if (TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_RECENT_HISTORY && TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_PREVIEW_DELIVERY)
            [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotesView"] animated:YES completion:nil];
    }
    
    //""
    if( anIndex == 7){

    }
    

    //MAIN MENU SCREEN
    if( (anIndex == 8 && [TTAppConatants CURRENT_ORDER_TYPE] != ORDER_TYPE_QUICK_ORDER)
       || (anIndex == 9 && [TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER)){
        [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO TO THE MAIN MENU SCREEN. DO YOU WANT TO CONTINUE?"
                                      withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                                          //main menu
                                          if (TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_RECENT_HISTORY && TTAppConatants.CURRENT_ORDER_TYPE != ORDER_TYPE_PREVIEW_DELIVERY)
                                              [TTHoushiApi closeOrder];
                                          [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
                                      }];
    }
    
    //@"PURCHASE ORDER NUMBER",
    if (anIndex == 8 && [TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_QUICK_ORDER) {
        _purchaseOrderNumView.hidden = NO;
    }
    
    
    //""
    if( anIndex == 9){
        
    }

    [_ddView removeFromSuperview];
    _ddView = nil;
}

-(void)paginateTableTo:(int)position
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:position inSection:0];
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)scrollTableTo:(int)position
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:position inSection:0];
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop  animated:YES];
}

- (IBAction)onOptionSelect:(id)sender {
    
    _ddView = [[UIView alloc] initWithFrame:self.view.frame];
    [_ddView setBackgroundColor:[UIColor grayColor]];
    //[_ddView setAlpha:0.7];
    //DropDownListView* dView = [[DropDownListView alloc] initWithTitle:@"MENU OPTIONS" options:@[@"SHOW SHORT DESC",@"SHOW LONG DESC", @"VERIFY", @"MAIN MENU",@"SWITCH SIDES", @"OPEN NOTES", @"SETTINGS",@"EXIT"] xy:CGPointMake(350, 200) size:CGSizeMake(300,535)
                                                          // isMultiple:NO];
    //[dView SetBackGroundDropDwon_R:42.0 G:43.0 B:44.0 alpha:0.70];
    [_ddView setAlpha:1.0];
    DropDownListView* dView;
    if([TTAppConatants CURRENT_ORDER_TYPE] != ORDER_TYPE_QUICK_ORDER){
        dView = [[DropDownListView alloc] initWithTitle:@"OPTIONS" options:@[@"VERIFY ORDER",
                                                                                           @"SHOW SHORT DESC",
                                                                                           @"SHOW LONG DESC",
                                                                                           @"",
                                                                                           @"SETTINGS",
                                                                                           @"SWITCH SIDES",
                                                                                           @"OPEN NOTES",
                                                                                           @"",
                                                                                           @"RETURN TO MAIN MENU",
                                                                                           @"EXIT OPTIONS"] xy:CGPointMake(350, 100) size:CGSizeMake(300,497)
                                                           isMultiple:NO];
    }else{
        dView = [[DropDownListView alloc] initWithTitle:@"OPTIONS" options:@[@"VERIFY ORDER",
                                                                                               @"SHOW SHORT DESC",
                                                                                               @"SHOW LONG DESC",
                                                                                               @"",
                                                                                               @"SETTINGS",
                                                                                               @"SWITCH SIDES",
                                                                                               @"OPEN NOTES",
                                                                                               @"",
                                                                                               @"ENTER PO NUMBER",
                                                                                               @"RETURN TO MAIN MENU",
                                                                                               @"EXIT OPTIONS"] xy:CGPointMake(350, 100) size:CGSizeMake(300,497)
                                                               isMultiple:NO];
    }
    [dView SetBackGroundDropDwon_R:42.0 G:43.0 B:44.0 alpha:1.0];
    [self.view addSubview:_ddView];
    dView.delegate = self;///42	43	44
    [_ddView addSubview:dView];/*****/
}

-(void)refreshTableView:(id)sender
{
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self refreshTableView:nil];
    
    [self setTableToScrollType:[TTAppConatants isScrollingTableView]];
    
}

-(BOOL)isReadyToPage
{
    int num = currentRowIndex % [TTAppConatants orderItemPagingSize];
    return ( (currentRowIndex) % [TTAppConatants orderItemPagingSize] == 0 && currentRowIndex != 0);
}



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [_ln setCurrentTextField:textField];
    
    currentTextFieldIndex = textField.tag;
    UIView* cellView = textField.superview;
    while (![cellView isKindOfClass:[TTEntryPageTableViewCell class]]) {
        cellView = cellView.superview;
        if (!cellView) {
            break;
        }
    }
    
    if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY) {
        if (deliveryChangedItemByPressingEnter == NO) {
          TTEntryPageTableViewCell* cell = (TTEntryPageTableViewCell*)[self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:previousActiveRowIndex inSection:0]];
            TTOrderItemDTO* orderItem = (TTOrderItemDTO*)_order.orderItem[previousActiveRowIndex];
            if (orderItem.productPull.qty.intValue > orderItem.productDelivery.qtyDelivered.intValue && orderItem.productDelivery.qtyDelivered != nil) {
                NSString* alert = [[NSString alloc] initWithFormat:@"THE ITEM %@ HAS BEEN CHANGED FROM %@ TO %@. DO YOU WANT TO CHANGE IT OR LEAVE THE ORIGINAL?",
                                   orderItem.product.desc,
                                   orderItem.productPull.qty,
                                   orderItem.productDelivery.qtyDelivered
                                   ];
                
                [[TTHoushiApi getInstance] showConfirmAlert:alert withNoMessage:[[NSString alloc] initWithFormat:@"KEEP %@",orderItem.productPull.qty] andYesText:[[NSString alloc] initWithFormat:@"CHANGE TO %@",orderItem.productDelivery.qtyDelivered] performingTheResutInBlock:^(){
                    
                    [[TTOrderItemSelectPopup alloc] initIntoView:self.view withItems:quantityChangeReasonsDel havingTheTitle:@"WHY DID THIS QUANTITY CHANGE" withDelegate:self];
                } andFailure:^(){
                    orderItem.productDelivery.qtyDelivered = orderItem.productPull.qty;
                    cell.quantityField.text = orderItem.productPull.qty;
                    deliveryChangedItemByPressingEnter = YES;
                    [self processToNextItem];
                }];
                return NO;
            }
            
            
        }else{
            deliveryChangedItemByPressingEnter = NO;
        }
    }
    
    // if([textField.superview.superview.superview isKindOfClass:[TTEntryPageTableViewCell class]])
    //{
        currentRowIndex = [((TTEntryPageTableViewCell*)cellView) getRowIndex];
        previousActiveRowIndex = currentRowIndex;
   // }else{
      //  currentRowIndex = [((TTInventoryTableViewCell*)textField.superview.superview.superview) getRowIndex];
    //}
    changingRows = YES;
    if (![TTAppConatants isScrollingTableView] && ![self isReadyToPage] /*currentPagingNum <= [TTAppConatants orderItemPagingSize]*/) {
        currentPagingNum++;
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:0] animated:YES scrollPosition:[TTAppConatants orderItemScrollType]];
        //[self scrollTableTo:currentRowIndex];
    }else if(![TTAppConatants isScrollingTableView] && [self isReadyToPage] /*currentPagingNum > [TTAppConatants orderItemPagingSize]*/){
        //currentPagingNum = 0;
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
        // [self scrollTableTo:currentRowIndex];
    }else{
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:0] animated:YES scrollPosition:[TTAppConatants orderItemScrollType]];
    }
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:0]];
    changingRows = NO;
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (changingRows) {
        if (invState != 3) {
            TTEntryPageTableViewCell* cell = (TTEntryPageTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell.quantityField setBackgroundColor:[UIColor yellowColor]];
        }else{
            TTInventoryTableViewCell* cell = (TTInventoryTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            if (currentTextFieldIndex == 0) {
                [cell.frontField setBackgroundColor:[UIColor yellowColor]];
            }else if (currentTextFieldIndex == 1){
                [cell.backField setBackgroundColor:[UIColor yellowColor]];
            }else{
                [cell.orderField setBackgroundColor:[UIColor yellowColor]];
            }
        }
        
        return;
    }
    if (invState != 3) {
        TTEntryPageTableViewCell* cell = (TTEntryPageTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [self textFieldShouldBeginEditing:cell.quantityField];
        //[cell.quantityField setBackgroundColor:[UIColor yellowColor]];
    }else{
        TTInventoryTableViewCell* cell = (TTInventoryTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        if (currentTextFieldIndex == 0) {
            [self textFieldShouldBeginEditing:cell.frontField];
            // [cell.frontField setBackgroundColor:[UIColor yellowColor]];
        }else if (currentTextFieldIndex == 1){
            [self textFieldShouldBeginEditing:cell.backField];
            // [cell.backField setBackgroundColor:[UIColor yellowColor]];
        }else{
            [self textFieldShouldBeginEditing:cell.orderField];
            //  [cell.orderField setBackgroundColor:[UIColor yellowColor]];
        }
    }
    [self checkAutoSaveStatus];
}

-(void)checkAutoSaveStatus{
    autoSaveCount++;
    if (autoSaveCount >= AUTO_SAVE_COUNT_MAX) {
        [self autoSaveTempOrder];
        autoSaveCount = 0;
    }
}

-(void)goToVerifyScreen{
    if (![self areAllQuantitiesFilled]) {
        [[[UIAlertView alloc] initWithTitle:@"NOT FILLED OUT" message:@"THIS ORDER IS NOT FILLED OUT ENTIRELY!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO TO THE VERIFY SCREEN. DO YOU WANT TO CONTINUE?"
                                  withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
                                      //@"Verify"
                                      [TTCurrentWorkingObjects setCurrentOrder:_order];
                                      [self performSegueWithIdentifier:@"VerifySegue" sender:self];
                                  }];
    
}

-(void)selectFirstTable:(id)sender{
    TTEntryPageTableViewCell* cell = (TTEntryPageTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:0]];
    if (cell != nil) {
        [timer invalidate];
        timer = nil;
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:0]];
    }else{
        TTEntryPageTableViewCell* cell2 = (TTEntryPageTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

        if (cell2) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:[TTAppConatants orderItemScrollType]];
        }
    }
}

-(void)sendNoteForDeliveryItem:(TTOrderItemDTO*)orderItem withMessage:(NSString*)message{
    TTOrderNoteDTO* orderNote = [TTOrderNoteDTO new];
    orderNote.createdBy = [TTHoushiApi getInstance].currentUser.username;
    orderNote.note = message;
    orderNote.orderId =[NSNumber numberWithInt: [TTCurrentWorkingObjects currentOrder].id.intValue];
    [TTHoushiApi saveNotes:@[orderNote]];
}

-(void)onItemSelected:(int)index;
{
    deliveryChangedItemByPressingEnter = YES;
    //create note and
    NSString* note = [[NSString alloc] initWithFormat:@"THE ITEM %@ HAS BEEN CHANGED FROM %@ TO %@ WITH REASON BEING %@",
        ((TTOrderItemDTO*)_order.orderItem[currentRowIndex]).product.desc,
        ((TTOrderItemDTO*)_order.orderItem[currentRowIndex]).productPull.qty,
        ((TTOrderItemDTO*)_order.orderItem[currentRowIndex]).productDelivery.qtyDelivered,
        (NSString*)quantityChangeReasonsDel[index] ];
    ((TTOrderItemDTO*)_order.orderItem[currentRowIndex]).productDelivery.changeReason = (NSString*)quantityChangeReasonsDel[index];
    [self sendNoteForDeliveryItem:(TTOrderItemDTO*)_order.orderItem[currentRowIndex] withMessage:note];
    [self processToNextItem];
}

@end
