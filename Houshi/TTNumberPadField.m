//
//  TTNumberPadField.m
//  Houshi
//
//  Created by JT on 5/15/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTNumberPadField.h"
#import "LNNumberpad.h"

@implementation TTNumberPadField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.inputView = [LNNumberpad defaultLNNumberpad];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
