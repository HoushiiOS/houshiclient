//
//  TTVerifyPageViewController.h
//  Houshi
//
//  Created by James Timberlake on 7/13/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTHiglightedStateButton.h"

@interface TTVerifyPageViewController : UIViewController<UIAlertViewDelegate>
@property (nonatomic, weak) IBOutlet UITableView* tableView;
- (IBAction)onVerifySelect:(id)sender;
- (IBAction)onBackSelect:(id)sender;
@property (nonatomic,strong) NSString* verifyType;
@property (nonatomic,weak) IBOutlet UILabel* headerLabel;
@property (weak, nonatomic) IBOutlet TTHiglightedStateButton *sundriesButton;
@property (weak, nonatomic) IBOutlet TTHiglightedStateButton *dryGButton;
@property (weak, nonatomic) IBOutlet TTHiglightedStateButton *coldGButton;
@property (weak, nonatomic) IBOutlet TTHiglightedStateButton *frzButton;
@property (weak, nonatomic) IBOutlet TTHiglightedStateButton *rushButton;
- (IBAction)onOrderTypeVerifyConfirm:(id)sender;
- (IBAction)onOrderTypeVerifyCancel:(id)sender;
-(IBAction)onPPPSubmit:(id)sender;
-(IBAction)onPPPCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *orderTypeView;
@property (weak, nonatomic) IBOutlet UIView *pppNumBoxesView;
@property (weak, nonatomic) IBOutlet UITextField *pppNumBoxesField;

@end
