//
//  TTEntryPageViewController.h
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTEntryPageViewController : UIViewController{
    int AUTO_SAVE_COUNT_MAX;
    int autoSaveCount;
}
@property (nonatomic, strong) IBOutlet UITableView* tableview;
-(void)autoSaveTempOrder;
-(void)saveOrderToSendLater;
-(BOOL)areAllQuantitiesFilled;
@end
