//
//  TTUrlRequest.h
//  Houshi
//
//  Created by JT on 5/16/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTBaseRequest.h"
#import "NSObject+SBJSON.h"
#import "JSON.h"

@interface TTUrlRequest : TTBaseRequest
@property (nonatomic, strong) NSString* method;
@property (nonatomic, strong) NSString* jsonrpc;
@property (nonatomic,strong) NSString* dataType;
@property (nonatomic,strong) id params;
@property (nonatomic, strong) NSString* id;
@end
