//
//  TTSignatureViewController.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTouchView.h"



@interface TTSignatureViewController : UIViewController{
        
    
	//the layer we draw to
    
	CALayer *canvasLayer;
    
    
    
	//the layer we use to display the cached image
    
	CALayer *backgroundLayer;
    
    
    
	//the image we cache to
    
	UIImage *cacheImage;
    
    
    
	//the path that represents the currently drawn line
    
	CGMutablePathRef path;
    
    
    
	//a flag to track if we're in the middle of a touch event
    
	BOOL touching;
    
    
    
	//a point to store the current location of a touch event
    
	CGPoint pathPoint;
}
@property (nonatomic, strong) IBOutlet TTTouchView* m_view;
- (IBAction)onCancel:(id)sender;

- (IBAction)onSubmit:(id)sender;
@end
