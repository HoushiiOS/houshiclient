//
//  TTAppDelegate.m
//  Houshi
//
//  Created by JT on 5/8/14.
//  Copyright (c) 2014 JT. All rights reserved.
//


#import "TTAppDelegate.h"
#import "TTHoushiApi.h"
#import "TTAppConatants.h"
#import "TTOrderNoteDTO.h"
#import "TTContactDTO.h"
#import <Crashlytics/Crashlytics.h>

@implementation TTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Crashlytics startWithAPIKey:@"6086dad21e50c00693d693468d0e75f302b7a3b1"];
    // Override point for customization after application launch.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder

    //let AFNetworking manage the activity indicator
    //[AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    // Initialize HTTPClient
//    NSURL *baseURL = [NSURL URLWithString:@"http://invapi.starlightcreations.com/api/json"];
//    AFHTTPClient* client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
//    
//    //we want to work with JSON-Data
//    [client setDefaultHeader:@"Content-Type" value:@"application/json"];
//    
//    // Initialize RestKit
//    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
//    
//    RKObjectMapping *sessionMapping = [RKObjectMapping mappingForClass:[TTBaseResponse class]];
//    [sessionMapping addAttributeMappingsFromDictionary:
//     @{@"result": @"result"}];
//    
//    RKResponseDescriptor* desc = [RKResponseDescriptor responseDescriptorWithMapping:sessionMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:nil];
    
    

        // You can access the model object used to construct the `NSError` via the `userInfo`
   // [TTHoushiApi loginWithUsername:@"admin" andPassword:@"1234"];
    //[TTHoushiApi showUserList];
   // [TTHoushiApi showInventoryList];
   //
   // [TTHoushiApi createOrderWithCustomerId:6 isQuickOrder:false];
    
   // [TTHoushiApi resetCustomerSchedule];
    [TTAppConatants setOrderItemFontSize:18];
    [TTAppConatants setScrollingTableView:YES];
    //[TTHoushiApi getCustomerContactsWithCustomerId:6];
    

//    TTOrderNoteDTO* note = [TTOrderNoteDTO new];
//    id object = [note toDictionary];
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    if (_usePortait) {
        return UIInterfaceOrientationMaskPortrait|
        UIInterfaceOrientationMaskLandscape
        ;
    }
    return 
            UIInterfaceOrientationMaskLandscape
    ;
}



@end
