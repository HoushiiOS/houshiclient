//
//  TTCustomerContactViewController.m
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTCustomerContactViewController.h"
#import "TTHoushiApi.h"
#import "TTCurrentWorkingObjects.h"
#import "TTGetCustomerContactResponse.h"
#import "TTSaveCustomerContactResponse.h"
#import "TTDeleteCustomerContactResponse.h"
#import "TTContactDTO.h"
#import "TTContactTableViewCell.h"

@interface TTCustomerContactViewController () <TTHoughiApiDelegate, UITableViewDataSource, UITableViewDelegate>
{
    int deleteIndex;
}
@property (nonatomic, strong) NSArray* contacts;
@property (nonatomic, strong) TTContactDTO* currentContact;
@end

@implementation TTCustomerContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [TTHoushiApi setDelegate:self];
    [TTHoushiApi getCustomerContactsWithCustomerId:[TTCurrentWorkingObjects currentOrder].customerId.intValue];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onDeleteContactClicked:(id)sender {
    
    deleteIndex = ((UIButton*)sender).tag;
    NSString* message = [[NSString alloc] initWithFormat:@"YOU ARE ABOUT TO DELETE THE CONTACT WITH THE EMAIL %@. ARE YOU SURE YOU WANT TO DO THIS?",((TTContactDTO*)_contacts[deleteIndex]).email ];
    

    [[TTHoushiApi getInstance] showConfirmAlert:message withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
        [TTHoushiApi deleteContactWithId:((TTContactDTO*)_contacts[deleteIndex]).id.intValue andCustomerId:[TTCurrentWorkingObjects currentOrder].customerId.intValue];
    }];
}

-(void)onOperationSuccess:(TTBaseResponse *)response
{
    if ([response isKindOfClass:[TTGetCustomerContactResponse class]]) {
        _contacts = ((TTGetCustomerContactResponse*)response).contacts;
        [self.tableview reloadData];
    }

    if ([response isKindOfClass:[TTSaveCustomerContactResponse class]]) {
        [self onCancelAddContact:nil];
        [TTHoushiApi getCustomerContactsWithCustomerId:[TTCurrentWorkingObjects currentOrder].customerId.intValue];
    }
    
    if ([response isKindOfClass:[TTDeleteCustomerContactResponse class]]) {
        [self onCancelAddContact:nil];
        [TTHoushiApi getCustomerContactsWithCustomerId:[TTCurrentWorkingObjects currentOrder].customerId.intValue];
    }
}

-(void)onOperationError:(TTBaseResponse *)error{
    //do nothing
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _contacts.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTContactTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    cell.emailLabel.text = ((TTContactDTO*)_contacts[indexPath.row]).email;
    cell.firstNameLabel.text = ((TTContactDTO*)_contacts[indexPath.row]).firstName;
    cell.lastNameLabel.text = ((TTContactDTO*)_contacts[indexPath.row]).lastName;
    cell.deleteButton.tag = indexPath.row;
    [cell.deleteButton setTransform:CGAffineTransformMakeRotation(62)];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _currentContact = (TTContactDTO*)_contacts[indexPath.row];
}


- (IBAction)onCancelClicked:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)switchToAddPage:(id)sender {
    [_contactView setHidden:YES];
    [_addContactView setHidden:NO];
}
- (IBAction)onSubmitAddContact:(id)sender {
    if ( [_addContactFName.text isEqualToString:@""] || [_addContactLName.text isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"INCOMPLETE INFORMATION" message:@"YOU MUST ENTER A VALID FIRST, AND LAST NAME TO CONTINUE." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    TTContactDTO* contact = [TTContactDTO new];
    contact.firstName = _addContactFName.text;
    contact.lastName = _addContactLName.text;
    contact.email = _addContactEmail.text;
    contact.mainPhone = _addContactPhone.text;
    contact.mainPhoneCountryCode = @"1";
    contact.address1 = _addContactAddress1.text;
    contact.address2 = _addContactAddress2.text;
    contact.city = _addContactCity.text;
    contact.state = _addContactState.text;
    contact.zip = _addContactZip.text;
    
    [TTHoushiApi saveContactToCustomerWithId:[TTCurrentWorkingObjects currentOrder].customerId.intValue AndContact:contact AndPrimary:_addContactPrimaryCheck.highlighted];
    _currentContact = nil;
}

- (IBAction)onCancelAddContact:(id)sender {
    [_contactView setHidden:NO];
    [_addContactView setHidden:YES];
}

- (IBAction)onSubmitClicked:(id)sender {
    if (!_currentContact) {
        [[[UIAlertView alloc] initWithTitle:@"NO CONTACT SELECTED" message:@"YOU MUST ENTER SELECT A CONTACT TO CONTINUE." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    [TTCurrentWorkingObjects setCurrentContact:_currentContact];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
