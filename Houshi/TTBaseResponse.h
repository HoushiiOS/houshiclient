//
//  TTBaseResponse.h
//  Houshi
//
//  Created by JT on 5/17/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTBaseResponse : TTBaseRequest
@property (nonatomic, strong) NSDictionary* result;
@end
