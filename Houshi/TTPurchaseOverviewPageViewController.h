//
//  TTPurchaseOverviewPageViewController.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTEntryPageViewController.h"
#import <UIKit/UIKit.h>
#import "TTTouchView.h"

@interface TTPurchaseOverviewPageViewController : TTEntryPageViewController{
    //the layer we draw to
    
    CALayer *canvasLayer;
    
    
    
    //the layer we use to display the cached image
    
    CALayer *backgroundLayer;
    
    
    
    //the image we cache to
    
    UIImage *cacheImage;
    
    
    
    //the path that represents the currently drawn line
    
    CGMutablePathRef path;
    
    
    
    //a flag to track if we're in the middle of a touch event
    
    BOOL touching;
    
    
    
    //a point to store the current location of a touch event
    
    CGPoint pathPoint;
}
@property (weak, nonatomic) IBOutlet UIButton *contactEmailField;
@property (weak, nonatomic) IBOutlet UILabel *contactFIrstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactLastNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *deliveryNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalItemsLabel;
- (IBAction)onChangeContact:(id)sender;

- (IBAction)onChangeSignature:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *signatureImageView;
- (IBAction)onSubmitClicked:(id)sender;
- (IBAction)onCancelClicked:(id)sender;
@property (nonatomic, strong) IBOutlet TTTouchView* m_view;
@end
