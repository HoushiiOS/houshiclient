//
//  TTAppConatants.m
//  Houshi
//
//  Created by James Timberlake on 7/24/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTAppConatants.h"

static int ORDER_ITEM_FONT_SIZE;
static int ORDER_ITEM_ROW_HEIGHT;
static int ORDER_ITEM_PAGE_BY_NUM;
static int CURRENT_ORDER_TYPE;
static BOOL PAGING_ORDERS;
static UITableViewScrollPosition ORDER_ITEM_SCROLL_TYPE;
@implementation TTAppConatants
+(int)CURRENT_ORDER_TYPE
{
    return CURRENT_ORDER_TYPE;
}

+(void)setCURRENT_ORDER_TYPE:(int)orderType;
{
    CURRENT_ORDER_TYPE = orderType;
}
+(int)orderItemFontSize
{
    return ORDER_ITEM_FONT_SIZE;
}
+(void)setOrderItemFontSize:(int)size
{
    ORDER_ITEM_FONT_SIZE = size;
    [TTAppConatants setOrderItemSettingSizes];
}
+(int)orderItemRowHeight
{
    return ORDER_ITEM_ROW_HEIGHT;
}
+(int)orderItemPagingSize
{
    return ORDER_ITEM_PAGE_BY_NUM;
}
+(BOOL)isScrollingTableView{
    return PAGING_ORDERS;
}
+(void)setScrollingTableView:(BOOL)isScrolling{
    PAGING_ORDERS = isScrolling;
    if (PAGING_ORDERS) {
        [TTAppConatants setOrderItemScrollType:UITableViewScrollPositionMiddle];
    }else{
        [TTAppConatants setOrderItemScrollType:UITableViewScrollPositionNone];
    }
}
+(UITableViewScrollPosition)orderItemScrollType
{
    return ORDER_ITEM_SCROLL_TYPE;
}
+(void)setOrderItemScrollType:(UITableViewScrollPosition)scrollType
{
    ORDER_ITEM_SCROLL_TYPE = scrollType;
}
+(void)setOrderItemSettingSizes
{
    if (ORDER_ITEM_FONT_SIZE > 20) {
        ORDER_ITEM_ROW_HEIGHT = 60;
        ORDER_ITEM_PAGE_BY_NUM = 7;
    }else{
        ORDER_ITEM_ROW_HEIGHT = 45;
        ORDER_ITEM_PAGE_BY_NUM = 11;
    }
}
@end
