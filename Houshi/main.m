
//
//  main.m
//  Houshi
//
//  Created by JT on 5/8/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TTAppDelegate class]));
    }
}
