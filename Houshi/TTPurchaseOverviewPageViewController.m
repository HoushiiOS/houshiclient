//
//  TTPurchaseOverviewPageViewController.m
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTPurchaseOverviewPageViewController.h"
#import "TTCurrentWorkingObjects.h"
#import "TTOrderItemDTO.h"
#import "TTHoushiApi.h"
#import "TTSaveDeliveryResponse.h"
#import "HelperMethods.h"
#import "TTOrderUtils.h"
#import "TTSaveOrderNetworkErrorResponse.h"
#import "TTAppConatants.h"
#import "HelperMethods.h"

@interface TTPurchaseOverviewPageViewController () <TTHoughiApiDelegate,TouchViewDelegate>

@end

@implementation TTPurchaseOverviewPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    int totalDelivered = 0;
    for (TTOrderItemDTO* orderItem in [TTCurrentWorkingObjects currentOrder].orderItem) {
        totalDelivered += orderItem.productDelivery.qtyDelivered.intValue;
    }
    _totalItemsLabel.text = [[NSString alloc] initWithFormat:@"%i TOTAL UNITS",totalDelivered];
    
    CGRect screenBounds = _m_view.bounds;
    //set up drawing layer
    
    canvasLayer = [CALayer layer];
    
    canvasLayer.bounds = screenBounds;
    
    canvasLayer.position = CGPointMake(screenBounds.size.width/2, screenBounds.size.height/2);
    
    [canvasLayer setDelegate:self];
    
    
    
    //set up storage layer
    
    backgroundLayer = [CALayer layer];
    
    backgroundLayer.bounds = screenBounds;
    
    backgroundLayer.position = CGPointMake(screenBounds.size.width/2, screenBounds.size.height/2);
    
    [backgroundLayer setDelegate:self];
    
    
    
    //set up view and add layers
    
    //m_view = [[TouchView alloc] initWithFrame:screenBounds];
    
    _m_view.backgroundColor = [UIColor whiteColor];
    
    [_m_view.layer addSublayer:canvasLayer];
    
    [_m_view.layer addSublayer:backgroundLayer];
    
    
    [_m_view setDelegate:self];
    
    
    //initialize some other variables
    
    cacheImage = nil;
    
    touching = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//TODO add contact and signature items
-(void)viewWillAppear:(BOOL)animated
{
    [TTHoushiApi setDelegate:self];
    [_signatureImageView setImage:[TTCurrentWorkingObjects signatureImage]];
    if ([TTCurrentWorkingObjects currentContact]) {
        [_contactEmailField setTitle:[TTCurrentWorkingObjects currentContact].email forState:UIControlStateNormal];
        _contactFIrstNameLabel.text = [TTCurrentWorkingObjects currentContact].firstName;
        _contactLastNameLabel.text = [TTCurrentWorkingObjects currentContact].lastName;
        _contactFIrstNameLabel.hidden = NO;
        _contactLastNameLabel.hidden = NO;
    }else{
        [_contactEmailField setTitle:@"CLICK TO ADD/SELECT A CONTACT" forState:UIControlStateNormal];
        _contactFIrstNameLabel.text = @"FIRST NAME";
        _contactLastNameLabel.text = @"LAST NAME";
        _contactFIrstNameLabel.hidden = YES;
        _contactLastNameLabel.hidden = YES;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onChangeContact:(id)sender {
    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ContactPage"] animated:YES completion:nil];
    
}

- (IBAction)onChangeSignature:(id)sender {
    
        //[self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SignaturePage"] animated:YES completion:nil];
    [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE GOING TO CLEAR THE SIGNATURE. ARE OU SURE YOU WANT TO DO THIS?" withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
        [TTCurrentWorkingObjects setSignatureImage:nil];
        cacheImage = nil;
        touching = YES;
        [backgroundLayer setNeedsDisplay];
        [canvasLayer setNeedsDisplay];
        touching = NO;
    }];
}

- (IBAction)onSubmitClicked:(id)sender {
    [TTCurrentWorkingObjects setSignatureImage:[cacheImage copy]];
    
    if ((([TTCurrentWorkingObjects currentOrder].customer.sigRequired.intValue == 1) && ![TTCurrentWorkingObjects signatureImage]) ||
        ![TTCurrentWorkingObjects currentContact]) {
        [[[UIAlertView alloc] initWithTitle:@"INCOMPLETE SIGNATURE INFO" message:@"PLEASE ADD A CONTACT AND A SIGNATURE TO CONTINUE" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    if (([TTCurrentWorkingObjects currentOrder].customer.sigRequired.intValue != 1) &&
         ![TTCurrentWorkingObjects currentContact]) {
            [[[UIAlertView alloc] initWithTitle:@"INCOMPLETE CONFIRMATION PAGE INFO" message:@"PLEASE ADD A CONTACT TO CONTINUE" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            return;
        }
    
    [[TTHoushiApi getInstance] showConfirmAlert:@"YOU ARE ABOUT TO GO SUBMIT A DELIVERY. DO YOU WANT TO CONTINUE?"
                                  withNoMessage:@"NO" andYesText:@"YES" performingTheResutInBlock:^(){
            
            [TTHoushiApi saveDeliveryStatustatusWithDelNumber:_deliveryNumberTextField.text];
    }];
}

- (IBAction)onCancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onOperationSuccess:(TTBaseResponse *)response
{
    if([response isKindOfClass:[TTSaveDeliveryResponse class]])
    {
        if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY) {
            [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_DELIVERY];
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
            [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:ORDER_SAVE_DIRECTORY_HISTORY_CREDIT];
        }
        
        [TTHoushiApi closeOrder];
        if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY) {
            [TTCurrentWorkingObjects currentOrder].workingUserId = nil;
            [TTOrderUtils deleteOrder:[TTCurrentWorkingObjects currentOrder] inDirectory:ORDER_SAVE_DIRECTORY_TEMP_DELIVERY];
            [TTHoushiApi createInvoice];
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
            [TTCurrentWorkingObjects currentOrder].workingUserId = nil;
            [TTOrderUtils deleteOrder:[TTCurrentWorkingObjects currentOrder] inDirectory:ORDER_SAVE_DIRECTORY_TEMP_CREDIT];
            [TTHoushiApi createCredit];
        }
        [canvasLayer setDelegate:nil];
        [backgroundLayer setDelegate:nil];
        [_m_view setDelegate:nil];
        [[[UIAlertView alloc] initWithTitle:@"OPERATION COMPLETE" message:@"YOUR ORDER HAS BEEN SUCCESSFULLY DELIVERED" delegate:nil cancelButtonTitle:@"GREAT" otherButtonTitles: nil] show];
        [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
    }
}

-(void)onOperationError:(TTBaseResponse *)error
{
    if([error isKindOfClass:[TTSaveOrderNetworkErrorResponse class]])
    {
        [TTCurrentWorkingObjects currentOrder].saved_Deliverynumber = _deliveryNumberTextField.text;
        [TTCurrentWorkingObjects currentOrder].saved_DeliveryContact = [TTCurrentWorkingObjects currentContact];
        [TTCurrentWorkingObjects currentOrder].saved_DeliverySignature = [HelperMethods convertImageToStringData:[TTCurrentWorkingObjects signatureImage]];
        if ([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_DELIVERY ) {
            [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:ORDER_SAVE_DIRECTORY_DELIVERY];
        }else if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT){
            [TTOrderUtils saveOrder:[TTCurrentWorkingObjects currentOrder] toDirectoryType:ORDER_SAVE_DIRECTORY_CREDIT];
        }
        [self.navigationController popToViewController:[TTCurrentWorkingObjects homeViewController] animated:YES];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (touching) return;
    
    
    
    //start a new path
    
    path = CGPathCreateMutable();
    
    
    
    //set the path's starting point
    
    UITouch *touch = (UITouch *)[touches anyObject];
    
    CGPathMoveToPoint(path, NULL, [touch locationInView:_m_view].x, [touch locationInView:_m_view].y);
    
    
    
    touching = YES;
    
    
    
}



-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (touching)
        
    {
        
        //get the current location of the touch event
        
        UITouch *theTouch = (UITouch *)[touches anyObject];
        
        pathPoint = [theTouch locationInView:_m_view];
        
        
        
        CGPathAddLineToPoint(path, NULL, pathPoint.x, pathPoint.y);
        
        
        
        [canvasLayer setNeedsDisplay];
        
    }
    
}



-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (!touching) return;
    
    
    
    //create a new image context
    
    UIGraphicsBeginImageContext(CGSizeMake(backgroundLayer.bounds.size.width, backgroundLayer.bounds.size.height));
    
    
    
    //grab a reference to the new image context
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    
    
    //push the image context to the top of the drawing stack
    
    UIGraphicsPushContext(ctx);
    
    
    
    //set the blend mode to prevent white pixels from
    
    //covering up the lines that have already been drawn
    
    CGContextSetBlendMode(ctx, kCGBlendModeDarken);
    
    
    
    if (cacheImage != nil) {
        
        //draw the cached state of the image to the image context and release it
        
        [cacheImage drawInRect:CGRectMake(0,0,backgroundLayer.bounds.size.width,backgroundLayer.bounds.size.height)];
        cacheImage = nil;
    }
    
    
    
    //blend the drawing layer into the image context
    
    [canvasLayer drawInContext:ctx];
    
    
    
    //we're done drawing to the image context
    
    UIGraphicsPopContext();
    
    
    
    //store the image context so we can add to it again later
    
    cacheImage = UIGraphicsGetImageFromCurrentImageContext();
    
    
    
    
    //we're finished with the image context altogether
    
    UIGraphicsEndImageContext();
    
    
    
    touching = NO;
    
    
    
    //release the path
    
    CGPathRelease(path);
    
    
    
    //update the background layer (we'll need to draw the cached image to the background)
    
    [backgroundLayer setNeedsDisplay];
    
}



- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    
    //this method is handling multiple layers, so first
    
    //determine which layer we're drawing to
    
    if (layer == canvasLayer) {
        
        //we don't want this to fire after the background layer update
        
        //and after the path has been released
        
        if (!touching) return;
        
        
        
        //add the path to the context
        
        CGContextAddPath(ctx, path);
        
        
        
        //set a line width and draw the path
        
        CGContextSetLineWidth(ctx, 2.0f);
        
        CGContextStrokePath(ctx);
        
    }
    
    else if (layer == backgroundLayer) {
        
        //remember the current state of the context
        
        CGContextSaveGState(ctx);
        
        
        
        //the cached image coordinate system is upside down, so do a backflip
        
        CGContextTranslateCTM(ctx, 0, backgroundLayer.bounds.size.height);
        
        CGContextScaleCTM(ctx, 1.0, -1.0);
        
        
        
        //draw the image
        
        CGImageRef ref = cacheImage.CGImage;
        
        CGContextDrawImage(ctx, backgroundLayer.bounds, ref);
        
        
        
        //restore the context to its pre-flipped state
        
        CGContextRestoreGState(ctx);
        
    }
    
}

//- (void)dealloc {
//    
//    //canvasLayer = nil;
//    
//    //backgroundLayer = nil;
//}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)onCancel:(id)sender {
  //  [canvasLayer removeFromSuperlayer];
  //  [backgroundLayer removeFromSuperlayer];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSubmit:(id)sender {
    //save cachedImage
   // [canvasLayer removeFromSuperlayer];
   // [backgroundLayer removeFromSuperlayer];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
