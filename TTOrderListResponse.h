//
//  TTOrderListResponse.h
//  Houshi
//
//  Created by James Timberlake on 9/6/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTOrderListResponse : TTBaseResponse
@property (nonatomic, strong) NSArray* order;
@end
