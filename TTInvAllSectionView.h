//
//  TTInvSingleHeaderView.h
//  Houshi
//
//  Created by James Timberlake on 6/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AllHeaderDelegate <NSObject>

-(void)onItemWithColumnType:(NSString*)type inAscendingOrder:(BOOL)isAscending;

@end

@interface TTInvAllSectionView : UIView
@property (weak, nonatomic) IBOutlet UILabel *descTitle;
@property (weak, nonatomic) IBOutlet UILabel *skuTitle;
@property (nonatomic,weak) IBOutlet UILabel* itemType1;
@property (nonatomic,weak) IBOutlet UILabel* itemType2;
@property (nonatomic,weak) IBOutlet UILabel* itemType3;
@property (nonatomic, weak) id<AllHeaderDelegate> delegate;
-(void)setActiveColor;
@end
