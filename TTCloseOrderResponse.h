//
//  TTCloseOrderRequest.h
//  Houshi
//
//  Created by James Timberlake on 11/22/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTCloseOrderResponse : TTBaseResponse
@property NSNumber* success;
@end
