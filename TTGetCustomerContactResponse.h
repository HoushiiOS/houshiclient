//
//  TTGetCustomeResponse.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTGetCustomerContactResponse : TTBaseResponse
@property (nonatomic, strong) NSArray* contacts;
@end
