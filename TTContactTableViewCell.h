//
//  TTContactTableViewCell.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTContactTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel* emailLabel;
@property (nonatomic, weak) IBOutlet UILabel* firstNameLabel;
@property (nonatomic, weak) IBOutlet UILabel* lastNameLabel;
@property (nonatomic, weak) IBOutlet UIButton* deleteButton;
@end
