                      //
//  TTHoushiApi.m
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

//for    //@"http://houshi.timberlaketekk.com/api/json"
//#define TEMP_SESSION_ID @"65aeb633-e455-11e2-8e16-acde48000502"

////DEVEOPMENT
//@"http://invapidev.slchoushi.com/api/json"
#define TEMP_SESSION_ID  @"66bf661d-d706-11e3-b05e-003048d3ea8c"

//for //PRODUCTION
//@"http://invapi.slchoushi.com/api/json"
//#define TEMP_SESSION_ID @"66bf661d-d706-11e3-b05e-003048d3ea8c"

#define ORDR_ITEM_LIMIT 1000

#import "TTHoushiApi.h"
#import "AFNetworking.h"
#import "TTStartSessionRequestParams.h"
#import "TTUrlRequest.h"
#import "TTBaseResponse.h"
#import "JSON.h"
#import "TTString+Utils.h"
#import "TTBaseRequest+Serialization.h"
#import "TTLoginResponse.h"
#import "TTGeneralSearchRequestParams.h"
#import "TTUserListResponse.h"
#import "TTInventoryListResponse.h"
#import "TTCreateOrderRequest.h"
#import "TTCreateOrderResponse.h"
#import "TTSaveOrderRequest.h"
#import "TTSaveOrderResponse.h"
#import "TTCurrentWorkingObjects.h"
#import "TTOrderItemDTO.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "ErrorHandling/ErrorHandlingFromApiResponse.h"
#import "TTAppConatants.h"
#import "TTOrderListResponse.h"
#import "TTSaveMultipleNoteRequest.h"
#import "TTSearchOrderItemRequest.h"
#import "TTSearchOrderItemRespone.h"
#import "TTSearchCustomerProductResponse.h"
#import "TTSearchProductPullResponse.h"
#import "TTSavePulledItemsRequest.h"
#import "TTSavePullItemsResponse.h"
#import "TTSearchCustomerResponse.h"
#import "TTGetCustomerContactRequest.h"
#import "TTGetCustomerContactResponse.h"
#import "TTSaveCustomerContactRequest.h"
#import "TTSaveCustomerContactResponse.h"
#import "TTDeleteCustomerContactRequest.h"
#import "TTDeleteCustomerContactResponse.h"
#import "TTSaveDeliveryRequest.h"
#import "TTSaveDeliveryResponse.h"
#import "HelperMethods.h"
#import "TTSaveMultipleNoteResponse.h"
#import "TTCloseOrderRequest.h"
#import "TTCloseOrderResponse.h"
#import "TTSaveOrderNetworkErrorResponse.h"
#import "TTValidOrderResponse.h"
#import "Base64.h"

@interface TTHoushiApi (){
     __block void (^alertSuccessBlock)(void) ;
    __block void (^alertFailureBlock)(void) ;
}

@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;
@property (nonatomic, strong) AFNetworkReachabilityManager *managerReachability;

@property (nonatomic, strong) NSString* sessionToken;
//@property (nonatomic, strong)
@end

@implementation TTHoushiApi


static TTHoushiApi* apiInstance;
static MBProgressHUD *hudMB;


+(TTHoushiApi*)getInstance
{
    if (!apiInstance) {
        apiInstance = [TTHoushiApi new];
        apiInstance.manager = [AFHTTPRequestOperationManager manager];
        apiInstance.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        apiInstance.managerReachability = [AFNetworkReachabilityManager sharedManager];
        [apiInstance.managerReachability startMonitoring];

    }
    
    return apiInstance;
}

+(void)setDelegate:(id)delegate
{
    [TTHoushiApi getInstance].delegate = delegate;
}

+(void)createOrderWithCustomerId:(int)customerId isQuickOrder:(BOOL)isQOrder withCustomerScheduleId:(int)custSchedId
{
    TTCreateOrderRequest* params = [TTCreateOrderRequest new];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :TEMP_SESSION_ID;
    params.customerId = [NSNumber numberWithInt:customerId];
    params.quickOrder = [NSNumber numberWithBool:isQOrder];
    if(custSchedId > 0)
        params.customerScheduleId = [NSNumber numberWithInt:custSchedId];
    if([TTAppConatants CURRENT_ORDER_TYPE] == ORDER_TYPE_CREDIT)
        params.isCreditOrder = [NSNumber numberWithBool:YES];
  
    [TTHoushiApi runQuerywithMethod:@"createOrder" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTCreateOrderResponse* newOrderResponse = [[TTCreateOrderResponse alloc] initWithDictionary:result];
        newOrderResponse.order.orderItem = newOrderResponse.orderItem;
        NSLog(@"YES");
        
        if([TTHoushiApi getInstance].delegate)
           [[TTHoushiApi getInstance].delegate onOperationSuccess:newOrderResponse];
            
    }andFailure:^(NSError* error){
        
    }];
    
}

+(void)resetCustomerSchedule
{
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict setObject:([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :TEMP_SESSION_ID forKey:@"sessionToken"];
    [TTHoushiApi runQuerywithMethod:@"resetSchedule" andParams:dict givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        APPDELEGATE_OBJ.calledResetCustomerScheduleAPISuccessfully = YES;
        NSLog(@"YES");

    }andFailure:^(NSError* error){
        NSLog(@"no");
        APPDELEGATE_OBJ.calledResetCustomerScheduleAPISuccessfully = NO;

    }];
    
    
}

+(void)showInventoryList
{
    TTGeneralSearchRequestParams* params = [TTGeneralSearchRequestParams new];
    params.searchField = @[@"active"];
    params.searchValue = @[@"1"];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:500];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :    TEMP_SESSION_ID;
    
    [TTHoushiApi runQuerywithMethod:@"searchCustomerSchedule" andParams:[params toDictionary] givingSuccess:^(id response){
      
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        TTInventoryListResponse* invResponse = [[TTInventoryListResponse alloc] initWithDictionary:result];
        NSLog(@"YES");
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:invResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)showCustomerList{
    TTGeneralSearchRequestParams* params = [TTGeneralSearchRequestParams new];
    params.searchField = @[@"active"];
    params.searchValue = @[[NSNumber numberWithInt:1]];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:500];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :    TEMP_SESSION_ID;
    
    [TTHoushiApi runQuerywithMethod:@"searchCustomer" andParams:[params toDictionary] givingSuccess:^(id response){
        
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        TTSearchCustomerResponse* custResponse = [[TTSearchCustomerResponse alloc] initWithDictionary:result];
        NSLog(@"YES");
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:custResponse];
    }andFailure:^(NSError* error){
        
    }];
    
    
}

+(void)showOrderList{
    TTGeneralSearchRequestParams* params = [TTGeneralSearchRequestParams new];
    params.searchField = @[@"statusId"];
    params.searchValue = @[[NSNumber numberWithInt:(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY)? ORDER_TYPE_DELIVERY :TTAppConatants.CURRENT_ORDER_TYPE]];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:500];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :    TEMP_SESSION_ID;
    

    NSMutableDictionary* parameters = [NSMutableDictionary dictionaryWithDictionary:[params toDictionary]];
    
    [TTHoushiApi runQuerywithMethod:@"searchOrder" andParams:parameters givingSuccess:^(id response){
        
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        TTOrderListResponse* ordResponse = [[TTOrderListResponse alloc] initWithDictionary:result];
        NSLog(@"YES");
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:ordResponse];
    }andFailure:^(NSError* error){
        
    }];
    
}

+(void)getCustomerProductByProductId:(int)productId
{
    TTGeneralSearchRequestParams* params = [TTGeneralSearchRequestParams new];
    params.searchField = @[@"customerId",@"productId"];
    params.searchValue = @[[NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].customer.id.intValue],[NSNumber numberWithInt:productId]];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:ORDR_ITEM_LIMIT];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :    TEMP_SESSION_ID;
    
    [TTHoushiApi runQuerywithMethod:@"searchCustomerProduct" andParams:[params toDictionary] givingSuccess:^(id response){
        
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        TTSearchCustomerProductResponse* custProdResponse = [[TTSearchCustomerProductResponse alloc] initWithDictionary:result];
        NSLog(@"YES");
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:custProdResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)getPPPItems{
    TTGeneralSearchRequestParams* params = [TTGeneralSearchRequestParams new];
    params.searchField = @[@"orderId"];
    params.searchValue = @[[NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue]];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:ORDR_ITEM_LIMIT];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :TEMP_SESSION_ID;
    
    [TTHoushiApi runQuerywithMethod:@"searchProductPull" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTSearchProductPullResponse* pppResponse = [[TTSearchProductPullResponse alloc] initWithDictionary:result];
        NSLog(@"YES");
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:pppResponse];
    }andFailure:^(NSError* error){
        
    }];
    
}

+(void)getCustomerContactsWithCustomerId:(int)custId
{
    TTGetCustomerContactRequest* params = [TTGetCustomerContactRequest new];
    params.customerId = [NSNumber numberWithInt:custId];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :TEMP_SESSION_ID;
    params.activeOnly = [NSNumber numberWithBool:NO];
    [TTHoushiApi runQuerywithMethod:@"getCustomerContacts" andParams:[params toDictionary] givingSuccess:^(id response){
        NSArray* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTGetCustomerContactResponse* contactResponse = [[TTGetCustomerContactResponse alloc] initWithDictionary:@{@"contacts": result}];
        NSLog(@"YES");
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:contactResponse];
    }andFailure:^(NSError* error){
        
    }];
    
}

+(void)showUserList
{
    TTGeneralSearchRequestParams* params = [TTGeneralSearchRequestParams new];
    params.searchField = @[@"userRoleId"];
    params.searchValue = @[@"< 3"];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:500];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :TEMP_SESSION_ID;
    
    [TTHoushiApi runQuerywithMethod:@"searchUser" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTUserListResponse* userResponse = [[TTUserListResponse alloc] initWithDictionary:result];
        NSLog(@"YES");
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:userResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)saveOrder:(int)orderState{
    
    TTSaveOrderRequest* params = [TTSaveOrderRequest new];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken : TEMP_SESSION_ID;
    params.customerId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].customerId.intValue];
    params.orderId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue];
    params.orderState = [NSNumber numberWithInt:orderState];
    params.eventName = [TTCurrentWorkingObjects currentOrder].eventName;
    params.occupancyRate = [TTCurrentWorkingObjects currentOrder].occupancyRate;
    params.purchaseOrderNumber = [TTCurrentWorkingObjects currentOrder].purchaseOrderNumber;
    params.orderItem = [NSMutableArray new];
    for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem) {
        NSMutableDictionary* orderItem = [NSMutableDictionary new];
        [orderItem setObject:[NSNumber numberWithInt:item.id.intValue ] forKey:@"id"];
        if (item.qtyFront && ![item.qtyFront isEqualToString:@""] )
            [orderItem setObject:[NSNumber numberWithInt:item.qtyFront.intValue ] forKey:@"qtyFront"];
        if (item.qtyBack && ![item.qtyBack isEqualToString:@""] )
            [orderItem setObject:[NSNumber numberWithInt:item.qtyBack.intValue ] forKey:@"qtyBack"];
        if (item.qtyOrdered && ![item.qtyOrdered isEqualToString:@""] )
            [orderItem setObject:[NSNumber numberWithInt:item.qtyOrdered.intValue ] forKey:@"qtyOrdered"];
        if (item.qtyOrderSuggested && ![item.qtyOrderSuggested isEqualToString:@""] )
            [orderItem setObject:item.qtyOrderSuggested forKey:@"qtyOrderSuggested"];
        [params.orderItem addObject:orderItem];
    }
    if ([TTCurrentWorkingObjects currentOrder].customerName) {
        params.orderName = [TTCurrentWorkingObjects currentOrder].customerName;
    }

    
    
    [TTHoushiApi runQuerywithMethod:@"saveOrder" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTSaveOrderResponse * orderResponse = [[TTSaveOrderResponse alloc] init];
        
        NSLog(@"YES");
        if ([result isKindOfClass:[NSNumber class]]) {
            orderResponse.success = (NSNumber*)result;
        }

        [TTHoushiApi closeOrder];

        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:orderResponse];
    }andFailure:^(NSError* error){
        
    }];
    
}

+(void)closeOrder{
    TTCloseOrderRequest* parameters = [TTCloseOrderRequest new];
    parameters.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken : TEMP_SESSION_ID;
    parameters.orderId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue];
    
    [TTHoushiApi runQuerywithMethod:@"closeOrder" andParams:[parameters toDictionary] givingSuccess:nil andFailure:nil];
}

+(void)getOrderItems
{
    TTSearchOrderItemRequest* params = [TTSearchOrderItemRequest new];
    params.searchField = @[@"orderId"];
    params.searchValue = @[[NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue]];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:ORDR_ITEM_LIMIT];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.readOnly = (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY)? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :TEMP_SESSION_ID;

    if(TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PPP || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY){
        params.nonZeroItemsOnly = [NSNumber numberWithBool:YES];
    }else{
        //params.nonZeroItemsOnly = [NSNumber numberWithBool:NO];
    }
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionaryWithDictionary:[params toDictionary]];
    
    if (TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_DELIVERY || TTAppConatants.CURRENT_ORDER_TYPE == ORDER_TYPE_PREVIEW_DELIVERY) {
        [parameters setObject:[NSNumber numberWithBool:YES] forKey:@"fullReport"];
    }
    
    
    [TTHoushiApi runQuerywithMethod:@"searchOrderItem" andParams:parameters givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTSearchOrderItemRespone* ordItemResponse = [[TTSearchOrderItemRespone alloc] initWithDictionary:result];
        NSLog(@"YES");
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:ordItemResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)loginWithUsername:(NSString *)userName andPassword:(NSString *)password
{
    TTStartSessionRequestParams* params = [[TTStartSessionRequestParams alloc] init];
    params.userName=userName;
    params.pin = password;
    params.pin = [params.pin sha1:params.pin];
    
    [TTHoushiApi runQuerywithMethod:@"startSession" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTLoginResponse* loginResponse = [[TTLoginResponse alloc] initWithDictionary:result];
        [TTHoushiApi getInstance].sessionToken = loginResponse.token;
        [TTHoushiApi getInstance].currentUser = loginResponse.user;
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:loginResponse];
        NSLog(@"YES");
        
    } andFailure:^(NSError* error){
        
        
    }];
    
}

+(void)logout
{
    [TTHoushiApi getInstance].sessionToken = nil;
    [TTHoushiApi getInstance].currentUser = nil;
}

+(void)saveNotes:(NSArray*)notes
{
    TTSaveMultipleNoteRequest* params = [[TTSaveMultipleNoteRequest alloc] init];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken : TEMP_SESSION_ID;
    params.orderId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue];
    params.notes = [NSMutableArray new];
    for (TTOrderNoteDTO* item in notes) {
        NSMutableDictionary* orderNote = [NSMutableDictionary new];
        if (item.id )
            [orderNote setObject:item.id forKey:@"id"];
        if (item.orderId)
            [orderNote setObject:item.orderId forKey:@"orderId"];
        if (item.note && ![item.note isEqualToString:@""] )
            [orderNote setObject:item.note forKey:@"note"];
        else
            [orderNote setObject:@"" forKey:@"note"];
        if (item.image && ![item.image isEqualToString:@""] )
            [orderNote setObject:item.image forKey:@"image"];
        [params.notes addObject:orderNote];
    }
    __block NSArray* notesArray = [NSArray arrayWithArray:notes];
    
    [TTHoushiApi runQuerywithMethod:@"saveMultipleNote" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTSaveMultipleNoteResponse * orderResponse = [[TTSaveMultipleNoteResponse alloc] init];
        
        NSLog(@"YES");
        if ([result isKindOfClass:[NSNumber class]]) {
            orderResponse.success = (NSNumber*)result;
        }
        
        [[TTCurrentWorkingObjects currentOrder].note addObjectsFromArray:notesArray];
        notesArray = nil;
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:orderResponse];
        
    } andFailure:^(NSError* error){
        
        
    }];
    
}

+(void)saveContactToCustomerWithId:(int)custId AndContact:(id)contact AndPrimary:(BOOL)isPrimary{
    if (![contact isKindOfClass:[TTContactDTO class]]) {
        NSLog(@"ERROR! NOT SENDING A CONTACT OBJECT");
        return;
    }
    
    TTSaveCustomerContactRequest* params = [TTSaveCustomerContactRequest new];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken : TEMP_SESSION_ID;
    params.customerId = [NSNumber numberWithInt:custId];
    params.contact = [((TTContactDTO*)contact) toDictionary];
    params.primaryContact = [NSNumber numberWithBool:isPrimary];//default is false
    
    [TTHoushiApi runQuerywithMethod:@"saveCustomerContact" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTSaveCustomerContactResponse * contResponse = [[TTSaveCustomerContactResponse alloc] init];
        
        NSLog(@"YES");
        if ([result isKindOfClass:[NSNumber class]]) {
            contResponse.success = (NSNumber*)result;
        }
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:contResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)deleteContactWithId:(int)contactId andCustomerId:(int)customerId
{
    TTDeleteCustomerContactRequest* params = [TTDeleteCustomerContactRequest new];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken : TEMP_SESSION_ID;
    params.customerId = [NSNumber numberWithInt:customerId];
    params.contactId = [NSNumber numberWithInt:contactId];

    
    [TTHoushiApi runQuerywithMethod:@"deleteCustomerContactWithId" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTDeleteCustomerContactResponse * contResponse = [[TTDeleteCustomerContactResponse alloc] init];
        
        NSLog(@"YES");
        if ([result isKindOfClass:[NSNumber class]]) {
            contResponse.success = (NSNumber*)result;
        }
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:contResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)savePullStatusWithNumberOfBoxes:(int)number
{
    TTSavePulledItemsRequest* params = [TTSavePulledItemsRequest new];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken : TEMP_SESSION_ID;
    params.orderId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue];
    params.numBoxes = [NSNumber numberWithInt:number];
    params.productPulled = [NSMutableArray new];
    for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem) {
        NSMutableDictionary* prodPull = [NSMutableDictionary new];
        
        if (item.productPull.orderId && ![item.productPull.orderId isEqualToString:@""] )
            [prodPull setObject:[NSNumber numberWithInt:item.productPull.orderId.intValue ] forKey:@"orderId"];
        
        if (item.id && ![item.id isEqualToString:@""] )
            [prodPull setObject:[NSNumber numberWithInt:item.id.intValue ] forKey:@"orderItemId"];
        
        if (item.product.id && ![item.product.id isEqualToString:@""] )
            [prodPull setObject:[NSNumber numberWithInt:item.product.id.intValue ] forKey:@"productId"];
        
        if (item.productPull.qty && ![item.productPull.qty isEqualToString:@""] )
            [prodPull setObject:[NSNumber numberWithInt:item.productPull.qty.intValue ] forKey:@"qty"];
        
        [params.productPulled addObject:prodPull];
    }
    //false by default
    params.adminEdit = [NSNumber numberWithBool:NO];
    
    [TTHoushiApi runQuerywithMethod:@"savePulledItems" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTSavePullItemsResponse * orderResponse = [[TTSavePullItemsResponse alloc] init];
        
        NSLog(@"YES");
        if ([result isKindOfClass:[NSNumber class]]) {
            orderResponse.success = (NSNumber*)result;
        }
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:orderResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)saveDeliveryStatustatusWithDelNumber:(NSString*)deliveryNumber
{
    TTSaveDeliveryRequest* params = [TTSaveDeliveryRequest new];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken : TEMP_SESSION_ID;
    params.orderId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].id.intValue];
    params.customerId = [NSNumber numberWithInt:[TTCurrentWorkingObjects currentOrder].customerId.intValue];
    params.deliveryNumber = deliveryNumber ? deliveryNumber : @"";
    params.confirmationName = [[NSString alloc] initWithFormat:@"%@ %@",[TTCurrentWorkingObjects currentContact].firstName, [TTCurrentWorkingObjects currentContact].lastName ];
    params.confirmationEmail = [TTCurrentWorkingObjects currentContact].email;
    UIImage* image = [[TTCurrentWorkingObjects signatureImage] copy];
    params.confirmationSignature = [HelperMethods convertImageToStringData:image];
    params.deliveredItems = [NSMutableArray new];
    for (TTOrderItemDTO* item in [TTCurrentWorkingObjects currentOrder].orderItem) {
        [params.deliveredItems addObject:[item.productDelivery toDictionary]];
    }
    //false by default
    params.adminEdit = [NSNumber numberWithBool:NO];
    
    [TTHoushiApi runQuerywithMethod:@"saveDeliveredItems" andParams:[params toDictionary] givingSuccess:^(id response){
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        
        TTSaveDeliveryResponse * orderResponse = [[TTSaveDeliveryResponse alloc] init];
        
        NSLog(@"YES");
        if ([result isKindOfClass:[NSNumber class]]) {
            orderResponse.success = (NSNumber*)result;
        }
        
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:orderResponse];
    }andFailure:^(NSError* error){
        
    }];
}

+(void)createInvoice{
    //http://admindev.slchoushi.com/orders/invoice/orderId=&customerId=
    NSString* invoiceUrl = [[NSString alloc] initWithFormat:@"http://admin.slchoushi.com/orders/invoice?orderId=%@&customerId=%@&user=slc&pass=1234",[Base64 encode:[[TTCurrentWorkingObjects currentOrder].id dataUsingEncoding:NSUTF8StringEncoding] ] ,[Base64 encode:[[TTCurrentWorkingObjects currentOrder].customerId dataUsingEncoding:NSUTF8StringEncoding] ] ];
    [[TTHoushiApi getInstance].manager GET:invoiceUrl parameters:nil success:nil failure:nil];
}

+(void)createCredit{
    //http://admindev.slchoushi.com/orders/invoice/orderId=&customerId=
    NSString* invoiceUrl = [[NSString alloc] initWithFormat:@"http://admin.slchoushi.com/orders/credit?orderId=%@&customerId=%@&user=slc&pass=1234",[Base64 encode:[[TTCurrentWorkingObjects currentOrder].id dataUsingEncoding:NSUTF8StringEncoding] ] ,[Base64 encode:[[TTCurrentWorkingObjects currentOrder].customerId dataUsingEncoding:NSUTF8StringEncoding] ] ];
    [[TTHoushiApi getInstance].manager GET:invoiceUrl parameters:nil success:nil failure:nil];
}

+(void)isSavedOrderValidWithOrderId:(NSNumber*)orderId andOrderState:(int)state{
    TTGeneralSearchRequestParams* params = [TTGeneralSearchRequestParams new];
    params.searchField = @[@"statusId",@"id"];
    params.searchValue = @[[NSNumber numberWithInt:state], orderId];
    params.returnFields = @[];
    params.offset = [NSNumber numberWithInt:0];
    params.limit = [NSNumber numberWithInt:500];
    params.orderByDirection = @[];
    params.orderByFields = @[];
    params.sessionToken = ([TTHoushiApi getInstance].sessionToken) ? [TTHoushiApi getInstance].sessionToken :    TEMP_SESSION_ID;
    
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionaryWithDictionary:[params toDictionary]];
    
    [TTHoushiApi runQuerywithMethod:@"searchOrder" andParams:parameters givingSuccess:^(id response){
        
        NSDictionary* result = [((NSDictionary*) response) objectForKey:@"result"];
        TTOrderListResponse* ordResponse = [[TTOrderListResponse alloc] initWithDictionary:result];
        
        NSLog(@"YES");
        TTValidOrderResponse* validationresponse = [TTValidOrderResponse new];
        if (ordResponse.order.count > 0) {
            validationresponse.isValid = [NSNumber numberWithBool:YES];
        }else{
            validationresponse.isValid = [NSNumber numberWithBool:NO];
        }
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationSuccess:validationresponse];
    }andFailure:^(NSError* error){
        
    }];
}


+(void)runQuerywithMethod:(NSString*)method andParams:(NSDictionary*)params givingSuccess:(void (^)(id responseObject))onSuccess
andFailure:(void (^)(NSError *error))onFailure
{
    
    
    if ([TTHoushiApi reachable] == NO) {
        
        NSLog(@"No network");
        if([TTHoushiApi getInstance].delegate)
            [[TTHoushiApi getInstance].delegate onOperationError:[TTSaveOrderNetworkErrorResponse new]];
        return;
    }
    
    NSLog(@"Connected");
    
    TTUrlRequest* req = [TTUrlRequest new];
    req.dataType = @"json";
    req.method = method,
    req.params = params;
    req.jsonrpc = @"2.0";
    req.id = @"1";
    
    NSDictionary* dict = [req toDictionary];
    
    NSLog(@"%@",[TTHoushiApi topMostController]);
    if ([TTHoushiApi topMostController].view) {
        
        [TTHoushiApi addHudWithMessage:@"Please wait..."];
    }
    

    
    //@"http://houshi.timberlaketekk.com/api/json"//@"http://invapi.slchoushi.com/api/json"
    [[TTHoushiApi getInstance].manager POST:@"http://invapidev.slchoushi.com/api/json" parameters:[dict JSONRepresentation]  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [TTHoushiApi releaseHud];
        
        NSString* json = [[NSString alloc] initWithData:(NSData*)responseObject encoding:NSUTF8StringEncoding];
        id value = [json JSONValue];
        if (![TTHoushiApi errorsPresent:value]) {
            if (onSuccess)
                onSuccess(value);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (onFailure)
            onFailure(error);
    
       
    
    }];
    
    
}


+(BOOL)errorsPresent:(NSDictionary*)response
{
   NSDictionary *dictStatus = [ErrorHandlingFromApiResponse checkErrorInResponse:response];
    if ([[dictStatus valueForKey:@"errorOccured"] integerValue] == 1) {
        
        if ([TTHoushiApi topMostController]) {

            NSString *strErrorMessage = [dictStatus valueForKey:@"message"];
            UIAlertView *alertError =  [ErrorHandlingFromApiResponse displayErrorAlert:strErrorMessage];
            alertError.delegate = [TTHoushiApi topMostController];
        }
        return true;
    }
    
    return false;
}



#pragma mark Add Hud

+(void)addHudWithMessage:(NSString *)message{
    
    if (!hudMB) {
        
        hudMB = [MBProgressHUD showHUDAddedTo:[TTHoushiApi topMostController].view animated:YES];
        hudMB.mode = MBProgressHUDModeIndeterminate;
        hudMB.labelText = message;
        hudMB.dimBackground = YES;
    }
}

+(void)releaseHud{
    
    if (hudMB) {
        
        [hudMB hide:YES];
        hudMB = nil;
        
    }
}

#pragma mark Reachability

+(BOOL)reachable {
    
    Reachability *r = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:@"Your Network connection seems off. Check for appropriate network connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        return NO;
    }
    return YES;
}

#pragma mark Show confirm alert

-(void)showConfirmAlert:(NSString *)message withNoMessage:(NSString *)noText andYesText:(NSString *)yesText performingTheResutInBlock:(void (^)())success{
    if (!noText) {
        noText = @"CANCEL";
    }
    
    if (!yesText) {
        yesText = @"YES";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:message delegate:self cancelButtonTitle:noText otherButtonTitles:yesText, nil];
    alert.tag = 2;
    alertSuccessBlock = success;
    [alert show];
}


-(void)showConfirmAlert:(NSString *)message withNoMessage:(NSString *)noText andYesText:(NSString *)yesText performingTheResutInBlock:(void (^)())success andFailure:(void (^)())failure{
    if (!noText) {
        noText = @"CANCEL";
    }
    
    if (!yesText) {
        yesText = @"YES";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:message delegate:self cancelButtonTitle:noText otherButtonTitles:yesText, nil];
    alert.tag = 2;
    alertSuccessBlock = success;
    alertFailureBlock = failure;
    [alert show];
}

-(void)showAlertWithOnlyOkButton:(NSString *)message performingTheResutInBlock:(void (^)())success andFailure:(void (^)())failure{
   
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 3;
    alertSuccessBlock = success;
    alertFailureBlock = failure;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
 
    if (buttonIndex == 1 ) {
        if (self.delegate && !alertSuccessBlock) {
            [self.delegate alertConfirmClicked];
        }
        if (alertSuccessBlock) {
            alertSuccessBlock();
            alertSuccessBlock = nil;
        }
        
    }
    
    if (buttonIndex == 0) {
        if (alertFailureBlock) {
            alertFailureBlock();
            alertFailureBlock = nil;
        }
    }
    
}

#pragma mark GetTopContoller

+ (UIViewController *) topMostController{

    UINavigationController *navigationController = (UINavigationController *)APPDELEGATE_OBJ.window.rootViewController;
    return [navigationController.viewControllers lastObject];
   // return [[[[UIApplication sharedApplication] keyWindow] subviews] lastObject];
}

#pragma mark GetTopContoller

@end
