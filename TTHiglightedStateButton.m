//
//  TTHiglightedStateButton.m
//  Houshi
//
//  Created by James Timberlake on 9/4/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTHiglightedStateButton.h"
@interface TTHiglightedStateButton(){
    UITapGestureRecognizer* tapGesture;
    BOOL added;
}
@property (nonatomic) BOOL currentHighlightState;
@end

@implementation TTHiglightedStateButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(void)awakeFromNib
{
    if(!added){
         [self addTarget:self action:@selector(setHighlightMode) forControlEvents:UIControlEventTouchUpInside];
        added = YES;
    }
}


-(void)setHighlightMode
{
    self.selected = !self.selected;
    if (_delegate) {
        [_delegate onButtonHighlightToggle:self.selected];
    }
}

-(BOOL)getSelected{
    return self.selected;
}


-(void)toggleHighlight:(id)sender {
   
//    if (_isHeader && self.highlighted != _currentHighlightState) {
//        self.highlighted = _currentHighlightState;
//    }
}

@end
