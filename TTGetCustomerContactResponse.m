//
//  TTGetCustomeResponse.m
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTGetCustomerContactResponse.h"
#import "TTContactDTO.h"

@implementation TTGetCustomerContactResponse
+(Class) contacts_class  {
    return [TTContactDTO class];
}
@end
