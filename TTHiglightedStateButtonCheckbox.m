//
//  TTHiglightedStateButton.m
//  Houshi
//
//  Created by James Timberlake on 9/4/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTHiglightedStateButtonCheckbox.h"
@interface TTHiglightedStateButtonCheckbox()<UIGestureRecognizerDelegate>{
    UITapGestureRecognizer* tapGesture;
}
@property (nonatomic) BOOL currentHighlightState;
@end

@implementation TTHiglightedStateButtonCheckbox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

-(void)awakeFromNib
{
    //if(!tapGesture){
       tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setHighlightMode)];
        tapGesture.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tapGesture];
   // }
}




-(void)setHighlightMode
{
    _currentHighlightState = !_currentHighlightState;
    //allow the call to pass through
    //[super sendAction:action to:target forEvent:event];
    //toggle the property after a delay (to make sure the event has processed)
    //[self performSelector:@selector(toggleHighlight:) withObject:nil afterDelay:.01];
    //[self toggleHighlight:nil];
    [self performSelectorOnMainThread:@selector(toggleHighlight:) withObject:nil waitUntilDone:YES];
}

-(void)setHighlightModeTo:(BOOL)isHighlighted{
    _currentHighlightState = isHighlighted;
    [self performSelectorInBackground:@selector(doBackgroundPost) withObject:nil];
    [self performSelectorOnMainThread:@selector(toggleHighlight:) withObject:nil waitUntilDone:YES];
}

-(void)doBackgroundPost{
    if (_delegate) {
        [_delegate onButtonHighlightToggle:_currentHighlightState];
    }
}

-(void)toggleHighlight:(id)sender {
    self.highlighted = _currentHighlightState;
   //self.selected = currentHighlightState;
}



@end
