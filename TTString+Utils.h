//
//  TTStringUtils.h
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TTStringUtils)
-(NSString*) sha1:(NSString*)input;
@end
