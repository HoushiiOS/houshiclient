//
//  TTCreateOrderResponse.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTOrderDTO.h"

@interface TTCreateOrderResponse : TTBaseResponse
@property (nonatomic, strong) TTOrderDTO* order;
@property (nonatomic, strong) NSArray* orderItem;
@end
