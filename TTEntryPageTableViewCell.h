//
//  TTEntryPageTableViewCell.h
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+UITableCellUtilities.h"
#import "TTHiglightedStateButton.h"
#import "TTBaseTableViewCell.h"

@protocol EntryCellDelegate <NSObject>

-(void)onEntryItemCheckedAtIndex:(int)rowIndex withQuantity:(NSString*)qty isHighlighted:(BOOL)isHighlighted;

@end


@interface TTEntryPageTableViewCell : TTBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *skuField;
@property (weak, nonatomic) IBOutlet UILabel *headerField;
@property (weak, nonatomic) IBOutlet TTHiglightedStateButton *highlightButton;
@property (weak, nonatomic) IBOutlet UITextField *pastItemField;
@property (weak, nonatomic) IBOutlet UITextField *quantityField;
@property (strong, nonatomic) id dataObject;
@property (nonatomic, weak) id<EntryCellDelegate> delegate;
@property (nonatomic) int rowIndex;
-(int)getRowIndex;
-(void)setRowIndex:(int)rowIndex;
-(void)setDataObject:(id)dataObject;
@end
