//
//  TTEntryPageTableViewCell.m
//  Houshi
//
//  Created by JT on 5/29/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTEntryPageTableViewCell.h"
#import "TTOrderItemDTO.h"
#import "TTAppConatants.h"

@interface TTEntryPageTableViewCell() <HighlightedButtonDelegate>

@end

@implementation TTEntryPageTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       // [_highlightButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setDataObject:(id)dataObject
{
    
    [self.highlightButton setSelected:NO];
    [_skuField setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
    [_headerField setFont:[UIFont fontWithName:@"HelveticaNeue" size:[TTAppConatants orderItemFontSize] ]];
     _highlightButton.delegate = self;

    if ([dataObject isKindOfClass:[TTOrderItemDTO class]]) {
        _dataObject = dataObject;
        _skuField.text = ((TTOrderItemDTO*)dataObject).product.sku;
        _headerField.text = ((TTOrderItemDTO*)dataObject).product.desc;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(int)getRowIndex{
    return _rowIndex;
}

-(void)setRowIndex:(int)rowIndex{
    _rowIndex = rowIndex;
}

-(void)onButtonHighlightToggle:(BOOL)isHighlighted
{
    self.quantityField.text = isHighlighted?  self.pastItemField.text : nil;
    if (_delegate) {
        [_delegate onEntryItemCheckedAtIndex:[self getRowIndex] withQuantity:(NSString*)_quantityField.text isHighlighted:isHighlighted];
    }
}

@end
