//
//  Constants.h
//  Houshi
//
//  Created by Vishal on 01/09/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#ifndef Houshi_Constants_h
#define Houshi_Constants_h

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define APPDELEGATE_OBJ ((TTAppDelegate *)[UIApplication sharedApplication].delegate)

#define APP_NAME @"Houshi"

#endif
