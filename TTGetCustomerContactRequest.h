//
//  TTGetCustomerContactRequest.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTGetCustomerContactRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSNumber* customerId;
@property (nonatomic, strong) NSNumber* activeOnly; //default is false

@end
