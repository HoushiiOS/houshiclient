//
//  TTContactDTO.h
//  Houshi
//
//  Created by James Timberlake on 10/18/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTContactDTO : TTBaseResponse
@property (nonatomic, strong) NSNumber* id;
@property (nonatomic, strong) NSNumber* customerContactId;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* middleName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* suffix;
@property (nonatomic, strong) NSString* position;
@property (nonatomic, strong) NSString* mainPhone;
@property (nonatomic, strong) NSString* mainPhoneCountryCode;
@property (nonatomic, strong) NSString* mainPhoneExt;
@property (nonatomic, strong) NSString* altPhone;
@property (nonatomic, strong) NSString* altPhoneCountryCode;
@property (nonatomic, strong) NSString* altPhoneExt;
@property (nonatomic, strong) NSString* contactType;
@property (nonatomic, strong) NSString* address1;
@property (nonatomic, strong) NSString* address2;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* zip;
@property (nonatomic, strong) NSString* zip4;
@property (nonatomic, strong) NSNumber* active;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@end
