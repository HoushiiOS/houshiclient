//
//  TTOrderItemSelect.m
//  Houshi
//
//  Created by James Timberlake on 9/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTOrderItemSelectPopup.h"
#import "DropDownListView.h"

@interface TTOrderItemSelectPopup () <kDropDownListViewDelegate>
@property (nonatomic,strong) DropDownListView* itemSelectView;
@property (nonatomic, weak) id<TTOrderItemSelectPopupDelegate> delegate;
@end

@implementation TTOrderItemSelectPopup

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}

-(id)initIntoView:(UIView*)view withItems:(NSArray*)nameArrays havingTheTitle:(NSString*)title withDelegate:(id<TTOrderItemSelectPopupDelegate>)delegate{
    self = [self initWithFrame:view.frame];
    
   _itemSelectView = [[DropDownListView alloc] initWithTitle:title options:nameArrays xy:CGPointMake(200, 100) size:CGSizeMake(600,535)
                                 isMultiple:NO];
    [_itemSelectView SetBackGroundDropDwon_R:42.0 G:43.0 B:44.0 alpha:0.70];
    [self addSubview:_itemSelectView];
    _itemSelectView.delegate = self;///42	43	44
    
    _delegate = delegate;
    
    self.backgroundColor = [UIColor grayColor];
    self.alpha = 1.0;
    [view addSubview:self];

    return self;
}

- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex
{
    [self removeFromSuperview];
    if (_delegate) {
        [_delegate onItemSelected:anIndex];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
