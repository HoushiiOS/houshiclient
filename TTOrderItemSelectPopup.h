//
//  TTOrderItemSelect.h
//  Houshi
//
//  Created by James Timberlake on 9/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TTOrderItemSelectPopupDelegate <NSObject>

-(void)onItemSelected:(int)index;

@end

@interface TTOrderItemSelectPopup : UIView
-(id)initIntoView:(UIView*)view withItems:(NSArray*)nameArrays havingTheTitle:(NSString*)title withDelegate:(id<TTOrderItemSelectPopupDelegate>)delegate;
@end

