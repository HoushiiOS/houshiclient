//
//  UIImage+TTUIImage_Scale.h
//  Houshi
//
//  Created by James Timberlake on 11/11/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TTUIImage_Scale)

-(UIImage*)scaleToSize:(CGSize)size;

@end
