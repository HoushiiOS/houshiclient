//
//  TTSearchOrderItemRequest.h
//  Houshi
//
//  Created by James Timberlake on 9/9/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseRequest.h"

@interface TTSearchOrderItemRequest : TTBaseRequest
@property (nonatomic, strong) NSString* sessionToken;
@property (nonatomic, strong) NSArray* searchField;
@property (nonatomic, strong) NSArray* searchValue;
@property (nonatomic, strong) NSNumber* offset;
@property (nonatomic, strong) NSNumber* limit;
@property (nonatomic, strong) NSArray* returnFields;
@property (nonatomic, strong) NSArray* orderByFields;
@property (nonatomic, strong) NSString* orderByDirection;
@property (nonatomic, strong) NSNumber* readOnly;// = true,
@property (nonatomic, strong) NSNumber* fullReport;// = false
@property (nonatomic, strong) NSNumber* nonZeroItemsOnly;// = false
@end
