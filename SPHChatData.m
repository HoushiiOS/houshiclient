//
//  SPHChatData.m
//  ChatBubble
//
//  Created by ivmac on 10/2/13.
//  Copyright (c) 2013 Conciergist. All rights reserved.
//

#import "SPHChatData.h"

@implementation SPHChatData

@synthesize messageText;
@synthesize avatarImageURL;
@synthesize messageImageURL;
@synthesize messageTime;
@synthesize bubbleImageName;
@synthesize messageType;
@synthesize messagestatus;
@synthesize messageImage;
@synthesize messagesfrom;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        messageText = [aDecoder decodeObjectForKey:@"messageText"];
        avatarImageURL = [aDecoder decodeObjectForKey:@"avatarImageURL"];
        messageImageURL = [aDecoder decodeObjectForKey:@"messageImageURL"];
        messageTime = [aDecoder decodeObjectForKey:@"messageTime"];
        bubbleImageName = [aDecoder decodeObjectForKey:@"bubbleImageName"];
        messageType = [aDecoder decodeObjectForKey:@"messageType"];
        messagestatus = [aDecoder decodeObjectForKey:@"messagestatus"];
        messageImage = [aDecoder decodeObjectForKey:@"messageImage"];
        messagesfrom = [aDecoder decodeObjectForKey:@"messagesfrom"];

    
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:messageText forKey:@"messageText"];
    [aCoder encodeObject:avatarImageURL forKey:@"avatarImageURL"];
    [aCoder encodeObject:messageImageURL forKey:@"messageImageURL"];
    [aCoder encodeObject:messageTime forKey:@"messageTime"];
    [aCoder encodeObject:bubbleImageName forKey:@"bubbleImageName"];
    [aCoder encodeObject:messageType forKey:@"messageType"];
    [aCoder encodeObject:messagestatus forKey:@"messagestatus"];
    [aCoder encodeObject:messageImage forKey:@"messageImage"];
    [aCoder encodeObject:messagesfrom forKey:@"messagesfrom"];

    
    
}

@end
