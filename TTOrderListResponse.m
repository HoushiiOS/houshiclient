//
//  TTOrderListResponse.m
//  Houshi
//
//  Created by James Timberlake on 9/6/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTOrderListResponse.h"
#import "TTOrderDTO.h"

@implementation TTOrderListResponse
+(Class) order_class                                                                                                   {
    return [TTOrderDTO class];
}
@end
