//
//  TTOrderNote.h
//  Houshi
//
//  Created by James Timberlake on 9/6/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"

@interface TTOrderNoteDTO : TTBaseResponse
@property (nonatomic, strong) NSNumber* id;
@property (nonatomic, strong) NSNumber* orderId;
@property (nonatomic, strong) NSString* note;
//image should be a base64 encoded string
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* createdBy;
//dates coming in as strings
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@end
