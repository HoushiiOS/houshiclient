//
//  TTProductPullDTO.h
//  Houshi
//
//  Created by JT on 5/27/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTProductDTO.h"

@interface TTProductPullDTO : TTBaseResponse
@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* orderItemId;
@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSString* productId;
@property (nonatomic, strong) NSString* qty;
@property (nonatomic, strong) NSString* pulledBy;
@property (nonatomic, strong) NSString* createdAt;
@property (nonatomic, strong) NSString* updatedAt;
@property (nonatomic, strong) TTProductDTO* product;
@end
