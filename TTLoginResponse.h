//
//  TTLoginResponse.h
//  Houshi
//
//  Created by JT on 5/26/14.
//  Copyright (c) 2014 JT. All rights reserved.
//

#import "TTBaseResponse.h"
#import "TTUserDTO.h"

@interface TTLoginResponse : TTBaseResponse
@property NSString* token;
@property TTUserDTO* user;
@end
